package com.scb.s2b.webhookclient.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scb.api.client.util.EncryptDecrypt;
import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.DatatypeConverter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.util.List;

@RestController
public class WebHookClientController {

    private static final Logger logger = LoggerFactory.getLogger(WebHookClientController.class);

    private long responseDelay = 0;

    private Integer responseStatus = 200;

    private final List<String> messages = Lists.newArrayList();

    private EncryptDecrypt ed = EncryptDecrypt.getDefaultInstance();

    private PrivateKey privateKey = EncryptDecrypt.convertStringToPrivateKey(getPrivateKey());
    private PublicKey publicKey = EncryptDecrypt.convertStringToPublicKey(getPublicKey());

    @RequestMapping(value = "/webhook/credit-debit-advice",
            consumes = MediaType.TEXT_PLAIN,
            produces = MediaType.APPLICATION_JSON,
            method = RequestMethod.POST)
    public synchronized ResponseEntity<?> doPost(
            @RequestHeader(name = "Signature") String signature,
            @RequestBody String body, HttpServletResponse response) throws InterruptedException, IOException {

        long start = System.currentTimeMillis();
        try {
            Thread.sleep(responseDelay);

            if (responseStatus != null) {
                response.setStatus(responseStatus);
            }

            EncryptDecrypt.EncryptedContent content = new ObjectMapper().readValue(body, EncryptDecrypt.EncryptedContent.class);
            String payload = ed.decryptWithPrivateKey(privateKey, content);

            byte[] hash = EncryptDecrypt.sha256Hash(payload.getBytes());
            verifySignature(signature, ed, hash);

            logger.info("WH-PAYLOAD$ " + payload);
            synchronized (messages) {
                messages.add(payload);
            }

            return new ResponseEntity("{ \"Status\" : \"OK\" } ", HttpStatus.valueOf(responseStatus));
        } finally {
            logger.info("Webhook processed event in " + (System.currentTimeMillis()-start) + "ms. ");
        }
    }
    
    
    @RequestMapping(value = "/webhook/payment/transaction-status",
            consumes = MediaType.TEXT_PLAIN,
            produces = MediaType.APPLICATION_JSON,
            method = RequestMethod.POST)
    public synchronized ResponseEntity<?> doPost(
            @RequestHeader(name = "Signature") String signature,
            @RequestBody String body, HttpServletResponse response) throws InterruptedException, IOException {

        long start = System.currentTimeMillis();
        try {
            Thread.sleep(responseDelay);

            if (responseStatus != null) {
                response.setStatus(responseStatus);
            }

            EncryptDecrypt.EncryptedContent content = new ObjectMapper().readValue(body, EncryptDecrypt.EncryptedContent.class);
            String payload = ed.decryptWithPrivateKey(privateKey, content);

            byte[] hash = EncryptDecrypt.sha256Hash(payload.getBytes());
            verifySignature(signature, ed, hash);

            logger.info("WH-PAYLOAD$ " + payload);
            synchronized (messages) {
                messages.add(payload);
            }

            return new ResponseEntity("{ \"Status\" : \"OK\" } ", HttpStatus.valueOf(responseStatus));
        } finally {
            logger.info("Webhook processed event in " + (System.currentTimeMillis()-start) + "ms. ");
        }
    }

    

    private void verifySignature(@RequestHeader(name = "Signature") String signature, EncryptDecrypt ed, byte[] hash) {
        try {
            Signature sig = Signature.getInstance("SHA256withRSA", "SUN");
            sig.initVerify(publicKey);
            sig.update(hash);
            if (!sig.verify(DatatypeConverter.parseBase64Binary(signature))) {
                logger.error("Invalid signature!!!!!");
            } else {
                logger.info("Valid signature.");
            }
        } catch (Exception e) {

        }
    }

    @RequestMapping(value = "/webhook/delay",
            produces = MediaType.TEXT_PLAIN,
            method = RequestMethod.GET)
    public String setDelay(
            @RequestParam(name = "value") Long value) throws InterruptedException {

        if (value != null) {
            responseDelay = value;
        }
        return String.valueOf(responseDelay);
    }

    @RequestMapping(value = "/webhook/statusCode",
            produces = MediaType.TEXT_PLAIN,
            method = RequestMethod.GET)
    public String setStatusCode(
            @RequestParam(name = "value") Integer value) throws InterruptedException {

        if (value != null) {
            responseStatus = value;
        }
        return String.valueOf(responseStatus);

    }

    @RequestMapping(value = "/webhook/messages",
            produces = MediaType.APPLICATION_JSON,
            method = RequestMethod.GET)
    public List<String> getMessages() throws InterruptedException {
        synchronized (messages) {
            return Lists.newArrayList(messages);
        }
    }

    @RequestMapping(value = "/webhook/clearMessages",
            produces = MediaType.APPLICATION_JSON,
            method = RequestMethod.GET)
    public String clearMessages() throws InterruptedException {
        synchronized (messages) {
            messages.clear();
        }
        return "";
    }


    private String getPublicKey() {
        String pk = System.getProperty("public-key");
        if (pk == null) {
            return getFile("./public.pem");
        }
        return pk;
    }

    private String getPrivateKey() {
        String pk = System.getProperty("private-key");
        if (pk == null) {
            return getFile("./private.pem");
        }
        return pk;
    }

    private String getFile(String file) {
        try(InputStream input = new FileInputStream(file)) {
            return EncryptDecrypt.toString(input);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(),e);
        }
    }
}
