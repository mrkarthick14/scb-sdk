package com.scb.s2b.webhookclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.Ssl;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.FileNotFoundException;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@ComponentScan
@EnableWebMvc
public class WebHookApp {

    private static int port = 8443;

    public static void forcePort(int port) {
        WebHookApp.port = port;
    }

    public static void main(String[] args) {
        SpringApplication.run(WebHookApp.class, args);
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {
        return container -> {
            container.setPort(port);
//            Ssl ssl = new Ssl();
//            ssl.setEnabled(true);
//            ssl.setKeyAlias("server");
//            ssl.setKeyPassword("password");
//            ssl.setKeyStore("classpath:webhook.jks");
//            ssl.setKeyStorePassword("password");
//            ssl.setKeyStoreType("JKS");
//            container.setSsl(ssl);

        };
    }

}
