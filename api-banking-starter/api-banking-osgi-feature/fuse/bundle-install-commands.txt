1)
Update custom.properties in ${FUSE_HOME}/etc and add:

scb-api-banking.environment=Test
scb-api-banking.keystore-file=file:///[Path to Keystore]
scb-api-banking.keystore-password=password
scb-api-banking.keystore-signature-alias=apibanking-sig-key
scb-api-banking.issuer-id=[organisation name]
scb-api-banking.proxy.host=[corporate proxy host, or blank]
scb-api-banking.proxy.port=[corporate proxy port, or blank]


2) Run the following commands in Fuse console:

osgi:install file:<path to api-banking-client jar>

osgi:install mvn:com.fasterxml.jackson.core/jackson-annotations/2.8.3
osgi:install mvn:com.fasterxml.jackson.core/jackson-core/2.8.3
osgi:install mvn:com.fasterxml.jackson.core/jackson-databind/2.8.3
osgi:install mvn:com.google.code.gson/gson/1.4
osgi:install mvn:com.google.guava/guava/19.0
osgi:install mvn:commons-codec/commons-codec/1.4
osgi:install mvn:commons-io/commons-io/2.6
osgi:install mvn:commons-logging/commons-logging/1.1.3
osgi:install mvn:javax.annotation/javax.annotation-api/1.2
osgi:install mvn:joda-time/joda-time/1.6.2
osgi:install mvn:net.java.dev.jna/jna/4.0.0
osgi:install mvn:net.minidev/accessors-smart/1.1
osgi:install mvn:net.minidev/json-smart/2.2.1
osgi:install mvn:org.apache.httpcomponents/httpclient-osgi/4.3.6
osgi:install mvn:org.bouncycastle/bcprov-jdk16/1.46
osgi:install mvn:org.ow2.asm/asm/5.0.3
osgi:install mvn:org.apache.httpcomponents/httpcore-osgi/4.0.1
osgi:install mvn:org.apache.httpcomponents/httpclient-osgi/4.3.6

osgi:install wrap:mvn:com.googlecode.jsontoken/jsontoken/1.1
osgi:install wrap:mvn:com.squareup.jnagmp/bouncycastle-rsa/2.0.0
osgi:install wrap:mvn:com.squareup.jnagmp/jnagmp/2.0.0
osgi:install wrap:mvn:io.jsonwebtoken/jjwt/0.2

osgi:start api-banking-client


com.fasterxml.jackson.databind,com.google.common.collect,com.google.common.hash,com.google.gson;version,com.squareup.crypto.rsa,in.goindigo.application.scbsdkpaymentservice,in.goindigo.webservices.DecryptionService,in.goindigo.webservices.service,javax.crypto,javax.crypto.spec,javax.net.ssl,javax.sql,javax.xml.bind,net.minidev.json",net.minidev.json.parser",net.oauth.jsontoken,net.oauth.jsontoken.crypto,net.oauth.jsontoken.discovery,org.apache.activemq,org.apache.activemq.camel.component,org.apache.activemq.pool,org.apache.camel;version",org.apache.camel.component.jms,org.apache.commons.codec.binary",org.apache.http,org.apache.http.client.methods,org.apache.http.conn.socket,org.apache.http.conn.ssl,org.apache.http.impl.client,org.apache.log4j,org.bouncycastle.crypto,org.bouncycastle.crypto.digests,org.bouncycastle.crypto.encodings,org.bouncycastle.jce.provider,org.bouncycastle.jce.provider.util,org.bouncycastle.util.io.pem,org.joda.time,org.osgi.service.blueprint,org.restlet.util,org.slf4j
