# SCB API-Banking-Client SDK

To begin using the Standard Chartered API Banking services, the following steps are required:

 1) Creation of a DigiCert rooted certificate
 2) Generate RSA key pair and store your private key in a secure keystore
 3) Supply the Certificate and public key to your Standard Chartered Relationship Manager

**Pre-requisites**

Java 8 with security pack installed 
(JCE 8, http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html)

# RSA Key Pair

1) Create Key store for 2028 RSA key pair

> keytool -genkeypair -keysize 2048 -keyalg RSA -alias apibanking-sig-key -keystore scb-api-banking.jks

2) Export certificate

> keytool -exportcert -keystore scb-api-banking.jks -alias apibanking-sig-key -rfc -file cert.pem

3) Convert to PEM (optional)

> openssl x509 -pubkey -noout -in cert.pem  > pubkey.pem

4) Send the pubkey.pem along with the DigiCert certificate (not including private key) to your relationship manager.

# Certificate issuance 

1) Create a private key 

> openssl genrsa -out ./scb-apibanking-client-cert-private-key.pem 2048

2) Generate CSR based on the private key

> openssl req -new -sha256 -key scb-apibanking-client-cert-private-key.pem -out ./scb-apibanking-client-cert-csr.pem

3) Once the signed certificate is provided, import the signed certificate and private keys into a P

> openssl pkcs12 -export -in indigo-sample.crt -inkey scb-apibanking-client-cert-private-key.pem -out scb-apibanking.keystore.p12 -name 1 

> keytool -importkeystore -deststorepass [password] -destkeypass [password]  -destkeystore keystore.jks -srckeystore scb-apibanking.keystore.p12  -srcstoretype PKCS12 -srcstorepass password -alias 1

NOTE: We will never ask you to send a private key.  The private key should be kept secret (ideally in an hardware security module (HSM)).   If you're private key is sent to us by mistake
we will NOT accept the corresponding public key and new RSA key pair will be required.

