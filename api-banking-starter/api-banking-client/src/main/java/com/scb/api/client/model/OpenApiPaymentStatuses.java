package com.scb.api.client.model;

import java.util.List;

public class OpenApiPaymentStatuses {

    private List<OpenApiPaymentStatus> statuses;

    public OpenApiPaymentStatuses() {
    }

    public OpenApiPaymentStatuses(List<OpenApiPaymentStatus> statuses) {
        this.statuses = statuses;
    }

    public List<OpenApiPaymentStatus> getStatuses() {
        return statuses;
    }
}
