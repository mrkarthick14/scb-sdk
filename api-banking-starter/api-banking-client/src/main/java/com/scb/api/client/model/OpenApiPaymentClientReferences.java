package com.scb.api.client.model;

import java.util.List;

public class OpenApiPaymentClientReferences {

    private List<String> clientReferenceIds;

    public OpenApiPaymentClientReferences() {
    }

    public OpenApiPaymentClientReferences(List<String> clientReferenceIds) {
        this.clientReferenceIds = clientReferenceIds;
    }

    public List<String> getClientReferenceIds() {
        return clientReferenceIds;
    }
}
