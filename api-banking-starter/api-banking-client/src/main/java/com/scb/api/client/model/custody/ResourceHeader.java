package com.scb.api.client.model.custody;


import org.joda.time.Instant;

public class ResourceHeader {

    public enum ResourceStatus {Pending, Complete, Invalid}

    private Instant timestamp;
    private ResourceStatus status;
    private String id;
    private String domainId;

    public Instant getTimestamp() {
        return timestamp;
    }

    public ResourceStatus getStatus() {
        return status;
    }

    public String getId() {
        return id;
    }

    public String getDomainId() {
        return domainId;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public void setStatus(ResourceStatus status) {
        this.status = status;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDomainId(String domainId) {
        this.domainId = domainId;
    }
}
