package com.scb.api.client;

public enum ApiBankingEnvironment {

    Production(
            "api.standardchartered.com",
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkM+yvLMHoJb2NptKzZDQYg967hHfgsnYe+Desz4WD9mYzgqj7HE53TVSbiEddKl7kvgI/ul7FZ/W01XIfWcw6Zg/boZuon/pF1U4tBnH5RI1vWkAuhzTCC/sDKZBXu1b9vMGR8yhctnzF+xS26Z2rzlv/DoaNPdmg3gnOODwThK/PrLM6Ia5JfKbPk8Dvf42JIKnmggLqPmJohRjMW02uLeFKNoVWk37vuqmIQQsNZ33Ua2Qsno39HAPHwCzqWQHOyiAAv3bhLKoKxM2dgLTEDy1txbse8gYu2FxC46kRSxL2TLq+TkIis58lwGttdX7WIf3O7AmnnxtFXF/CLlC0QIDAQAB"),

    Test(
            "apitest.standardchartered.com",
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnzB3mAUtGPB96WAo3LKpoyHd/oDgXU74UhxwcFLZSqJ+G2Xsvqr+wXBsj6RCtWkhZT0dg+kUyo/4KTRTgn8lIMKdWUM3V5+web+H5aecEqw6PfH18KY67pLf62U5qoCZ10PiBbGwHMCr2PmVxt0jxiD4l0o1Bs6dOr2ZwB32eJw7Z5XHWWrN1PEcPXWdvrXmWYy6tamVBcwTt/Q11RhRuXxd13yS6mqsfyJtvwiBCNsaRDeE/7iQGft0U94njDxyG4KpiBWXF0HG/NbKxLDas1VL7T6JpY/yNZHXiJ4hR70VbNmJAKu8vtlWX0BF4cuYduhjE71p/Q+UTHHWRVL7rwIDAQAB"),

    Sandbox(
            "developer.sc.com/cib",
            null);

    private final String host;
    private String publicKey;

    ApiBankingEnvironment(String host, String publicKey) {
        this.host = host;
        this.publicKey = publicKey;
    }

    public String urlOfPath(String path) {
        return String.format("https://%s/%s", host, path);
    }

    public String getPublicKey() {
        return publicKey;
    }

};
