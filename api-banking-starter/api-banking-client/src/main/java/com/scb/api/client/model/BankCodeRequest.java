package com.scb.api.client.model;

public class BankCodeRequest {

    private String branchSwiftCode;
    private String countryCode;

    public BankCodeRequest() { }

    public String getBranchSwiftCode() {
        return branchSwiftCode;
    }

    public String getCountryCode() {
        return countryCode;
    }
}
