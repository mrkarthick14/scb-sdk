
package com.scb.api.client.model;

public class OpenApiParty {

    private String name;
    private OpenApiAddress postalAddress;
    private OpenApiContact contact;
    private OpenApiIdentity identity;

    /**
     * No args constructor for use in serialization
     * 
     */
    public OpenApiParty() {
    }

    public OpenApiParty(String name, OpenApiAddress postalAddress, OpenApiContact contact, OpenApiIdentity identity) {
        this.name = name;
        this.postalAddress = postalAddress;
        this.contact = contact;
        this.identity = identity;
    }

    public OpenApiParty(OpenApiIdentity identity) {
        this.identity = identity;
    }

    public OpenApiContact getContact() {
        return contact;
    }

    public String getName() {
        return name;
    }

    public OpenApiAddress getPostalAddress() {
        return postalAddress;
    }

    public OpenApiIdentity getIdentity() {
        return identity;
    }
}
