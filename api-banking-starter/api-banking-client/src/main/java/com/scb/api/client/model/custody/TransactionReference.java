package com.scb.api.client.model.custody;

import java.util.Objects;

public class TransactionReference {

    private String clientTransactionIdentifier = "";
    private String transactionIdentifier = "";

    public String getClientTransactionIdentifier() {
        return clientTransactionIdentifier;
    }

    public String getTransactionIdentifier() {
        return transactionIdentifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionReference that = (TransactionReference) o;
        return Objects.equals(clientTransactionIdentifier, that.clientTransactionIdentifier) &&
                Objects.equals(transactionIdentifier, that.transactionIdentifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientTransactionIdentifier, transactionIdentifier);
    }
}
