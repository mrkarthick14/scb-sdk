package com.scb.api.client.model;

public class OpenApiAddress {

    private String line_1 = "";
    private String line_2 = "";
    private String line_3 = "";
    private String city = "";
    private String state = "";
    private String postcode = "";
    private String country = "";

    public OpenApiAddress() {}

    public OpenApiAddress(String line_1, String line_2, String line_3, String city, String state, String postcode, String country) {
        this.line_1 = line_1;
        this.line_2 = line_2;
        this.line_3 = line_3;
        this.city = city;
        this.state = state;
        this.postcode = postcode;
        this.country = country;
    }

    public OpenApiAddress(String country) {
        this.country = country;
    }



    public String getLine_1() {
        return line_1;
    }

    public String getLine_2() {
        return line_2;
    }

    public String getLine_3() {
        return line_3;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getPostcode() {
        return postcode;
    }

    public String getCountry() {
        return country;
    }
}
