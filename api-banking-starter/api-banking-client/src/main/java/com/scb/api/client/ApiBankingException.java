package com.scb.api.client;

public class ApiBankingException extends RuntimeException {

    private int code;
    private String message;

    public ApiBankingException(int code, String message) {
        super("Status code: " + code + ", message " + message);
        this.code = code;
        this.message = message;
    }

    public ApiBankingException(Exception e) {
        super(e.getMessage(),e);
        message = e.getMessage();
    }
}
