package com.scb.api.client.model;

public class OpenApiCheque {

    private String number;
    private String clearingDate;
    private String transactionReference;
    private String batchNumber = "";

    public OpenApiCheque() { }

    public OpenApiCheque(String number, String clearingDate, String transactionReference, String batchNumber) {
        this.number = number;
        this.clearingDate = clearingDate;
        this.transactionReference = transactionReference;
        this.batchNumber = batchNumber;
    }

    public String getNumber() {
        return number;
    }

    public String getClearingDate() {
        return clearingDate;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public String getBatchNumber() {
        return batchNumber;
    }
}
