package com.scb.api.client.model.custody;

import com.scb.api.client.model.DenominatedAmount;
import org.joda.time.LocalDate;

import java.util.Objects;

public class Holding {

    private LocalDate asOfDate;
    private String businessPartnerId;
    private String accountId;
    private String accountName;
    private ListedSecurity security;

    private SecurityPosition position;
    private String placeOfSettlement;
    private String oldAccountId;

    public LocalDate getAsOfDate() {
        return asOfDate;
    }

    public String getBusinessPartnerId() {
        return businessPartnerId;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public ListedSecurity getSecurity() {
        return security;
    }

    public SecurityPosition getPosition() {
        return position;
    }

    public String getPlaceOfSettlement() {
        return placeOfSettlement;
    }

    public String getOldAccountId() {
        return oldAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Holding holding = (Holding) o;
        return Objects.equals(asOfDate, holding.asOfDate) &&
                Objects.equals(businessPartnerId, holding.businessPartnerId) &&
                Objects.equals(accountId, holding.accountId) &&
                Objects.equals(accountName, holding.accountName) &&
                Objects.equals(security, holding.security) &&
                Objects.equals(position, holding.position) &&
                Objects.equals(placeOfSettlement, holding.placeOfSettlement) &&
                Objects.equals(oldAccountId, holding.oldAccountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(asOfDate, businessPartnerId, accountId, accountName, security, position, placeOfSettlement, oldAccountId);
    }
}
