package com.scb.api.client.model;


import org.joda.time.Instant;
import org.joda.time.LocalDate;

public class OpenApiPaymentInstructionInstruction {

    public enum PaymentTypePreference {Fastest, Cheapest, Explicit }

    private Instant paymentTimestamp = Instant.now();

    private PaymentTypePreference paymentTypePreference = PaymentTypePreference.Explicit; // Earlier it was fastest here
    private LocalDate requiredExecutionDate = LocalDate.now();

    private DenominatedAmount amount = DenominatedAmount.ZERO;

    private String referenceId;
    private String type;   // what is this
    private String purpose = "CASH";
    private String paymentType;

    private ExternalCodes.ChargeBearerType chargerBearer;
    private OpenApiBankParty debtor;
    private OpenApiPaymentAccount debtorAccount;
    private OpenApiAgent debtorAgent;

    private OpenApiParty creditor;
    private OpenApiAgent creditorAgent;
    private OpenApiPaymentAccount creditorAccount;

    private OpenApiRemittanceInfo remittanceInfo;
    private OpenApiRemittanceInfo relatedRemittanceInfo;

    public OpenApiPaymentInstructionInstruction() {  }

    public OpenApiPaymentInstructionInstruction(Instant paymentTimestamp, PaymentTypePreference paymentTypePreference, LocalDate requiredExecutionDate, DenominatedAmount amount, String referenceId, String type, String purpose, String paymentType, ExternalCodes.ChargeBearerType chargerBearer, OpenApiBankParty debtor, OpenApiPaymentAccount debtorAccount, OpenApiAgent debtorAgent, OpenApiParty creditor, OpenApiAgent creditorAgent, OpenApiPaymentAccount creditorAccount, OpenApiRemittanceInfo remittanceInfo, OpenApiRemittanceInfo relatedRemittanceInfo) {
        this.paymentTimestamp = paymentTimestamp;
        this.paymentTypePreference = paymentTypePreference;
        this.requiredExecutionDate = requiredExecutionDate;
        this.amount = amount;
        this.referenceId = referenceId;
        this.type = type;
        this.purpose = purpose;
        this.paymentType = paymentType;
        this.chargerBearer = chargerBearer;
        this.debtor = debtor;
        this.debtorAccount = debtorAccount;
        this.debtorAgent = debtorAgent;
        this.creditor = creditor;
        this.creditorAgent = creditorAgent;
        this.creditorAccount = creditorAccount;
        this.remittanceInfo = remittanceInfo;
        this.relatedRemittanceInfo = relatedRemittanceInfo;
    }

    public Instant getPaymentTimestamp() {
        return paymentTimestamp;
    }

    public PaymentTypePreference getPaymentTypePreference() {
        return paymentTypePreference;
    }

    public ExternalCodes.ChargeBearerType getChargerBearer() {
        return chargerBearer;
    }

    public LocalDate getRequiredExecutionDate() {
        return requiredExecutionDate;
    }

    public DenominatedAmount getAmount() {
        return amount;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public String getType() {
        return type;
    }

    public String getPurpose() {
        return purpose;
    }

    public OpenApiBankParty getDebtor() {
        return debtor;
    }

    public OpenApiAgent getDebtorAgent() {
        return debtorAgent;
    }

    public OpenApiParty getCreditor() {
        return creditor;
    }

    public OpenApiAgent getCreditorAgent() {
        return creditorAgent;
    }

    public OpenApiRemittanceInfo getRemittanceInfo() {
        return remittanceInfo;
    }

    public OpenApiRemittanceInfo getRelatedRemittanceInfo() {
        return relatedRemittanceInfo;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public OpenApiPaymentAccount getDebtorAccount() {
        return debtorAccount;
    }

    public OpenApiPaymentAccount getCreditorAccount() {
        return creditorAccount;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        public enum PaymentTypePreference {Fastest, Cheapest, Explicit }

        private Instant paymentTimestamp = Instant.now();

        private OpenApiPaymentInstructionInstruction.PaymentTypePreference paymentTypePreference = OpenApiPaymentInstructionInstruction.PaymentTypePreference.Explicit;
        private LocalDate requiredExecutionDate = LocalDate.now();

        private DenominatedAmount amount = DenominatedAmount.ZERO;

        private String referenceId;
        private String type;
        private String purpose = "CASH";
        private String paymentType;

        private ExternalCodes.ChargeBearerType chargerBearer;
        private OpenApiBankParty debtor;
        private OpenApiPaymentAccount debtorAccount;
        private OpenApiAgent debtorAgent;

        private OpenApiParty creditor;
        private OpenApiAgent creditorAgent;
        private OpenApiPaymentAccount creditorAccount;

        private OpenApiRemittanceInfo remittanceInfo;
        private OpenApiRemittanceInfo relatedRemittanceInfo;


        public OpenApiPaymentInstructionInstruction build() {
            return new OpenApiPaymentInstructionInstruction(paymentTimestamp,
                    paymentTypePreference,
                    requiredExecutionDate,
                    amount,
                    referenceId,
                    type,
                    purpose,
                    paymentType,
                    chargerBearer,
                    debtor,
                    debtorAccount,
                    debtorAgent,
                    creditor,
                    creditorAgent,
                    creditorAccount,
                    remittanceInfo,
                    relatedRemittanceInfo
            );
        }

        public Builder withPaymentTimestamp(Instant paymentTimestamp) {
            this.paymentTimestamp = paymentTimestamp;
            return this;
        }

        public Builder withPaymentTypePreference(OpenApiPaymentInstructionInstruction.PaymentTypePreference paymentTypePreference) {
            this.paymentTypePreference = paymentTypePreference;
            return this;
        }

        public Builder withRequiredExecutionDate(LocalDate requiredExecutionDate) {
            this.requiredExecutionDate = requiredExecutionDate;
            return this;
        }

        public Builder withAmount(DenominatedAmount amount) {
            this.amount = amount;
            return this;
        }

        public Builder withReferenceId(String referenceId) {
            this.referenceId = referenceId;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Builder withPurpose(String purpose) {
            this.purpose = purpose;
            return this;
        }

        public Builder withPaymentType(String paymentType) {
            this.paymentType = paymentType;
            return this;
        }

        public Builder withChargerBearer(ExternalCodes.ChargeBearerType chargeBearer) {
            this.chargerBearer = chargeBearer;
            return this;
        }
        public Builder withDebtor(OpenApiBankParty debtor) {
            this.debtor = debtor;
            return this;
        }
        public Builder withDebtorAccount(OpenApiPaymentAccount debtorAccount) {
            this.debtorAccount = debtorAccount;
            return this;
        }
        public Builder withDebtorAgent(OpenApiAgent debtorAgent) {
            this.debtorAgent = debtorAgent;
            return this;
        }

        public Builder withCreditor(OpenApiParty creditor) {
            this.creditor = creditor;
            return this;
        }

        public Builder withCreditorAgent(OpenApiAgent creditorAgent) {
            this.creditorAgent = creditorAgent;
            return this;
        }
        public Builder withCreditorAccount(OpenApiPaymentAccount creditorAccount) {
            this.creditorAccount = creditorAccount;
            return this;
        }

        public Builder withRemittanceInfo(OpenApiRemittanceInfo remittanceInfo) {
            this.remittanceInfo = remittanceInfo;
            return this;
        }
        public Builder withRelatedRemittanceInfo(OpenApiRemittanceInfo relatedRemittanceInfo) {
            this.relatedRemittanceInfo = relatedRemittanceInfo;
            return this;
        }






    }
}
