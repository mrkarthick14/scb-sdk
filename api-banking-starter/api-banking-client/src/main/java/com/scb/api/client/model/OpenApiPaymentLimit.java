package com.scb.api.client.model;

public class OpenApiPaymentLimit {
	private String maximumTransactionAmount;
	private String maximumBatchAmount;
	private String periodicity;
	
	
	
	
	public OpenApiPaymentLimit() {
	}
	public OpenApiPaymentLimit(String maximumTransactionAmount, String maximumBatchAmount, String periodicity) {
		
		this.maximumTransactionAmount = maximumTransactionAmount;
		this.maximumBatchAmount = maximumBatchAmount;
		this.periodicity = periodicity;
	}
	public String getMaximumTransactionAmount() {
		return maximumTransactionAmount;
	}
	public String getMaximumBatchAmount() {
		return maximumBatchAmount;
	}
	public String getPeriodicity() {
		return periodicity;
	}
	
	

}
