package com.scb.api.client.util;

import com.squareup.crypto.rsa.NativeRSAEngine;
import org.bouncycastle.crypto.digests.NullDigest;
import org.bouncycastle.crypto.encodings.PKCS1Encoding;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.provider.JDKDigestSignature;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import static javax.xml.bind.DatatypeConverter.parseBase64Binary;
import static javax.xml.bind.DatatypeConverter.parseHexBinary;

/**
 *
 * A standard implementation of the Encrypt/Decrypt utility class used to create requests
 * and handle responses from the Standard Chartered API Banking system.
 *
 * Note that instance methods are not intended to be thread safe due to the re-use of the expensive
 * cipher objects.  Static methods are thread safe.
 *
 */
public final class EncryptDecrypt {

    private static final byte[] IV = new byte[16];

    private static final String SYMMETRIC_KEY_ALGO = "AES";

    private static final String SYMMETRIC_CIPHER_ALGO = "AES/CBC/PKCS5Padding";

    private static final String KEY_PAIR_ALGO = "RSA";

    private static final int AES_KEY_LENGTH_IN_BITS = 256;

    private static final String UTF_8 = "UTF-8";

    private Cipher cipher;
    private Cipher aesCipher;

    public static class EncryptedContent {

        private String content;
        private String key;

        @SuppressWarnings("unused")
        public EncryptedContent() {
        }

        EncryptedContent(String content, String key) {
            this.content = content;
            this.key = key;
        }

        @SuppressWarnings("unused")
        public String getContent() {
            return content;
        }

        @SuppressWarnings("unused")
        public String getKey() {
            return key;
        }
    }

    private EncryptDecrypt() throws NoSuchAlgorithmException, NoSuchPaddingException {

        this.cipher = Cipher.getInstance(KEY_PAIR_ALGO);
        this.aesCipher = Cipher.getInstance(SYMMETRIC_CIPHER_ALGO);

        //Bouncy castle + GMP significantly improves the performance of RSA encryption due to
        //native modPow function, this will try to install the capability if available.
        BouncyCastleProvider bcProvider = (BouncyCastleProvider)Security.getProvider("BC");
        if (bcProvider == null) {
            bcProvider = new BouncyCastleProvider();
            Security.addProvider(bcProvider);
        }
        bcProvider.put("Signature.RSA", noneRSA.class.getName());

    }

    static public class noneRSA extends JDKDigestSignature
    {
        public noneRSA()
        {
            super(new NullDigest(), new PKCS1Encoding(new NativeRSAEngine()));
        }
    }


    /**
     *
     * Returns a default implementation of the Encrypt/Decrypt utility class.
     *
     * @return @EncryptDecrypt
     */
    @SuppressWarnings("unused")
    public static EncryptDecrypt getDefaultInstance() {
        try {
            return new EncryptDecrypt();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e.getMessage(),e);
        }
    }

    /**
     *
     * Returns a private key from a given file name.  This allows private keys to be easily loaded from local files
     * Whilst this is useful for testing scenarios, it is recommended to extract keys from a protected key store (e.g. HSM) for
     * production scenarios.
     *
     */
    @SuppressWarnings("unused")
    public static PrivateKey getPrivateKey(String filename) {
        try (InputStream fileIn = EncryptDecrypt.class.getClassLoader().getResourceAsStream(filename)) {
            String content = new String(toByteArray(fileIn)).replaceAll("\n","").replaceAll("\r","").replaceAll(" ","");
            return convertStringToPrivateKey(content);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static byte[] sha256Hash(byte[] content) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            return digest.digest(content);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage(),e);
        }
    }

    /**
     *
     * Returns a public key from a given file name.  This allows public keys to be easily loaded from local files
     * Whilst this is useful for testing scenarios, it is recommended to extract keys from a protected key store (e.g. HSM) for
     * production scenarios.
     *
     */
    @SuppressWarnings("unused")
    public static PublicKey getPublicKey(String filename) {
        try (InputStream fileIn = EncryptDecrypt.class.getClassLoader().getResourceAsStream(filename)) {
            String content = new String(toByteArray(fileIn));
            return convertStringToPublicKey(content);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }


    /**
     *
     * Encrypts an arbitrarily large payload using the supplied public key.  This method is used where encryption for messages
     * is required in addition to signing.  The public key to be supplied should be the Standard Chartered API banking public key,
     * accessible at https://developer.sc.com/cib/api/publicKey
     *
     */
    @SuppressWarnings("unused")
    public EncryptedContent encryptWithPublicKey(PublicKey publicKey, String content) {

        SecretKey aesKey = generateSecret();
        try {

            this.aesCipher.init(Cipher.ENCRYPT_MODE, aesKey, new IvParameterSpec(IV));
            byte[] contentEncrypted = aesCipher.doFinal(content.getBytes(UTF_8));

            byte[] pkEncAesKey = aesKey.getEncoded();
            this.cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            byte[] aesEncrypyted = this.cipher.doFinal(pkEncAesKey);

            return new EncryptedContent(
                    DatatypeConverter.printBase64Binary(contentEncrypted),
                    DatatypeConverter.printBase64Binary(aesEncrypyted)
            );

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    /**
     *
     * Decrypts an arbitrarily large payload using the supplied private key.  This method is used to decrypt messages provided by API Banking
     * either through the REST services or via a WebHook push.
     *
     */
    @SuppressWarnings("unused")
    public String decryptWithPrivateKey(PrivateKey key, EncryptedContent content) {

        try {
            this.cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] aesKey = cipher.doFinal(parseBase64Binary(content.getKey()));
            this.aesCipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(aesKey, 0, aesKey.length, SYMMETRIC_KEY_ALGO), new IvParameterSpec(IV));
            return new String(this.aesCipher.doFinal(parseBase64Binary(content.getContent())));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(),e);
        }
    }
    /**
     *
     * Converts a base64 string (e.g. from a PEM file) into a (@see #java.security.PublicKey) instance.
     *
     */
    @SuppressWarnings("unused")
    public static PublicKey convertStringToPublicKey(String KeyString) {
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(parseBase64Binary(KeyString));
        try {
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_PAIR_ALGO);
            return keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    /**
     *
     * Converts a base64 string (e.g. from a PEM file) into a (@see #java.security.PrivateKey) instance.
     *
     */
    @SuppressWarnings("unused")
    public static PrivateKey convertStringToPrivateKey(String KeyString) {
        String trimmedKey = KeyString.replace("-----END RSA PRIVATE KEY-----","").replace("-----BEGIN RSA PRIVATE KEY-----","").replace("\n","");
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(parseBase64Binary(trimmedKey));
        try {
            KeyFactory kf = KeyFactory.getInstance(KEY_PAIR_ALGO);
            return kf.generatePrivate(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private static SecretKey generateSecret() {
        return new SecretKeySpec(parseHexBinary(randomHexaKeyGenerator(AES_KEY_LENGTH_IN_BITS)), SYMMETRIC_KEY_ALGO);
    }

    public static String toString(InputStream resourceAsStream) throws IOException {
        return new String(toByteArray(resourceAsStream));
    }


    static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        copyLarge(input, output);
        return output.toByteArray();
    }

    private static void copyLarge(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[4096];
        int n1;
        for (; -1 != (n1 = input.read(buffer)); ) {
            output.write(buffer, 0, n1);
        }
    }

    private static String randomHexaKeyGenerator(int bitLength) {
        int len = bitLength >> 3;
        Random r = new Random();
        StringBuilder sb = new StringBuilder();
        while (sb.length() < len) {
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, len);
    }

}