package com.scb.api.client.model;

public class AccountIdentifier {

    public static final AccountIdentifier DEFAULT = new AccountIdentifier();

    public enum IdentifierType { BBAN, IBAN, Other }

    private String identifier;
    private IdentifierType identifierType;
    private String identifierScheme;

    public AccountIdentifier() { }

    public AccountIdentifier(String identifier, IdentifierType identifierType, String identifierScheme) {
        this.identifier = identifier;
        this.identifierType = identifierType;
        this.identifierScheme = identifierScheme;
    }

    public String getIdentifier() {
        return identifier;
    }

    public IdentifierType getIdentifierType() {
        return identifierType;
    }

    public String getIdentifierScheme() {
        return identifierScheme;
    }
}
