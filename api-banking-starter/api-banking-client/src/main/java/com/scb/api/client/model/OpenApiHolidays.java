package com.scb.api.client.model;

import com.google.common.collect.Lists;

import java.util.List;

public class OpenApiHolidays {

    private List<OpenApiHoliday> holidays;

    public OpenApiHolidays() { }

    public OpenApiHolidays(List<OpenApiHoliday> holidays) {
        this.holidays = Lists.newArrayList(holidays);
    }

    public List<OpenApiHoliday> getHolidays() {
        return holidays;
    }
}
