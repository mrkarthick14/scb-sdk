package com.scb.api.client;

import com.scb.api.client.model.*;
import com.scb.api.client.model.MetaData.MetaDataModelBuilder;
import com.scb.api.client.model.MetaData.OpenApiMetaDataId;
import com.scb.api.client.model.PaymentTemplate.OpenApiPaymentTemplateBuilder;
import com.scb.api.client.model.PaymentTemplate.OpenApiPaymentTemplateId;

import java.util.Collection;

public interface OpenAPI {

    Collection<OpenApiAccount> listAccounts(String messageId);

    Object getBalance();

    Collection<String> paymentTypes(String messageId);

    OpenApiPaymentId initiatePayment(String messageId, OpenApiPaymentInstruction paymentInstruction, boolean isPayNow);
    
    OpenApiPaymentTemplateId initiatePaymentTemplate(String messageId, OpenApiPaymentTemplateBuilder template);
    
    OpenApiMetaDataId initiateMetaSave(String messageId, MetaDataModelBuilder template);
    
    OpenApiDDIId initiateDDI(String messageId, DDIInitiateModelBuilder template);
    
    OpenApiDDAID initiateDDA(String messageId, DDAInitiateModelBuilder template);
    
    OpenApiDDAStatus DDAStatus(String messageId, String referenceId);
    
    OpenApiDDIStatus DDIStatus(String messageId, String referenceId);
    
    OpenApiPaymentStatuses  paymentStatus(String messageId, Collection<String> clientReferenceIds);

}
