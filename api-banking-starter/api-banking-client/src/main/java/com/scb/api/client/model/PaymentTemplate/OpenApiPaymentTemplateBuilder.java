package com.scb.api.client.model.PaymentTemplate;

public class OpenApiPaymentTemplateBuilder {
	private String scheme;
	private String countryCode;
	private String bicCode;
	private String transactionType;
	private String localInstrumentType;
	private String chargerBearerCode;
	private String initiatingPartyId;
	private String paymentMethod;
	private String paymentServiceLevel;
	private String paymentPriority;
	private String categoryPurpose;
	private String defaultCreditorAccountType;
	private int version;
	private String defaultCity;
	private int tsVersion;
	public OpenApiPaymentTemplateBuilder(String scheme, String countryCode, String bicCode, String transactionType,
			String localInstrumentType, String chargerBearerCode, String initiatingPartyId, String paymentMethod,
			String paymentServiceLevel, String paymentPriority, String categoryPurpose,
			String defaultCreditorAccountType, int version, String defaultCity, int tsVersion) {
		
		this.scheme = scheme;
		this.countryCode = countryCode;
		this.bicCode = bicCode;
		this.transactionType = transactionType;
		this.localInstrumentType = localInstrumentType;
		this.chargerBearerCode = chargerBearerCode;
		this.initiatingPartyId = initiatingPartyId;
		this.paymentMethod = paymentMethod;
		this.paymentServiceLevel = paymentServiceLevel;
		this.paymentPriority = paymentPriority;
		this.categoryPurpose = categoryPurpose;
		this.defaultCreditorAccountType = defaultCreditorAccountType;
		this.version = version;
		this.defaultCity = defaultCity;
		this.tsVersion = tsVersion;
	}
	
	
	
	

}
