package com.scb.api.client.model;

public class OpenApiPaymentStatus {

    private String groupId;

    private String endToEndId;

    private String clientIdentifier;

    private String paymentContainerId;

    private String statusString;
    
    private String statusCode;

    private String lastAssignee;

    private String reasonCode;

    private String reasonIsoCode;

    private String additionalInformation;

    private String timestamp;

    private String originalTransactionId;

    private String clearingSystemReference;

    private String limitBreachIndicator;

    private String reasonInformation;

    private String instructionIdentifier;

    private OpenApiPaymentInstructionContainer paymentInstructionContainer;
    
    public OpenApiPaymentStatus() {
    }

    public OpenApiPaymentStatus bindPaymentInstruction(OpenApiPaymentInstructionContainer paymentInstructionContainer) {
        this.paymentInstructionContainer = paymentInstructionContainer;
        return this;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getEndToEndId() {
        return endToEndId;
    }

    public String getClientIdentifier() {
        return clientIdentifier;
    }

    public String getPaymentContainerId() {
        return paymentContainerId;
    }

    public String getStatusString() {
        return statusString;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getLastAssignee() {
        return lastAssignee;
    }

    public String getReasonCode() {
        return reasonCode;
    }

    public String getReasonIsoCode() {
        return reasonIsoCode;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getOriginalTransactionId() {
        return originalTransactionId;
    }

    public String getClearingSystemReference() {
        return clearingSystemReference;
    }

    public String getLimitBreachIndicator() {
        return limitBreachIndicator;
    }

    public String getReasonInformation() {
        return reasonInformation;
    }

    public OpenApiPaymentInstructionContainer getPaymentInstructionContainer() {
        return paymentInstructionContainer;
    }
}
