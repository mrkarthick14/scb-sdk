package com.scb.api.client.model;

import java.time.Instant;
import java.util.List;

public class OpenApiPaymentStatusRequest {

    public enum StatusType {
        StatusOnly, StatusWithPayment
    }

    private List<String> clientReferenceIds;

    private StatusType statusType = StatusType.StatusOnly;

    private String reasonCodes = "";

    public OpenApiPaymentStatusRequest() {
    }

    public List<String> getClientReferenceIds() {
        return clientReferenceIds;
    }

    public StatusType getStatusType() {
        return statusType;
    }

    public String getReasonCodes() {
        return reasonCodes;
    }
}
