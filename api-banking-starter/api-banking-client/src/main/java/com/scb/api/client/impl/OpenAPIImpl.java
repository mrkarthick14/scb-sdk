





package com.scb.api.client.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.scb.api.client.ApiBankingException;
import com.scb.api.client.OpenAPI;
import com.scb.api.client.model.*;
import com.scb.api.client.model.MetaData.MetaDataModelBuilder;
import com.scb.api.client.model.MetaData.OpenApiMetaDataId;
import com.scb.api.client.model.PaymentTemplate.OpenApiPaymentTemplateBuilder;
import com.scb.api.client.model.PaymentTemplate.OpenApiPaymentTemplateId;
import com.scb.api.client.util.HttpUtility;
import com.scb.api.client.util.ObjectMapperProvider;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

public class OpenAPIImpl implements OpenAPI {

    private static final String GET_BALANCE = "https://apitest.standardchartered.com/uat2/openapi/accounts/v2/0101132824";

    private static final String URI_LIST_ACCOUNTS = "/scb/cib/account";
    private static final String URI_PAYMENT_INITIATE = "/uat2/openapi/payments/v2/fast/initiate";       //working
    private static final String URI_PAYNOW_PAYMENT_INITIATE = "/uat2/openapi/payments/v2/paynow/initiate";
    private static final String URI_PAYMENT_STATUS = "/uat2/openapi/payment/v2/status";           //Working
    private static final String URI_PAYMENT_TEMPLATE = "/uat/openapi/payment/template/save"; //Working
    private static final String URI_eDDA_INITIATE = "/uat2/openapi/collections/v1/dda/initiate"; //Working
    private static final String URI_META_SAVE = "/uat/openapi/meta/save";          //Working
    private static final String URI_eDDI_INITIATE = "/uat2/openapi/collections/v1/ddi/initiate"; //Working
    private static final String URI_eDDA_STATUS = "/uat2/openapi/collections/v1/dda/status";  //Working
    private static final String URI_eDDI_STATUS = "/uat2/openapi/collections/v1/ddi/status";  //Working
    private HttpUtility httpUtility;

    private ObjectMapper objectMapper = ObjectMapperProvider.objectMapper();
    private boolean encryptedResponse;

    OpenAPIImpl(HttpUtility httpUtility,boolean encryptedResponse) {
        this.httpUtility = httpUtility;
        this.encryptedResponse = encryptedResponse;
        
    }
    
    

    @Override
    public Collection<OpenApiAccount> listAccounts(String messageId) {
        InputStream response = httpUtility.performGet(URI_LIST_ACCOUNTS, encryptedResponse,messageId, null);
        try {
            //noinspection ConstantConditions
            return Arrays.asList(objectMapper.readValue(response, OpenApiAccount[].class));
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
    }

    @Override
    public Object getBalance() {
        InputStream response = httpUtility.performGet(GET_BALANCE, encryptedResponse, UUID.randomUUID().toString(), null);
        try {
            return Arrays.asList(objectMapper.readValue(response, OpenApiAccount[].class));
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }    }

    @Override
    public Collection<String> paymentTypes(String messageId) {
        InputStream response = httpUtility.performGet(URI_LIST_ACCOUNTS, encryptedResponse, messageId, null);
        try {
            //noinspection ConstantConditions
            return Arrays.asList(objectMapper.readValue(response, String[].class));
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
    }


    @Override
    public OpenApiPaymentId initiatePayment(String messageId, OpenApiPaymentInstruction paymentInstruction, boolean isPayNow) {
    	
        InputStream response = httpUtility.performPost(uriPaymentInitiate(isPayNow), messageId, encryptedResponse, paymentInstruction, null);

//    InputStreamReader isReader = new InputStreamReader(response);
//      BufferedReader reader = new BufferedReader(isReader);
//      StringBuffer sb = new StringBuffer();
//      String str;
       
        
        try {
//        	while((str = reader.readLine())!=null){
//          		sb.append(str);
//           	}
//           	System.out.println("InputStream to string " + sb.toString());
            return objectMapper.readValue(response, OpenApiPaymentId.class);
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
    }

    private String uriPaymentInitiate(boolean isPayNow) {
        return isPayNow ? URI_PAYNOW_PAYMENT_INITIATE : URI_PAYMENT_INITIATE;
    }

    @Override
    public OpenApiPaymentStatuses paymentStatus(String messageId, Collection<String> clientReferenceIds) {
        InputStream response = httpUtility.performPost(URI_PAYMENT_STATUS, messageId, encryptedResponse, new OpenApiPaymentClientReferences(Lists.newArrayList(clientReferenceIds)), null);
        
        try {
            return objectMapper.readValue(response, OpenApiPaymentStatuses.class);
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
    }



	@Override
	public OpenApiPaymentTemplateId initiatePaymentTemplate(String messageId, OpenApiPaymentTemplateBuilder template) {
		 InputStream response = httpUtility.performPost(URI_PAYMENT_TEMPLATE, messageId, encryptedResponse, template, null);
		 
	        try {
	        	
	            return objectMapper.readValue(response, OpenApiPaymentTemplateId.class);
	        } catch (IOException e) {
	            throw new ApiBankingException(e);
	        }
	}

	@Override
	public OpenApiMetaDataId initiateMetaSave(String messageId, MetaDataModelBuilder template) {
		 InputStream response = httpUtility.performPost(URI_META_SAVE, messageId, encryptedResponse, template, null);
		 
	        try {
	        	
	            return objectMapper.readValue(response, OpenApiMetaDataId.class);
	        } catch (IOException e) {
	            throw new ApiBankingException(e);
	        }
	}


	@Override
	public OpenApiDDAID initiateDDA(String messageId, DDAInitiateModelBuilder template) {
		InputStream response = httpUtility.performPost(URI_eDDA_INITIATE, messageId, encryptedResponse, template, null);
//		InputStreamReader isReader = new InputStreamReader(response);
//	      BufferedReader reader = new BufferedReader(isReader);
//	      StringBuffer sb = new StringBuffer();
//	      String str;
        try {
//        	while((str = reader.readLine())!=null){
//          		sb.append(str);
//           	}
//           	System.out.println("InputStream to string " + sb.toString());
//        	
            return objectMapper.readValue(response, OpenApiDDAID.class);
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
	}



	@Override
	public OpenApiDDIId initiateDDI(String messageId, DDIInitiateModelBuilder template) {
		InputStream response = httpUtility.performPost(URI_eDDI_INITIATE, messageId, encryptedResponse, template, null);
		 
        try {
        	
            return objectMapper.readValue(response, OpenApiDDIId.class);
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
	}



	@Override
	public OpenApiDDAStatus DDAStatus(String messageId, String referenceId) {
		
		
		
		InputStream response = httpUtility.performGet(modifyURI(URI_eDDA_STATUS,referenceId), encryptedResponse, messageId, null);
        try {
            //noinspection ConstantConditions
            return (objectMapper.readValue(response, OpenApiDDAStatus.class));
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
	}
	
	public String modifyURI(String uri, String newString){
		return String.format("%s/%s",uri,newString);
	}



	@Override
	public OpenApiDDIStatus DDIStatus(String messageId, String referenceId) {
		InputStream response = httpUtility.performGet(modifyURI(URI_eDDI_STATUS,referenceId), encryptedResponse, messageId, null);
        try {
            //noinspection ConstantConditions
            return (objectMapper.readValue(response, OpenApiDDIStatus.class));
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
	}







//	@Override
//	public OpenApiPaymentTemplateId initiateMetaSave(String messageId, String template) {
//		InputStream response = httpUtility.performPost(URI_META_SAVE, messageId, encryptedResponse, template, null);
//		 
//        try {
//        	
//            return objectMapper.readValue(response, OpenApiPaymentTemplateId.class);
//        } catch (IOException e) {
//            throw new ApiBankingException(e);
//        }
//	}

}
