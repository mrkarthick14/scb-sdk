package com.scb.api.client.model;


import java.util.UUID;

import org.joda.time.Instant;

public class OpenApiPaymentInstructionHeader {

    private String messageSender = "client-sdk";
    private String messageId = UUID.randomUUID().toString();
    private String countryCode = "IN";
    private Instant timestamp = Instant.now();

    public OpenApiPaymentInstructionHeader() { 
    	
    }

    public OpenApiPaymentInstructionHeader(String messageSender, String messageId, String countryCode, Instant timestamp) {
        this.messageSender = messageSender;
        this.messageId = messageId;
        this.countryCode = countryCode;
        this.timestamp = timestamp;
    }

    public String getMessageSender() {
        return messageSender;
    }

    public String getMessageId() {
        return messageId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public Instant getTimestamp() {
        return timestamp;
    }
    
    public OpenApiPaymentInstructionHeader build(){
    	return new OpenApiPaymentInstructionHeader(messageSender, messageId, countryCode,timestamp);
    }
}
