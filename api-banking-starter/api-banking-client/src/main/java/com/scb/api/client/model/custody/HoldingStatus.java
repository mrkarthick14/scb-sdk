package com.scb.api.client.model.custody;

import java.util.Objects;

public class HoldingStatus {

    private String status;
    private String scaId;
    private String date;

    public String getStatus() {
        return status;
    }

    public String getScaId() {
        return scaId;
    }

    public String getDate() {
        return date;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HoldingStatus that = (HoldingStatus) o;
        return Objects.equals(status, that.status) &&
                Objects.equals(scaId, that.scaId) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(status, scaId, date);
    }
}
