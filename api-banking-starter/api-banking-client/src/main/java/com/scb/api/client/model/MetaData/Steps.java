package com.scb.api.client.model.MetaData;



public class Steps {
	private DataFilter dataFilter;
	private RequestTransform requestTransform;
	private Delivery delivery;
	private ResponseTransform responseTransform;
	public Steps(DataFilter dataFilter, RequestTransform requestTransform, Delivery delivery,
			ResponseTransform responseTransform) {
		
		this.dataFilter = dataFilter;
		this.requestTransform = requestTransform;
		this.delivery = delivery;
		this.responseTransform = responseTransform;
	}
	
	
}
