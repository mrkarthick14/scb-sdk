package com.scb.api.client.model;

import java.security.PrivateKey;
import java.util.HashMap;
import java.util.Map;

public class JWTTokenPrototype<T> {

    private T payload;
    private String messageId;
    private PrivateKey privateKey;
    private String issuer;

    public static JWTTokenPrototype<Map> createEmpty(String issuerId, String messageId, PrivateKey key) {
        return create((Map)new HashMap(), issuerId, messageId, key);
    }

    public static <T> JWTTokenPrototype<T> create(T payload, String issuer, String messageId, PrivateKey privateKey) {
        JWTTokenPrototype<T> proto = new JWTTokenPrototype<>();
        proto.payload = payload;
        proto.issuer = issuer;
        proto.messageId = messageId;
        proto.privateKey = privateKey;
        
        return proto;
    }

    public T getPayload() {
        return payload;
    }

    public String getMessageId() {
        return messageId;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

}
