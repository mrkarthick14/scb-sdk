package com.scb.api.client.model.custody;

import java.util.List;
import java.util.Objects;

public class Holdings {

    private ResourceHeader header;
    private List<Holding> holdings;

    public List<Holding> getHoldings() {
        return holdings;
    }

    public ResourceHeader getHeader() {
        return header;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Holdings holdings1 = (Holdings) o;
        return Objects.equals(header, holdings1.header) &&
                Objects.equals(holdings, holdings1.holdings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, holdings);
    }
}
