package com.scb.api.client.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.hash.Hashing;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.ApiBankingException;
import com.scb.api.client.model.JWTTokenPrototype;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.PrivateKeyDetails;
import org.apache.http.conn.ssl.PrivateKeyStrategy;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HttpUtility {

    private static final Logger logger = LoggerFactory.getLogger(HttpUtility.class);
    public static final String CLIENT_CERT_ALIAS = "1";

    private HttpClient httpClient;
    private String issuerId;
    private KeyStore keyStore;
    private final String signatureKeyAlias;
    private final char[] keyPassword;
    private ApiBankingEnvironment environment;
    private final ObjectMapper objectMapper;

    public HttpUtility(HttpClient httpClient, String issuerId, KeyStore keyStore, String signatureKeyAlias, char[] keyPassword,
                       ObjectMapper objectMapper,
                       ApiBankingEnvironment environment) throws KeyStoreException {
        this.httpClient = httpClient;
        this.issuerId = issuerId;
        this.keyStore = keyStore;
        this.objectMapper = objectMapper;
        this.signatureKeyAlias = signatureKeyAlias;
        this.keyPassword = keyPassword;
        this.environment = environment;

        Certificate cert = keyStore.getCertificate(signatureKeyAlias);
        // Get public key
        PublicKey publicKey = cert.getPublicKey();
        logger.info("Signing public key hash: " +
                addColons(
                        Hashing.sha256().hashBytes(publicKey.getEncoded()).toString()));

    }

    public InputStream performGet(String uri, boolean encryptedResponses, String messageId, Map<String,String> headers) {

        HttpResponse response;
        try {

            String token = JWT.createToken(
                    JWTTokenPrototype.createEmpty(issuerId, messageId, (PrivateKey) keyStore.getKey(signatureKeyAlias, keyPassword)));

            HttpGet httpGet = new HttpGet(environment.urlOfPath(uri));
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            httpGet.setHeader("JWTToken", token);

            applyEncryptionHeaderIfRequired(encryptedResponses, httpGet);
            applyParameterHeaders(headers, httpGet);
             
            response = httpClient.execute(httpGet);
         
            logger.info("Received status " + response.getStatusLine().getStatusCode());

            if (response.getStatusLine().getStatusCode() != 200) {
                //noinspection ConstantConditions
                throw new ApiBankingException(response.getStatusLine().getStatusCode(),
                        response.getStatusLine().getReasonPhrase() + "/" + IOUtils.toString(response.getEntity().getContent()));
            }
            return encryptedResponses ? decrypt(response.getEntity().getContent()) : response.getEntity().getContent();

        } catch (Exception e) {
            throw new ApiBankingException(e);
        }

    }

    public <T> InputStream performPost(String uri, String messageId, boolean encryptedResponses, T requestBody, Map<String,String> headers) {
        HttpResponse response;
        try {

        	  
            String token = JWT.createToken(
                    JWTTokenPrototype.create(requestBody, issuerId, messageId, (PrivateKey) keyStore.getKey(signatureKeyAlias, keyPassword)));
            
          
            HttpPost httpPost = new HttpPost(environment.urlOfPath(uri));
       
            httpPost.setEntity(new StringEntity(token));

            applyEncryptionHeaderIfRequired(encryptedResponses, httpPost);
            applyParameterHeaders(headers, httpPost);
            
            
            
            response = httpClient.execute(httpPost);
            
            
            if (response.getStatusLine().getStatusCode() != 200) {
                //noinspection ConstantConditions
            	
                throw new ApiBankingException(response.getStatusLine().getStatusCode(),
                        response.getStatusLine().getReasonPhrase() + "/" + IOUtils.toString(decrypt(response.getEntity().getContent())));
            }

            return encryptedResponses ? decrypt(response.getEntity().getContent()) : response.getEntity().getContent();

        } catch (Exception e) {
            throw new ApiBankingException(e);
        }

    }

    private InputStream decrypt(InputStream content) {
        EncryptDecrypt.EncryptedContent encContent = null;
        try {
            byte[] ec = IOUtils.toByteArray(content);
            if (ec.length > 0) {
                encContent = objectMapper.readValue(ec, EncryptDecrypt.EncryptedContent.class);
                PrivateKey key = (PrivateKey) keyStore.getKey(signatureKeyAlias, keyPassword);
                return new ByteArrayInputStream(EncryptDecrypt.getDefaultInstance().decryptWithPrivateKey(key, encContent).getBytes());
            } else {
                return new ByteArrayInputStream(new byte[0]);
            }
        } catch (IOException | UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
            throw new RuntimeException(e.getMessage(),e);
        }
    }

    private void applyParameterHeaders(Map<String, String> headers, HttpRequestBase httpPost) {

        if (headers == null) {
            return;
        }

        for(Map.Entry<String,String> header : headers.entrySet()) {
            httpPost.addHeader(header.getKey(), header.getValue());
        }
    }

    private void applyEncryptionHeaderIfRequired(boolean encrypted, HttpRequestBase builder) {
        if (encrypted) {
            builder.addHeader("ResponseEncryptionType", "AES256Signed");
        }
    }

    public static HttpClientBuilder safeHttpsBuilder(
            KeyStore keyStore,
            String clientCertPassword,
            HttpClientBuilder httpClientBuilder) throws NoSuchAlgorithmException, KeyManagementException {

        SSLContext sslContext = null;
        try {
            X509Certificate cert = (X509Certificate)keyStore.getCertificate(CLIENT_CERT_ALIAS);
            if (cert == null) {
                throw new KeyManagementException("No key alias '" + CLIENT_CERT_ALIAS + "' found in key store, cannot authenticate to server");
            }
            logger.info("Authenticating with certificate " + addColons(Hashing.sha1().hashBytes(cert.getEncoded()).toString()));
            sslContext = SSLContexts
                    .custom()
                    // load identity keystore
                    .loadKeyMaterial(keyStore, clientCertPassword.toCharArray(), new PrivateKeyStrategy() {
                        @Override
                        public String chooseAlias(Map<String, PrivateKeyDetails> aliases, Socket socket) {
                            return CLIENT_CERT_ALIAS;
                        }
                    })
                    // load trust keystore
//                    .loadTrustMaterial(trustKeyStore, null)
                    .build();
        } catch (CertificateEncodingException | KeyStoreException | UnrecoverableKeyException e) {
            throw new KeyManagementException(e.getMessage(),e);
        }

        SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(
                sslContext,
                new String[]{"TLSv1.2", "TLSv1.1"},
                null,
                SSLConnectionSocketFactory.STRICT_HOSTNAME_VERIFIER);

        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("https", sslConnectionSocketFactory)
                .register("http", new PlainConnectionSocketFactory())
                .build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);

        connectionManager.setMaxTotal(40);
        connectionManager.setDefaultMaxPerRoute(5);

        return httpClientBuilder.setConnectionManager(connectionManager);


    }

    private static String addColons(String s) {
        StringBuilder sb = new StringBuilder();
        for(int i=0;i<s.length();i+=2) {
            sb.append(s.substring(i,i+2));
            sb.append(":");
        }
        return sb.substring(0,sb.length()-1);
    }
}
