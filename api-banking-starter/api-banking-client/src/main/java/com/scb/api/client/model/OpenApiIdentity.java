package com.scb.api.client.model;

public class OpenApiIdentity {

    private String id;
    private String identityType;

    public OpenApiIdentity() {
    }

    public OpenApiIdentity(String id, String identityType) {
        this.id = id;
        this.identityType = identityType;
    }

    public String getId() {
        return id;
    }

    public String getIdentityType() {
        return identityType;
    }
}
