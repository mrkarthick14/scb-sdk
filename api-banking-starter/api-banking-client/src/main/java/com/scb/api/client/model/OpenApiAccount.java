package com.scb.api.client.model;

import java.util.Collection;

public class OpenApiAccount {

    private String id;
    private String label;
    private String number;
    private Collection<OpenApiAccountOwner> owners;
    private String type;
    private OpenApiAccountBalance balance;
    private String IBAN;
    private String swiftBic;
    private String bankId;
    private String currency;
    private String groupId;
    private String country;

    public OpenApiAccount(String id, String label, String number, Collection<OpenApiAccountOwner> owners, String type, OpenApiAccountBalance balance, String IBAN, String swift_bic, String bank_id,
                          String currency, String groupId, String country) {
        this.id = id;
        this.label = label;
        this.number = number;
        this.owners = owners;
        this.type = type;
        this.balance = balance;
        this.IBAN = IBAN;
        this.swiftBic = swift_bic;
        this.bankId = bank_id;
        this.currency = currency;
        this.groupId=groupId;
        this.country=country;
    }

    public String getCurrency() {
        return currency;
    }

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getNumber() {
        return number;
    }

    public Collection<OpenApiAccountOwner> getOwners() {
        return owners;
    }

    public String getType() {
        return type;
    }

    public OpenApiAccountBalance getBalance() {
        return balance;
    }

    public String getIBAN() {
        return IBAN;
    }

    public String getSwiftBic() {
        return swiftBic;
    }

    public String getBankId() {
        return bankId;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getCountry() {
        return country;
    }
}
