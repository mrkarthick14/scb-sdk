package com.scb.api.client.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Maps {

    public static <K,V> Map<K,V> map(K key, V value) {
        Map<K,V> res = new HashMap<>();
        res.put(key,value);
        return res;
    }

    public static <K,V> Map<K,V> map(K k1, V v1,K k2, V v2) {
        Map<K,V> res = map(k1,v1);
        res.put(k2,v2);
        return res;
    }

    public static <K,V> Map<K,V> map(K k1, V v1,K k2, V v2,K k3, V v3) {
        Map<K,V> res = map(k1,v1,k2,v2);
        res.put(k3,v3);
        return res;
    }

    public static <K,V> List<Map<K, V>> toList(Map[] maps) {
        List<Map<K,V>> list = new ArrayList<>();
        for(Map m : maps) {
            list.add(m);
        }
        return list;
    }



}
