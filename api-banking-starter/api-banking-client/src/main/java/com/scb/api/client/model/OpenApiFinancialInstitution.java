
package com.scb.api.client.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OpenApiFinancialInstitution {

    @JsonProperty("BIC")
    private String BIC;
    private OpenApiAddress postalAddress;
    private String branchCode;
    private String name;


    /**
     * No args constructor for use in serialization
     * 
     */
    public OpenApiFinancialInstitution() {
    }

    /**
     * 
     * @param bic
     * @param postalAddress
     */
    public OpenApiFinancialInstitution(String bic, String branchCode, OpenApiAddress postalAddress, String name) {
        super();
        this.BIC = bic;
        this.branchCode = branchCode;
        this.postalAddress = postalAddress;
        this.name = name;
    }

    @JsonProperty("BIC")
    public String getBIC() {
        return BIC;
    }

    @JsonProperty("BIC")
    public void setBIC(String bic) {
        this.BIC = bic;
    }

    public OpenApiAddress getPostalAddress() {
        return postalAddress;
    }

    public void setPostalAddress(OpenApiAddress postalAddress) {
        this.postalAddress = postalAddress;
    }


    public String getBranchCode() {
        return branchCode;
    }

    public String getName() {
        return name;
    }
}
