package com.scb.api.client.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class OpenApiAccountBalance {

    private String currency;
    private LocalDate asOf;
    private BigDecimal amount;
    private BigDecimal opening;
    private BigDecimal closing;
    private BigDecimal openingLedger;
    private BigDecimal closingLedger;

    public OpenApiAccountBalance(String currency, LocalDate asOf,
                                 BigDecimal amount, BigDecimal opening, BigDecimal closing,
                                 BigDecimal openingLedger, BigDecimal closingLedger) {
        this.currency = currency;
        this.asOf = asOf;
        this.amount = amount;
        this.opening = opening;
        this.closing = closing;
        this.openingLedger = openingLedger;
        this.closingLedger = closingLedger;

    }

    public LocalDate getAsOf() {
        return asOf;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getOpening() {
        return opening;
    }

    public BigDecimal getClosing() {
        return closing;
    }

    public BigDecimal getOpeningLedger() {
        return openingLedger;
    }

    public BigDecimal getClosingLedger() {
        return closingLedger;
    }
}
