package com.scb.api.client.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import static com.scb.api.client.util.EncryptDecrypt.toByteArray;
import static javax.xml.bind.DatatypeConverter.parseBase64Binary;

public class SignatureExample {

    private static final String SAMPLE_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEA3m96qPVriqrOjW+dnkhbaX7eZ8eX49Oq07DEC+ajkEy7DGYP\n" +
            "7+dw5R6JXs76uImAHe2utH1EY8X5NyROksquRJt+E/G5LDFNV3oGMeS/WDMCnkce\n" +
            "JGUWWc/t0uPaKtql7YW6hSsvABReK+2Apory4szT2HW8juYi4dGpOQf3Y6u02sTn\n" +
            "TJMZ2o6wyQNXDyqQki0xUy7ikrCLX8L0XRyJaK+L/DX+JsZm9qNJ4i/rny6KnXBM\n" +
            "2VpZCadqMD9+IRSMFvOBSUnkV1mNDvtJxWNjTLlbizt+lrjpIBevPMVGNk/BLWOm\n" +
            "3qRd2U0oB8A4xGtoYY1Qoy912zOfP8gbzbzucQIDAQABAoIBAE/2s5nZ4HdRQEAI\n" +
            "SBmG2ZXrTfjw/+GfTmIbwd6lY1eUvgmPB1uvUB6OA8RZlDiS9cPMlmm8PsG/ggAY\n" +
            "xUZwJoXzgDebRn5kmvE/pMp6+W6zp9VOq79Zt9dBsLWHw/5z7AKfeJY3GAZnTBNu\n" +
            "pjk46mguSloeuVKF2TRlMviVrJHflNYxuAjViQaaKvBqsi5rhljHyIfceCf2Oj9g\n" +
            "bTqNtwsxqV6wRk/uJmHYNeMuU3DfZ9kt3sNAEG58i44IVyz6H1rFHY6bcJqAyfaa\n" +
            "eh53vZEE0tLYuTwxvOVvHkBpOgGoWL2UCITQ5bkEgHOKTNmGHpxnKRPRVhrLz0Yr\n" +
            "0djTs4ECgYEA9e70O1v3PfhQGky5f1uashD6tKNpgOZGBpYII82zlJ2rY6kZDsvr\n" +
            "ll9xNecyW3wwICX5quwk4e+rHKktyrtmC4B+490cWzuQL5+q4RSzHi1fshzxZcmq\n" +
            "Rm5KTdF0BLNpWArXtlXgFFv2o8uX6N18NFYT5M5N0l+7dYFO312YxhUCgYEA54pM\n" +
            "eZTXH3fPpvk6baNrIlkOqKzvXflzcru0l8O1QsG1F16+TdeiKCBLdrFRSeiMRd0x\n" +
            "WjMcY+gJDxYFj7MIVnY+dLNF+Hv/6BcI8nWO2KEbNN2Ed6v+G4g1IZ6yafzrg2jR\n" +
            "lrq8AFz6GOEzsgBI6RYahjmagu08a/X8fuL/me0CgYB5ufpc3xozLBLJ0awG45pg\n" +
            "BJNykWeJC3EhlWUWA5ydNKU+QNZMQpaq6TapzXNZYNCZOPKorRVcVzpEh2BI9hR+\n" +
            "IO2tbyW36nVlfQwhOBOoHiE9kaHSFc0X60MP3VolcX/0ufknIL42lYYwcHcSJX5y\n" +
            "kSBroclXFVc9w/D8EeGqOQKBgQCUbHAnhAT+1BPVym/s9lPn+cTO1cS/rfeWtcLH\n" +
            "gppAR+IpL/+38KGvLTySwooEYcEBzBAl7Rum44hjFk/MVQzkN/pDXuRcrEhYY+PF\n" +
            "YRyHu4awNcwD1soWphLBEXT3Qj1KyLeJ8vwvE2ry5fH4ifwzBotMPcLRaPhRkKci\n" +
            "Q6tr5QKBgHOr2Xr9w728fiLqx8nvd28ZgQXP0WijKjIFx4/qv8YyFviXjKf8ak07\n" +
            "blqe5z8e4+HJ0Ue3dQuoj+0b2+PlEO2S5eR7lGcsfjQ5kCr+XJUWKYgcqMClonBm\n" +
            "GoQxcHRqDm9IBj5L75W96pC5XhJbSI23zeqJqZMQoC38HQFy5PhC\n" +
            "-----END RSA PRIVATE KEY-----\n";

    private static final String SCB_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt1AbpH2nyeX3oh79GhtmC7lQHmdBq4E3xpATu0Kj2VudHI83wqKhMPic7Dc/XLmQ7AyuLmDj5mTxvj70zBdlofetmXy0aCXEE5lkl3pHaqkrSMhRozuni4rF74VvaPvvaRG9V+zAEzKs6jV713x81m94A86sREu+ZGkmzUMhFLYHXizoaULkzVaJCFjsWLRzSF+lt50QUbQGKghU0Zh1vIN+noU9EmdioHpQxu5w2Pap3KCErHePkVLukUqZR0VmuG+ScXbgQhqX6y7z6cL1Gaa1x6262gewLvQ4piQQqvSp8Hn7ogyYdXcoVpuT89Hgep57IIwfzxDGBemr4cnerQIDAQAB";

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException {

        String sampleBody = "{\"content\":\"dJVKBA0NzixZwjdY2EuW4HaLRVbDCJb2SVXEarJgx6w=\",\"key\":\"G14UDvEDkIUzVzP2nLXrKTOByk3GOb9/cQe+rIRlj61nm4ZfKvseXDOUePDZvmtb/3PUZYADklXWNUglQ8epOyhoqhA/JCEeYUabVaCgv5M2+05kjnQvkNEAOhnG0jqkFIsimLLLRRwSd/eOkkTr7AgqhMnwxsVeMF6O3zHWyVAw56+jokHz+G/nJUzOX6hMOrk5I1qRWK1WmY9SWxXYMGVNyjHa8TJSyGeQ5C7rhIS4Gz6SNUyRubSpUFC21T605qauj2f8tQzM+JpXG6ks/OzwWlMX8SPACIGaKZVOkSsxzJyASy2lTtVujO700YU9kAMpSwDXbNfzn+95ZcmtUA==\"}";
        String sig = "5D5862356F50B6EF655943ED63AED35592600A476ED76887BA815CC9E4D1E867.OqcgbNsSo7EIS7IH+vtmIAIcSbvG/gQN37dsclyIQHsh72S+/1Ah0uyHhQrSJB64BJyHaemajKC+n2kDD/EovplIDVHO5wILqhfJdyBqplY3Vs5O5mZDiDV0X47Qe7mEeI+SGsAzMCcsgRMCIfUDfE9fD4Tta1KN/atOWwtYDR/sTV+ByECnW9CzJzNVfqgmvzeRZ7LTpe1BOqChxu1M4x7PydnIAkHTcxzX7Ixf+Yg+RKE5/bYs08eE2TwUDVKOIi5E/0OrctlCZ6pXp8Ic0WOXvSjwCL8x1l2Bi8XzfujYcgwtpjuAD6XtR3y4OMxtS0PYUBiSlt3jB3Rk4bJ/nw==";

        String content = EncryptDecrypt.getDefaultInstance().decryptWithPrivateKey(EncryptDecrypt.convertStringToPrivateKey(SAMPLE_PRIVATE_KEY),
                new ObjectMapper().readValue(sampleBody, EncryptDecrypt.EncryptedContent.class));

        verifySignature(content, EncryptDecrypt.convertStringToPublicKey(SCB_PUBLIC_KEY), sig);

    }

    @SuppressWarnings("unused")
    public static PublicKey getPublicKey(String filename) {
        try {
            System.out.println(filename);
            InputStream fileIn = SignatureExample.class.getClassLoader().getResourceAsStream("publicKey");
            System.out.println("file is" + fileIn);
            try {
                String content = new String(toByteArray(fileIn));
                return convertStringToPublicKey(content);
            } finally {
                fileIn.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public static boolean verifySignature(String decryptedContent, PublicKey scbPublicKey, String signatureHeaderValue) throws NoSuchProviderException, NoSuchAlgorithmException, SignatureException, InvalidKeyException {
        if (signatureHeaderValue.contains(".")) {
            byte[] hash = EncryptDecrypt.sha256Hash(decryptedContent.getBytes());
            Signature sig = Signature.getInstance("SHA1withRSA", "SunRsaSign");
            sig.initVerify(scbPublicKey);
            sig.update(hash);
            return sig.verify(parseBase64Binary(signatureHeaderValue.split("\\.")[1]));
        } else {
            throw new IllegalArgumentException("Signature header should be of the form [HEX].[BASE64]");
        }
    }

    public static PublicKey convertStringToPublicKey(String KeyString) throws InvalidKeySpecException {
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(parseBase64Binary(KeyString));
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static void status(String content) throws Exception {
        String signature = "cxBvLXRTR9FgptNtdu36E5lW6+2SHkfaxWG2fNh53NJsHtrpn+SkMvXOkmumx7SgMpWSaioH8aieMCmAe2unABbgUoBBqifjl2d1AwxVG1zbSAozcVW+jQRJN1XM1kCXfid/vdY/p3uLAIOZyIp5tGHjnrwlq+4ggZQojT/KX9eRl8fsSu8F1d7Mvisci1eeY/nxeHQKqyQIRMI+EXIx29YVPHZnDecippaLjinmHex85br/Uc9pPulz8jggTx5zekgQWCZYbVoR1LCUEJbpOCpiibAKVHteUwW0vU6jq+v6Uda6UIhcNgG7Jg2/R/HSmtjMpmzJTkUfDNDuu+nb5Q==";
        System.out.println(content);
        PublicKey publicKey = getPublicKey("/home/sahil/Desktop/SCB/publicKey");
        Boolean status = verifySignature(content, publicKey, signature);
        System.out.println(status + "----------");
    }

}

