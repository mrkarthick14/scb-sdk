package com.scb.api.client.model;

import java.util.List;

public class OpenApiAccounts {

    private List<OpenApiAccount> accounts;

    public OpenApiAccounts(List<OpenApiAccount> accounts) {
        this.accounts = accounts;
    }

    public List<OpenApiAccount> getAccounts() {
        return accounts;
    }
}
