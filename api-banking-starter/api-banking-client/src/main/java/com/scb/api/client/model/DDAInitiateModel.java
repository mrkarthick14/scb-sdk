package com.scb.api.client.model;

import org.joda.time.LocalDate;

import com.scb.api.client.model.OpenApiAgent;
import com.scb.api.client.model.OpenApiBankParty;
import com.scb.api.client.model.OpenApiPaymentAccount;

public class DDAInitiateModel {
	private String referenceId;
	private OpenApiPaymentAccount debtorAccount;
	private OpenApiPaymentAccount creditorAccount;
	private OpenApiAgent debtorAgent;
	private OpenApiAgent creditorAgent;
	private OpenApiBankParty creditor;
	private OpenApiBankParty debtor;
	private OpenApiPaymentLimit paymentLimit;
	private LocalDate expiryDate;
	private LocalDate effectiveDate;
	private String debtorReference;
	private String currencyCode;
	private String mandateType;
	private String type;
	
	public DDAInitiateModel(String referenceId) {
		this.referenceId = referenceId;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public OpenApiPaymentAccount getDebtorAccount() {
		return debtorAccount;
	}
	public OpenApiPaymentAccount getCreditorAccount() {
		return creditorAccount;
	}
	public OpenApiAgent getDebtorAgent() {
		return debtorAgent;
	}
	public OpenApiAgent getCreditorAgent() {
		return creditorAgent;
	}
	public OpenApiBankParty getCreditor() {
		return creditor;
	}
	public OpenApiBankParty getDebtor() {
		return debtor;
	}
	public OpenApiPaymentLimit getPaymentLimit() {
		return paymentLimit;
	}
	public LocalDate getExpiryDate() {
		return expiryDate;
	}
	public LocalDate getEffectiveDate() {
		return effectiveDate;
	}
	public String getDebtorReference() {
		return debtorReference;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public String getMandateType() {
		return mandateType;
	}
	public String getType() {
		return type;
	}
	
	public static DDAInitiateModel withReferenceId(String referenceId){
		return new DDAInitiateModel(referenceId);
	}
	
	public DDAInitiateModel withDebtorAccount(OpenApiPaymentAccount debtorAccount){
		this.debtorAccount = debtorAccount;
		return this;
	}
	
	public DDAInitiateModel withCreditorAccount(OpenApiPaymentAccount creditorAccount){
		this.creditorAccount = creditorAccount;
		return this;
	}
	
	
	public DDAInitiateModel withDebtorAgent(OpenApiAgent debtorAgent){
		this.debtorAgent = debtorAgent;
		return this;
	}
	public DDAInitiateModel withCreditorAgent(OpenApiAgent creditorAgent){
		this.creditorAgent = creditorAgent;
		return this;
	}
	public DDAInitiateModel withCreditor(OpenApiBankParty creditor){
		this.creditor = creditor;
		return this;
	}
	
	public DDAInitiateModel withDebtor(OpenApiBankParty debtor){
		this.debtor = debtor;
		return this;
	}
	
	public DDAInitiateModel withPaymentLimit(OpenApiPaymentLimit paymentLimit){
		this.paymentLimit = paymentLimit;
		return this;
	}
	
	public DDAInitiateModel withExpiryDate(LocalDate expiryDate){
		this.expiryDate = expiryDate;
		return this;
	}
	public DDAInitiateModel withEffectiveDate(LocalDate effectiveDate){
		this.effectiveDate = effectiveDate;
		return this;
	}
	public DDAInitiateModel withDebtorReference(String debtorReference){
		this.debtorReference = debtorReference;
		return this;
	}
	public DDAInitiateModel withCurrencyCode(String currencyCode){
		this.currencyCode = currencyCode;
		return this;
	}
	public DDAInitiateModel withMandateType(String mandateType){
		this.mandateType = mandateType;
		return this;
	}
	
	public DDAInitiateModel withType(String type){
		this.type = type;
		return this;
	}
	
	public DDAInitiateModelBuilder build(){
		return new DDAInitiateModelBuilder(
				 referenceId,  debtorAccount,
				 creditorAccount,  debtorAgent,  creditorAgent,
				 creditor,  debtor,  paymentLimit,  expiryDate,
				 effectiveDate,  debtorReference,  currencyCode,  mandateType,  type);
	}
	

}
