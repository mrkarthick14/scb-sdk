package com.scb.api.client.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scb.api.client.ApiBankingException;
import com.scb.api.client.Custody;
import com.scb.api.client.model.custody.*;
import com.scb.api.client.util.HttpUtility;
import com.scb.api.client.util.ObjectMapperProvider;

import java.io.IOException;
import java.io.InputStream;

public class CustodyImpl implements Custody {

    /*
     * This class contains the client code necessary to call APIs for custody holding and custody transaction status
     * */
    private static final String URI_LIST_HOLDINGS = "/openapi/custody/holding";
    private static final String URI_GET_HOLDING_STATUS = "/openapi/custody/holding/status/%s";
    private static final String URI_TRANSACTION_STATUS = "/openapi/custody/transaction";

    private HttpUtility httpUtility;
    private boolean encryptedResponse;
    private ObjectMapper objectMapper = ObjectMapperProvider.objectMapper();


    public CustodyImpl(HttpUtility httpUtility, boolean encryptedResponse) {
        this.httpUtility = httpUtility;
        this.encryptedResponse = encryptedResponse;
    }

    @Override
    public Holdings getHoldings(HoldingsReportRequest request, String messageId) {
        InputStream response = httpUtility.performPost(URI_LIST_HOLDINGS, messageId, false, request, null);
        System.out.println("Inside CustodyImpl");
        try {
            return objectMapper.readValue(response, Holdings.class);
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
    }

    @Override
    public HoldingStatus getHoldingStatus(String id, String messageId) {
        InputStream response = httpUtility.performGet(String.format(URI_GET_HOLDING_STATUS, id), false, "", null);
        try {
            return objectMapper.readValue(response, HoldingStatus.class);
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
    }

    @Override
    public TransactionStatuses getTransactionStatus(TransactionStatusRequest request, String messageId) {
        InputStream response = httpUtility.performPost(URI_TRANSACTION_STATUS, messageId, false, request, null);
        try {
            return objectMapper.readValue(response, TransactionStatuses.class);
        } catch (IOException e) {
            throw new ApiBankingException(e);
        }
    }
}
