package com.scb.api.client.model.custody;

import com.google.common.collect.Lists;

import java.util.List;

public class TransactionStatuses {

    private List<TransactionStatus> status = Lists.newArrayList();

    public TransactionStatuses() { }

    public TransactionStatuses(List<TransactionStatus> status) {
        this.status = status;
    }

    public List<TransactionStatus> getStatus() {
        return status;
    }
}
