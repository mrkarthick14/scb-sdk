package com.scb.api.client.model;

import org.joda.time.LocalDate;

import com.scb.api.client.model.OpenApiAgent;
import com.scb.api.client.model.OpenApiBankParty;
import com.scb.api.client.model.OpenApiPaymentAccount;

public class DDAInitiateModelBuilder {
	private String referenceId;
	private OpenApiPaymentAccount debtorAccount;
	private OpenApiPaymentAccount creditorAccount;
	private OpenApiAgent debtorAgent;
	private OpenApiAgent creditorAgent;
	private OpenApiBankParty creditor;
	private OpenApiBankParty debtor;
	private OpenApiPaymentLimit paymentLimit;
	private LocalDate expiryDate;
	private LocalDate effectiveDate;
	private String debtorReference;
	private String currencyCode;
	private String mandateType;
	private String type;
	public DDAInitiateModelBuilder(String referenceId, OpenApiPaymentAccount debtorAccount,
			OpenApiPaymentAccount creditorAccount, OpenApiAgent debtorAgent, OpenApiAgent creditorAgent,
			OpenApiBankParty creditor, OpenApiBankParty debtor, OpenApiPaymentLimit paymentLimit, LocalDate expiryDate,
			LocalDate effectiveDate, String debtorReference, String currencyCode, String mandateType, String type) {
	
		this.referenceId = referenceId;
		this.debtorAccount = debtorAccount;
		this.creditorAccount = creditorAccount;
		this.debtorAgent = debtorAgent;
		this.creditorAgent = creditorAgent;
		this.creditor = creditor;
		this.debtor = debtor;
		this.paymentLimit = paymentLimit;
		this.expiryDate = expiryDate;
		this.effectiveDate = effectiveDate;
		this.debtorReference = debtorReference;
		this.currencyCode = currencyCode;
		this.mandateType = mandateType;
		this.type = type;
	}
	
	
	
}
