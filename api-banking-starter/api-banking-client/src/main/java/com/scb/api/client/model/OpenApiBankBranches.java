package com.scb.api.client.model;

import java.util.List;

public class OpenApiBankBranches {

    private List<OpenApiBankBranch> branches;

    public OpenApiBankBranches(List<OpenApiBankBranch> branches) {
        this.branches = branches;
    }

    public List<OpenApiBankBranch> getBranches() {
        return branches;
    }
}
