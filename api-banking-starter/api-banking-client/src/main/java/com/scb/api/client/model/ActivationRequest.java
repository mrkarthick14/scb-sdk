package com.scb.api.client.model;

import com.scb.api.client.util.EncryptDecrypt;

public class ActivationRequest {

    private String webHookUrl;
    private boolean enableWebHook;
    private EncryptDecrypt.EncryptedContent activationKey;

    public ActivationRequest(String webHookUrl, boolean enableWebHook, EncryptDecrypt.EncryptedContent activationKey) {
        this.webHookUrl = webHookUrl;
        this.enableWebHook = enableWebHook;
        this.activationKey = activationKey;
    }

    public String getWebHookUrl() {
        return webHookUrl;
    }

    public boolean isEnableWebHook() {
        return enableWebHook;
    }

    public EncryptDecrypt.EncryptedContent  getActivationKey() {
        return activationKey;
    }
}
