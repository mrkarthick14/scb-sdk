package com.scb.api.client.model.PaymentTemplate;

import java.util.List;

public class OpenApiPaymentTemplateId {
	private List<String>  id;
	
	

	public OpenApiPaymentTemplateId() {
		
	
	}



	public OpenApiPaymentTemplateId(List<String> id) {
		
		this.id = id;
	}



	public List<String> getId() {
		return id;
	}
	

}
