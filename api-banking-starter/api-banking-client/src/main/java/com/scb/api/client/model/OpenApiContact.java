package com.scb.api.client.model;

public class OpenApiContact {

    private String phoneNumber;
    private String mobileNumber;
    private String emailAddress;
    private String faxNumber;
    private String name;

    public OpenApiContact() { }
    
    

    public OpenApiContact(String phoneNumber, String mobileNumber, String emailAddress, String faxNumber, String name) {
		super();
		this.phoneNumber = phoneNumber;
		this.mobileNumber = mobileNumber;
		this.emailAddress = emailAddress;
		this.faxNumber = faxNumber;
		this.name = name;
	}



	public String getMobileNumber() {
        return mobileNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

}
