package com.scb.api.client;

import com.scb.api.client.impl.ApiBankingImpl;
import org.apache.http.HttpHost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import static com.scb.api.client.util.HttpUtility.safeHttpsBuilder;

public class ApiBankingBuilder {

    private final ApiBankingEnvironment environment;
    private Proxy proxy;
    private KeyStore keystore;
    private char[] keystorePassword;
    private String signatureKeyAlias;
    private boolean encryptedResponses;
    private String issuerId = "SCB";

    public ApiBankingBuilder(ApiBankingEnvironment environment) {
        this.environment = environment;
    }

    public static ApiBankingBuilder $( ApiBankingEnvironment environment) {
        return new ApiBankingBuilder(environment);
    }

    public ApiBankingBuilder withProxy(Proxy proxy) {
        this.proxy = proxy;
        return this;
    }

    public ApiBankingBuilder withKeystore(KeyStore keystore, String keystorePassword) {
        this.keystore = keystore;
        this.keystorePassword = keystorePassword.toCharArray();
        return this;
    }

    public ApiBankingBuilder withKeystore(InputStream keystore, String keystorePassword) throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
        this.keystore = KeyStore.getInstance("JKS");
        this.keystore.load(keystore, keystorePassword.toCharArray());
        this.keystorePassword = keystorePassword.toCharArray();
        return this;
    }

    public ApiBankingBuilder withIssuerId(String issuerId) {
        this.issuerId = issuerId;
        return this;
    }

    public ApiBankingBuilder encryptedResponses() {
        this.encryptedResponses = true;
        return this;
    }

    public ApiBankingBuilder withSignatureAlias(String signatureKeyAlias) {
        this.signatureKeyAlias = signatureKeyAlias;
        return this;
    }

    public ApiBanking build() throws KeyManagementException, NoSuchAlgorithmException {
            return new ApiBankingImpl(
                    applyProxy(safeHttpsBuilder(this.keystore, new String(this.keystorePassword),
                            HttpClients.custom())),
                    keystore,
                    keystorePassword,
                    signatureKeyAlias,
                    issuerId,
                    encryptedResponses,
                    environment
            );
    }

    private HttpClientBuilder applyProxy(HttpClientBuilder httpClientBuilder) {
        if (proxy != null) {
            if (proxy.address() instanceof InetSocketAddress) {
                return httpClientBuilder.setProxy(new HttpHost(
                        ((InetSocketAddress)proxy.address()).getHostName(),
                        ((InetSocketAddress)proxy.address()).getPort()));
            }
        }
        return httpClientBuilder;
    }

}
