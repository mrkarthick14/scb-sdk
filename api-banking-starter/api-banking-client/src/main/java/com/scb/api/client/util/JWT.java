package com.scb.api.client.util;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.scb.api.client.model.JWTTokenPrototype;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;
import net.oauth.jsontoken.Checker;
import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.JsonTokenParser;
import net.oauth.jsontoken.crypto.RsaSHA256Signer;
import net.oauth.jsontoken.crypto.RsaSHA256Verifier;
import net.oauth.jsontoken.crypto.SignatureAlgorithm;
import net.oauth.jsontoken.crypto.Verifier;
import net.oauth.jsontoken.discovery.VerifierProvider;
import net.oauth.jsontoken.discovery.VerifierProviders;

import org.joda.time.Instant;

import javax.xml.bind.DatatypeConverter;

import java.security.InvalidKeyException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import static java.lang.System.currentTimeMillis;

@SuppressWarnings("unused")
public class JWT {

    private static final String JTI_KEY = "jti";

	private static final Logger logger = Logger.getLogger(JWT.class.getName());

    private static final String AUDIENCE = "SCB-APIBanking";

    private static final long TOKEN_TIMEOUT_DURATION = 1000L * 30L;

    private static final String PAYLOAD = "payload";

    private static long jwtIssueTimeOffset;

    private JWT(long jwtIssueTimeOffset) {
        this.jwtIssueTimeOffset = jwtIssueTimeOffset;
    }

    private JWT() {

    }

    public static void applyIssueTimeOffset(long jwtIssueTimeOffset) {
        JWT.jwtIssueTimeOffset = jwtIssueTimeOffset;
    }

    @SuppressWarnings("unused")
    public static synchronized <T> String createToken(JWTTokenPrototype tokenPrototype) {

        Gson gson = Converters.registerAll(new GsonBuilder()).create();
        RsaSHA256Signer signer;
        try {

            signer = new RsaSHA256Signer(tokenPrototype.getIssuer(), null, (RSAPrivateKey) tokenPrototype.getPrivateKey());

            //Configure JSON token
            JsonToken token = new JsonToken(signer);

            long issueTime = currentTimeMillis() - jwtIssueTimeOffset;

            token.setParam(JTI_KEY, tokenPrototype.getMessageId());
            token.setAudience(AUDIENCE);
            token.setIssuedAt(new Instant(issueTime));
            token.setExpiration(new Instant(issueTime + TOKEN_TIMEOUT_DURATION));
            

            //Configure request object, which provides information of the item
            JsonObject payloadO = token.getPayloadAsJsonObject();
            payloadO.add(PAYLOAD, gson.toJsonTree(tokenPrototype.getPayload()));
            System.out.println("Payload is " + payloadO);
            return token.serializeAndSign();

        } catch (SignatureException | InvalidKeyException e) {
            throw new RuntimeException(e);
        }
    }

    public static String extractPayloadAndVerify(String token, PublicKey publicKey, PrivateKey privateKey, String aesKey) {

        EncryptDecrypt encryptDecrypt = EncryptDecrypt.getDefaultInstance();

        try {

            if (isEmpty(token)) {
                return null;
            }

            final Verifier hmacVerifier = new RsaSHA256Verifier(publicKey);

            if (!isEmpty(aesKey)) {
                token = encryptDecrypt.decryptWithPrivateKey(privateKey,
                        new EncryptDecrypt.EncryptedContent(new String(DatatypeConverter.parseBase64Binary(token)), aesKey));
            }

            String[] arrayOfToken = token.split("\\.");
            String payloadString = new String(DatatypeConverter.parseBase64Binary(arrayOfToken[1]));

            @SuppressWarnings("deprecation")
			JSONObject jsonObj = new JSONObject((JSONObject) new JSONParser().parse(payloadString));

            String headerString = (String) jsonObj.get("header");
            logger.info("header : " + headerString);
            String responseString = jsonObj.get("payload").toString();

            VerifierProviders locators = new VerifierProviders();

            locators.setVerifierProvider(SignatureAlgorithm.RS256, verifierProvider(hmacVerifier));
            JsonTokenParser parser = new JsonTokenParser(locators, checker());

            parser.verifyAndDeserialize(token).getPayloadAsJsonObject();
            return responseString;

        } catch (Exception e) {
            logger.severe("exception in side decryptAndVerifyForHeaderAndPayload :" + e.getMessage());
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    private static VerifierProvider verifierProvider(final Verifier hmacVerifier) {
            return new VerifierProvider() {
            @Override
            public List<Verifier> findVerifier(String id, String key) {
                return Collections.singletonList(hmacVerifier);
            }
        };
    }

    private static Checker checker() {
        return new Checker() {
            @Override
            public void check(JsonObject id) throws SignatureException {
            }
        };
    }

    private static boolean isEmpty(String token) {
        return token == null || token.trim().length() == 0;
    }

}
