package com.scb.api.client.model;

public class OpenApiBankBranch {

    private String name;
    private String swiftCode;
    private String country;
    private OpenApiAddress address;
    private String routingCode;

    public OpenApiBankBranch(String swiftCode, String name, String country, OpenApiAddress address, String routingCode) {
        this.name = name;
        this.swiftCode = swiftCode;
        this.country = country;
        this.address = address;
        this.routingCode = routingCode;
    }

    public String getName() {
        return name;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public String getCountry() {
        return country;
    }

    public OpenApiAddress getAddress() {
        return address;
    }

    public String getRoutingCode() {
        return routingCode;
    }
}
