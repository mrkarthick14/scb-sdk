package com.scb.api.client.model.PaymentTemplate;

public class OpenApiPaymentTemplate {
	
	private String scheme;
	private String countryCode;
	private String bicCode;
	private String transactionType;
	private String localInstrumentType;
	private String chargerBearerCode;
	private String initiatingPartyId;
	private String paymentMethod;
	private String paymentServiceLevel;
	private String paymentPriority;
	private String categoryPurpose;
	private String defaultCreditorAccountType;
	private int version;
	private String defaultCity;
	private int tsVersion;
	
	public OpenApiPaymentTemplate(String scheme) {
		this.scheme = scheme;
	}

	
	
	public String getScheme() {
		return scheme;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public String getBicCode() {
		return bicCode;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public String getLocalInstrumentType() {
		return localInstrumentType;
	}

	public String getChargerBearerCode() {
		return chargerBearerCode;
	}

	public String getInitiatingPartyId() {
		return initiatingPartyId;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public String getPaymentServiceLevel() {
		return paymentServiceLevel;
	}

	public String getPaymentPriority() {
		return paymentPriority;
	}

	public String getCategoryPurpose() {
		return categoryPurpose;
	}

	public String getDefaultCreditorAccountType() {
		return defaultCreditorAccountType;
	}

	public int getVersion() {
		return version;
	}

	public String getDefaultCity() {
		return defaultCity;
	}

	public int getTsVersion() {
		return tsVersion;
	}
	
	public static  OpenApiPaymentTemplate withScheme(String scheme){
		return new OpenApiPaymentTemplate(scheme);
	}
	public OpenApiPaymentTemplate withCountryCode(String countryCode){
		this.countryCode =countryCode;
		return this;
	}
	public OpenApiPaymentTemplate withBicCode(String bicCode){
		this.bicCode =bicCode;
		return this;
	}
	public OpenApiPaymentTemplate withTransactionType(String transactionType){
		this.transactionType =transactionType;
		return this;
	}
	
	public OpenApiPaymentTemplate withLocalInstrumentType(String localInstrumentType){
		this.localInstrumentType =localInstrumentType;
		return this;
	}
	public OpenApiPaymentTemplate withChargerBearerCode(String chargerBearerCode){
		this.chargerBearerCode =chargerBearerCode;
		return this;
	}
	public OpenApiPaymentTemplate withInitiatingPartyId(String initiatingPartyId){
		this.initiatingPartyId =initiatingPartyId;
		return this;
	}
	public OpenApiPaymentTemplate withPaymentMethod(String paymentMethod){
		this.paymentMethod =paymentMethod;
		return this;
	}
	public OpenApiPaymentTemplate withpaymentServiceLevel(String paymentServiceLevel){
		this.paymentServiceLevel =paymentServiceLevel;
		return this;
	}
	public OpenApiPaymentTemplate withPaymentPriority(String paymentPriority){
		this.paymentPriority =paymentPriority;
		return this;
	}
	
	public OpenApiPaymentTemplate withCategoryPurpose(String categoryPurpose){
		this.categoryPurpose =categoryPurpose;
		return this;
	}
	public OpenApiPaymentTemplate withDefaultCreditorAccountType(String defaultCreditorAccountType){
		this.defaultCreditorAccountType =defaultCreditorAccountType;
		return this;
	}
	public OpenApiPaymentTemplate withDefaultCity(String defaultCity){
		this.defaultCity =defaultCity;
		return this;
	}
	public OpenApiPaymentTemplate withVersion(int version){
		this.version =version;
		return this;
	}
	public OpenApiPaymentTemplate withTsVersion(int tsVersion){
		this.tsVersion =tsVersion;
		return this;
	}
	
	public OpenApiPaymentTemplateBuilder build(){
				return new OpenApiPaymentTemplateBuilder( scheme,  countryCode,  bicCode,  transactionType,
						 localInstrumentType,  chargerBearerCode,  initiatingPartyId,  paymentMethod,
						 paymentServiceLevel,  paymentPriority,  categoryPurpose,
						 defaultCreditorAccountType,  version,  defaultCity,  tsVersion);
		
	}
	

	
}
