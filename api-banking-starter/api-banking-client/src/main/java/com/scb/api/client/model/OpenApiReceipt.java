package com.scb.api.client.model;

import java.util.Arrays;
import java.util.List;

public class OpenApiReceipt {

    private List<String> id;

    public List<String> getId() {
        return id;
    }

    public void setId(List<String> id) {
        this.id = id;
    }

    public OpenApiReceipt() {}

    public OpenApiReceipt(String... id) {
        this.id = Arrays.asList(id);
    }


}
