package com.scb.api.client.model;

public class QueueStatistics {

    private long messageCount;
    private long headOfBacklogTimestamp;

    public QueueStatistics() { }

    public QueueStatistics(long messageCount, long headOfBacklogTimestamp) {
        this.messageCount = messageCount;
        this.headOfBacklogTimestamp = headOfBacklogTimestamp;
    }

    public long getMessageCount() {
        return messageCount;
    }

    public long getHeadOfBacklogTimestamp() {
        return headOfBacklogTimestamp;
    }


}
