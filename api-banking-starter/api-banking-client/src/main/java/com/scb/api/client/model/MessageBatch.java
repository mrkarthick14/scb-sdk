package com.scb.api.client.model;

import java.util.List;
import java.util.Map;

public class MessageBatch {

    private String messageId;
    private List<Map<String,Object>> content;

    public MessageBatch() { }

    public MessageBatch(String messageId, List<Map<String, Object>> content) {
        this.messageId = messageId;
        this.content = content;
    }

    public String getMessageId() {
        return messageId;
    }

    public List<Map<String, Object>> getContent() {
        return content;
    }
}
