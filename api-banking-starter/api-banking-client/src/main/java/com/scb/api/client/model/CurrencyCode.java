package com.scb.api.client.model;


import java.util.Objects;

public class CurrencyCode {

    public static final CurrencyCode DEFAULT = new CurrencyCode("USD");

    public static final CurrencyCode ALL = new CurrencyCode("ALL");

    private String isoCode = "";

    public CurrencyCode() {
    }

    public CurrencyCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public String getIsoCode() {
        return isoCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyCode that = (CurrencyCode) o;
        return Objects.equals(isoCode, that.isoCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isoCode);
    }
}
