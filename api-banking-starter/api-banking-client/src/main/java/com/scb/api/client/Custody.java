package com.scb.api.client;

import com.scb.api.client.model.custody.*;

public interface Custody {

    Holdings getHoldings(HoldingsReportRequest request, String messageId);

    HoldingStatus getHoldingStatus(String id, String messageId);

    TransactionStatuses getTransactionStatus(TransactionStatusRequest request, String messageId);
}
