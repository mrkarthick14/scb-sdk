package com.scb.api.client.model;

public class DDIInitiateModel {
	private String referenceId;
	private instructedAmount instructedAmount;
	private String mandateId;
	private OpenApiBankParty debtor;
	
	
	
	public DDIInitiateModel(String referenceId) {
		
		this.referenceId = referenceId;
	}



	public DDIInitiateModel(String referenceId, instructedAmount instructedAmount,
			String mandateId, OpenApiBankParty debtor) {
	
		this.referenceId = referenceId;
		this.instructedAmount = instructedAmount;
		this.mandateId = mandateId;
		this.debtor = debtor;
	}



	public String getReferenceId() {
		return referenceId;
	}



	public instructedAmount getInstructedAmount() {
		return instructedAmount;
	}



	public String getMandateId() {
		return mandateId;
	}



	public OpenApiBankParty getDebtor() {
		return debtor;
	}
	
	public static DDIInitiateModel WithReferenceId(String referenceId){
		return new DDIInitiateModel(referenceId);
	}
	
	public DDIInitiateModel withInstructedAmount(instructedAmount instructedAmount){
		this.instructedAmount = instructedAmount;
		return this;
	}
	
	public DDIInitiateModel withMandateId(String mandateId){
		this.mandateId = mandateId;
		return this;
	} 
	
	public DDIInitiateModel withDebtor(OpenApiBankParty debtor){
		this.debtor = debtor;
		return this;
	}
	
	public DDIInitiateModelBuilder build(){
		return new DDIInitiateModelBuilder( referenceId,  instructedAmount,
				 mandateId,  debtor);
	}
	
	
	
}
