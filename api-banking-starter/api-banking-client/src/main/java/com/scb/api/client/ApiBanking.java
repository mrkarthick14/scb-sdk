package com.scb.api.client;

import com.scb.api.client.model.EventAck;
import com.scb.api.client.model.MessageBatch;
import com.scb.api.client.model.QueueStatistics;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public interface ApiBanking {

    String URI_ACTIVATE = "activate";
    String URI_STATS    = "uat/core/event/statistics";
    String URI_PEEK     = "uat/core/event/peek";
    String URI_RECOVER  = "uat/core/event/recover";
    String URI_CONSUME  = "uat/core/event/consume";

    String URI_FAILURE = "uat/core/event/ack/failure";
    String URI_SUCCESS = "uat/core/event/ack/success";

    InputStream activate(InputStream activationKeyContent, String messageId, boolean enableWebHook, String webHookUrl);

    QueueStatistics statistics(String messageId) throws IOException;
    MessageBatch peek(String messageId) throws IOException;
    MessageBatch consume(String messageId, int batchSize) throws IOException;
    MessageBatch consume(String messageId) throws IOException;
    MessageBatch recover(String messageId) throws IOException;

    <T> EventAck failure(String messageId, String transactionId, T failureReport) throws IOException;
    <T> EventAck success(String messageId, String transactionId, T successReport) throws IOException;

    <T> T schema(Class<T> type);

}
