package com.scb.api.client.model;

public class OpenApiLicense {

    private String id;
    private String name;

    public OpenApiLicense() { }

    public OpenApiLicense(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
