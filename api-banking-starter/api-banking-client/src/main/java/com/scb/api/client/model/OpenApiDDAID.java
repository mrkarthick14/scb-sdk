package com.scb.api.client.model;

import java.util.List;

public class OpenApiDDAID {
	private String mandateId;
	private String referenceId;
	private String status;
	private String statusCode;
	private String statusMessage;
	private List<String>comment;
	
	private String timeStamp;
	
	
	

	public OpenApiDDAID() {
		
		
	}

	public OpenApiDDAID(String mandateId, String referenceId, String status, String statusCode, String statusMessage,
			List<String> comment, String timeStamp) {
		super();
		this.mandateId = mandateId;
		this.referenceId = referenceId;
		this.status = status;
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.comment = comment;
		this.timeStamp = timeStamp;
	}

	public String getMandateId() {
		return mandateId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public String getStatus() {
		return status;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public List<String> getComment() {
		return comment;
	}

	public String getTimeStamp() {
		return timeStamp;
	}
	
	
	
	

	
	

}
