package com.scb.api.client.model.custody;


import com.scb.api.client.model.OpenApiPaymentInstructionInstruction;
import org.joda.time.Instant;
import org.joda.time.LocalDate;

public class HoldingsReportRequest {

    private String businessPartnerId;
    private String scaId;
    private LocalDate date;

    public HoldingsReportRequest() {
    }

    public HoldingsReportRequest(String businessPartnerId, String scaId, LocalDate date) {
        this.date = date;
        this.businessPartnerId = businessPartnerId;
        this.scaId = scaId;
    }

    public static class Builder {
        private String businessPartnerId;
        private String scaId;
        private LocalDate date;

        public HoldingsReportRequest build() {
            return new HoldingsReportRequest(businessPartnerId, scaId, date);
        }

        public HoldingsReportRequest.Builder withBusinessPartnerId(String businessPartnerId) {
            this.businessPartnerId = businessPartnerId;
            return this;
        }

        public HoldingsReportRequest.Builder withScaId(String scaId) {
            this.scaId = scaId;
            return this;
        }

        public HoldingsReportRequest.Builder withDate(LocalDate date) {
            this.date = date;
            return this;
        }
    }


    public LocalDate getDate() {
        return date;
    }

    public String getBusinessPartnerId() {
        return businessPartnerId;
    }

    public String getScaId() {
        return scaId;
    }

}
