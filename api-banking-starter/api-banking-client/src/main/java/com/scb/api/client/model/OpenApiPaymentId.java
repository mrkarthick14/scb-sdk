package com.scb.api.client.model;

public class OpenApiPaymentId {

    private String paymentIdentifier;
    private String internalTrackingId;
    private String clientReferenceId;

    //No-args constructor for serialization
    public OpenApiPaymentId() {
    }

    public OpenApiPaymentId(String paymentIdentifier, String internalTrackingId, String clientReferenceId) {

        this.paymentIdentifier = paymentIdentifier;
        this.internalTrackingId = internalTrackingId;
        this.clientReferenceId = clientReferenceId;
    }

    public String getPaymentIdentifier() {
        return paymentIdentifier;
    }

    public String getInternalTrackingId() {
        return internalTrackingId;
    }

    public String getClientReferenceId() {
        return clientReferenceId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OpenApiPaymentId{");
        sb.append("paymentIdentifier='").append(paymentIdentifier).append('\'');
        sb.append(", internalTrackingId='").append(internalTrackingId).append('\'');
        sb.append(", clientReferenceId='").append(clientReferenceId).append('\'');
        sb.append('}');
        return sb.toString();
    }
}

