
package com.scb.api.client.model;

public class OpenApiPaymentAccount {

    private String id;
    private AccountIdentifier.IdentifierType identifierType = AccountIdentifier.IdentifierType.Other;
    private ExternalCodes.CashAccountType accountType;

    /**
     * No args constructor for use in serialization
     * 
     */
    public OpenApiPaymentAccount() {
    }

    public OpenApiPaymentAccount(String id, ExternalCodes.CashAccountType cashAccountType, AccountIdentifier.IdentifierType identifierType) {
        super();
        this.id = id;
        this.accountType = cashAccountType;
        this.identifierType = identifierType;
    }
   

    public String getId() {
        return id;
    }

    public AccountIdentifier.IdentifierType getIdentifierType() {
        return identifierType;
    }

    public ExternalCodes.CashAccountType getAccountType() {
        return accountType;
    }
}
