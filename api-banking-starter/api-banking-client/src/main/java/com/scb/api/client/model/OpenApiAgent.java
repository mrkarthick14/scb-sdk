
package com.scb.api.client.model;

public class OpenApiAgent {

    private OpenApiFinancialInstitution financialInstitution;
    private String clearingSystemId;
    private String clearingSystemCode;
    private String branchCode;

    /**
     * No args constructor for use in serialization
     * 
     */
    public OpenApiAgent() {
    }

    /**
     * 
     * @param clearingSystemCode
     * @param financialInstitution
     * @param clearingSystemId
     */
    public OpenApiAgent(OpenApiFinancialInstitution financialInstitution, String clearingSystemId, String clearingSystemCode, String branchCode) {
        super();
        this.financialInstitution = financialInstitution;
        this.clearingSystemId = clearingSystemId;
        this.branchCode = branchCode;
        this.clearingSystemCode = clearingSystemCode;
    }

    public OpenApiFinancialInstitution getFinancialInstitution() {
        return financialInstitution;
    }

    public void setFinancialInstitution(OpenApiFinancialInstitution financialInstitution) {
        this.financialInstitution = financialInstitution;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public String getClearingSystemId() {
        return clearingSystemId;
    }

    public void setClearingSystemId(String clearingSystemId) {
        this.clearingSystemId = clearingSystemId;
    }

    public String getClearingSystemCode() {
        return clearingSystemCode;
    }

    public void setClearingSystemCode(String clearingSystemCode) {
        this.clearingSystemCode = clearingSystemCode;
    }


}
