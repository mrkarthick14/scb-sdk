package com.scb.api.client.model;


public interface ExternalCodes {

    enum NCSCD {
        AT,
        AU,
        BL,
        CC,
        CN,
        CP,
        ES,
        FW,
        GR,
        HK,
        IE,
        IN,
        IT,
        PL,
        PT,
        RT,
        RU,
        SC,
        SW,
        ZA,
        NZ,
        NONE,
        TW
    }

    enum ClearingSystemIdentificationType {

        ATBLZ(NCSCD.AT, "Austrian Bankleitzahl", "Bank Branch code used in Austria",""),
        AUBSB(NCSCD.AU, "Australian Bank State Branch Code (BSB)", "Bank Branch code used in Australia",""),
        CACPA(NCSCD.CC,"Canadian Payments Association Payment Routing Number", "Bank Branch code used in Canada",""),
        CHBCC(NCSCD.SW,"Swiss Financial Institution Identification (short)", "Financial Institution Identification (IID) used in Switzerland, without check digit",""),
        CHSIC(NCSCD.SW,"Swiss Financial Institution Identification (long)", "Financial Institution Identification (IID) used in Switzerland, including check digit",""),
        CNAPS(NCSCD.CN,"CNAPS Identifier", "Bank Branch code used in China",""),
        DEBLZ(NCSCD.BL,"German Bankleitzahl", "Bank Branch code used in Germany",""),
        ESNCC(NCSCD.ES,"Spanish Domestic Interbanking Code", "Bank Branch code used in Spain",""),
        GBDSC(NCSCD.SC,"UK Domestic Sort Code", "Bank Branch code used in the UK",""),
        GRBIC(NCSCD.GR,"Helenic Bank Identification Code", "Bank Branch code used in Greece",""),
        HKNCC(NCSCD.HK,"Hong Kong Bank Code ", "Bank Branch code used in Hong Kong",""),
        IENCC(NCSCD.IE,"Irish National Clearing Code", "Bank Branch code used in Ireland",""),
        INFSC(NCSCD.IN,"Indian Financial System Code", "Bank Branch code used in India",""),
        ITNCC(NCSCD.IT,"Italian Domestic Identification Code", "Bank Branch code used in Italy",""),
        JPZGN(NCSCD.NONE,"Japan Zengin Clearing Code", "Bank Branch code used in Japan",""),
        NZNCC(NCSCD.NZ, "New Zealand National Clearing Code", "Bank Branch code used in New Zealand",""),
        PLKNR(NCSCD.NONE, "Polish National Clearing Code", "Bank Branch code used in Poland",""),
        PTNCC(NCSCD.PT,"Portuguese National Clearing Code", "Bank Branch code used in Portugal",""),
        RUCBC(NCSCD.NONE, "Russian Central Bank Identification Code", "Bank Branch code used in Russia",""),
        SESBA(NCSCD.NONE, "Sweden Bankgiro Clearing Code", "Bank Branch code used in Sweden",""),
        SGIBG(NCSCD.NONE, "IBG Sort Code", "Bank Branch code used in Singapore",""),
        THCBC(NCSCD.NONE,"Thai Central Bank Identification Code", "Bank Identification code used in Thailand",""),
        TWNCC(NCSCD.TW,"Financial Institution Code", "Bank Branch code used in Taiwan",""),
        USABA(NCSCD.FW,"United States Routing Number (Fedwire, NACHA)", "Routing Transit number assigned by the ABA for US financial institutons",""),
        USPID(NCSCD.CP,"CHIPS Participant Identifier", "Bank identifier used by CHIPs in the US",""),
        ZANCC(NCSCD.ZA,"South African National Clearing Code", "Bank Branch code used in South Africa","");

        private final NCSCD ncscd;
        private final String name;
        private final String definition;
        private String s2bId;

        ClearingSystemIdentificationType(NCSCD ncscd, String name, String definition, String s2bId) {
            this.ncscd = ncscd;
            this.name = name;
            this.definition = definition;
            this.s2bId = s2bId;
        }

        public NCSCD getNcscd() {
            return ncscd;
        }

        public String getName() {
            return name;
        }

        public String getDefinition() {
            return definition;
        }

        public String getS2BId() {
            return s2bId;
        }
    }

    enum DocumentLineType {
        ADPI("Additional Product Identification Assigned by the Manufacturer", "Line item reference is an additional product identification assigned by the manufacturer."),
        AISB("Alternate ISBN", "Line item reference is an alternate International Standard Book Number (ISBN)."),
        ASNB("Asset Number", "Line item reference is an asset number."),
        CTNB("Catalog Number", "Line item reference is a catalog number."),
        DBSP("Dun & Bradstreet Standard Product and Service Code", "Line item reference is Dun & Bradstreet Standard Product and Service code."),
        EANN("European Article Number (EAN) (2-5-5-1)", "Line item reference is an European Article Number (EAN)."),
        EINB("Equipment Identification Number", "Line item reference is an equipment identification number."),
        GSNB("General Specification Number", "Line item reference is a general specification number."),
        HIBC("HIBC (Health Care Industry Bar Code)", "Line item reference is a Health Care Industry Bar Code (HIBC)"),
        ISBN("International Standard Book Number (ISBN)", "Line item reference is an International Standard Book Number (ISBN)."),
        LTNB("Lot Number", "Line item reference is a lot number."),
        MDNB("Model Number", "Line item reference is a model number"),
        PRNB("Part Number", "Line item reference is a part reference number."),
        PTCD("Product Type Code", "Line item reference is a product type code."),
        SKNB("Stock Number", "Line item reference is a stock number."),
        STNB("Style Number", "Line item reference is a style number."),
        TONB("Technical Order Number", "Line item reference is a technical order number."),
        UPCC("UPC Consumer Package Code", "Line item reference is an UPC consumer package code."),
        UPNB("Universal Product Number", "Line item reference is an Universal Product Number.");
        private final EnumInfo info;

        DocumentLineType(String nickname, String description) {
            info = new EnumInfo(nickname, description);
        }

        public EnumInfo getInfo() {
            return info;
        }
    }

    enum DocumentType {
        CNFA("CreditNoteRelatedToFinancialAdjustment", "Document is a credit note for the final amount settled for a commercial transaction."),
        DNFA("DebitNoteRelatedToFinancialAdjustment", "Document is a debit note for the final amount settled for a commercial transaction."),
        CINV("CommercialInvoice", "Document is an invoice."),
        CREN("CreditNote", "Document is a credit note."),
        DEBN("DebitNote", "Document is a debit note."),
        HIRI("HireInvoice", "Document is an invoice for the hiring of human resources or renting goods or equipment."),
        SBIN("SelfBilledInvoice", "Document is an invoice issued by the debtor."),
        CMCN("CommercialContract", "Document is an agreement between the parties, stipulating the terms and conditions of the delivery of goods or services."),
        SOAC("StatementOfAccount", "Document is a statement of the transactions posted to the debtor's account at the supplier."),
        DISP("DispatchAdvice", "Document is a dispatch advice."),
        BOLD("BillOfLading", "Document is a shipping notice."),
        VCHR("Voucher", "Document is an electronic payment document."),
        AROI("AccountReceivableOpenItem", "Document is a payment that applies to a specific source document."),
        TSUT("TradeServicesUtilityTransaction", "Document is a transaction identifier as assigned by the Trade Services Utility."),
        PUOR("PurchaseOrder", "Document is a purchase order.");

        private final EnumInfo info;

        DocumentType(String nickname, String description) {
            info = new EnumInfo(nickname, description);
        }

        public EnumInfo getInfo() {
            return info;
        }
    }

    enum InstructionPriority {
        HIGH,
        NORM
    }

    enum PaymentMethod {
        CHK,
        TRF,
        TRA
    }

    enum ChargeBearerType {
        DEBT,
        CRED,
        SHAR,
        SLEV
    }

    enum InstructionCode {
        CHQB("PayCreditorByCheque", "(Ultimate) creditor must be paid by cheque."),
        HOLD("HoldCashForCreditor", "Amount of money must be held for the (ultimate) creditor, who will call. Pay on identification."),
        PHOB("PhoneBeneficiary", "Please advise/contact (ultimate) creditor/claimant by phone"),
        TELB("Telecom", "Please advise/contact (ultimate) creditor/claimant by the most efficient means of telecommunication.");

        private final EnumInfo info;

        InstructionCode(String nickname, String description) {
            info = new EnumInfo(nickname, description);
        }

        public EnumInfo getInfo() {
            return info;
        }
    }

    enum CashAccountType {

        CACC("Current", "Account used to post debits and credits when no specific account has been nominated.","C"),
        CASH("CashPayment", "Account used for the payment of cash.","C"),
        CHAR("Charges", "Account used for charges if different from the account for payment.",""),
        CISH("CashIncome", "Account used for payment of income if different from the current cash account",""),
        COMM("Commission", "Account used for commission if different from the account for payment.",""),
        CPAC("ClearingParticipantSettlementAccount", "Account used to post settlement debit and credit entries on behalf of a designated Clearing Participant.",""),
        LLSV("LimitedLiquiditySavingsAccount", "Account used for savings with special interest and withdrawal terms.  ",""),
        LOAN("Loan", "Account used for loans.",""),
        MGLD("Marginal Lending", "Account used for a marginal lending facility.",""),
        MOMA("Money Market", "Account used for money markets if different from the cash account.",""),
        NREX("NonResidentExternal", "Account used for non-resident external.",""),
        ODFT("Overdraft", "Account is used for overdrafts.",""),
        ONDP("OverNightDeposit", "Account used for overnight deposits.",""),
        OTHR("OtherAccount", "Account not otherwise specified. ",""),
        SACC("Settlement", "Account used to post debit and credit entries, as a result of transactions cleared and settled through a specific clearing and settlement system.",""),
        SLRY("Salary", "Accounts used for salary payments.","S"),
        SVGS("Savings", "Account used for savings.","SA"),
        TAXE("Tax", "Account used for taxes if different from the account for payment.",""),
        TRAN("TransactingAccount", "A transacting account is the most basic type of bank account that you can get. The main difference between transaction and cheque accounts is that you usually do not get a cheque book with your transacting account and neither are you offered an overdraft facility.",""),
        TRAS("Cash Trading", "Account used for trading if different from the current cash account.","");

        private final EnumInfo info;
        private String s2BId;

        CashAccountType(String nickname, String description, String s2BId) {
            info = new EnumInfo(nickname, description);
            this.s2BId = s2BId;
        }

        public EnumInfo getInfo() {
            return info;
        }

        public String getS2BId() {
            return s2BId;
        }

    }

    enum ChequePriority {
        HIGH, NORM
    }

    enum ChequeType {

        CCHQ("CustomerCheque", "Cheque drawn on the account of the debtor, and debited on the debtor's account when the cheque is cashed. Synonym is 'corporate cheque'."),
        CCCH("CertifiedCustomerCheque", "Cheque drawn on the account of the debtor, and debited on the debtor's account when the cheque is cashed. The financial institution prints and certifies the cheque, guaranteeing the payment."),
        BCHQ("BankCheque", "Cheque drawn on the account of the debtor's financial institution, which is debited on the debtor's account when the cheque is issued. These cheques are printed by the debtor's financial institution and payment is guaranteed by the financial institution. Synonym is 'cashier's cheque'."),
        DRFT("Draft ", "A guaranteed bank cheque with a future value date (do not pay before], which in commercial terms is a 'negotiatable instrument': the beneficiary can receive early payment from any bank under subtraction of a discount. The ordering customer's account is debited on value date."),
        ELDR("ElectronicDraft ", "An instrument with a future value date (do not pay before], which in commercial terms is a 'negotiatable instrument': the beneficiary can receive early payment from any bank under subtraction of a discount. The ordering customer's account is debited on value date.");

        private final EnumInfo info;

        ChequeType(String nickname, String description) {
            info = new EnumInfo(nickname, description);
        }

        public EnumInfo getInfo() {
            return info;
        }

    }

    enum ChequeDeliveryMethod {

        MLDB("MailToDebtor", "Cheque is to be sent through mail services to debtor."),
        MLCD("MailToCreditor", "Cheque is to be sent through mail services to creditor."),
        MLFA("MailToFinalAgent", "Cheque is to be sent through mail services to creditor agent."),
        CRDB("CourierToDebtor", "Cheque is to be sent through courier services to debtor."),
        CRCD("CourierToCreditor", "Cheque is to be sent through courier services to creditor."),
        CRFA("CourierToFinalAgent", "Cheque is to be sent through courier services to creditor agent."),
        PUDB("PickUpByDebtor", "Cheque will be picked up by the debtor."),
        PUCD("PickUpByCreditor", "Cheque will be picked up by the creditor"),
        PUFA("PickUpByFinalAgent", "Cheque will be picked up by the creditor agent."),
        RGDB("RegisteredMailToDebtor", "Cheque is to be sent through registered mail services to debtor"),
        RGCD("RegisteredMailToCreditor", "Cheque is to be sent through registered mail services to creditor."),
        RGFA("RegisteredMailToFinalAgent", "Cheque is to be sent through registered mail services to creditor agent.");

        private final EnumInfo info;

        ChequeDeliveryMethod(String nickname, String description) {
            info = new EnumInfo(nickname, description);
        }

        public EnumInfo getInfo() {
            return info;
        }

    }

    enum PurposeCode {
        BKDF("Bank Debt", "Bank Loan Delayed Draw Funding"),
        BKFE("Bank Debt", "Bank Loan Fees"),
        BKFM("Bank Debt", "Bank Loan Funding Memo"),
        BKIP("Bank Debt", "Bank Loan Accrued Interest Payment"),
        BKPP("Bank Debt", "Bank Loan Principal Paydown"),
        CBLK("Card Settlement", "Card Bulk Clearing"),
        CDCB("Card Settlement", "CardPayment with CashBack"),
        CDCD("Card Settlement", "CashDisbursement"),
        CDCS("Card Settlement", "Cash Disbursement with Surcharging"),
        CDDP("Card Settlement", "Card Deferred Payment"),
        CDOC("Card Settlement", "OriginalCredit "),
        CDQC("Card Settlement", "QuasiCash "),
        ETUP("Card Settlement", "E-Purse Top Up"),
        FCOL("Card Settlement", "Fee Collection"),
        MTUP("Card Settlement", "Mobile Top Up"),
        ACCT("Cash Mgmt", "AccountManagement"),
        CASH("Cash Mgmt", "CashManagementTransfer"),
        COLL("Cash Mgmt", "CollectionPayment"),
        CSDB("Cash Mgmt", "CashDisbursement"),
        DEPT("Cash Mgmt", "Deposit"),
        INTC("Cash Mgmt", "IntraCompanyPayment"),
        LIMA("Cash Mgmt", "LiquidityManagement"),
        NETT("Cash Mgmt", "Netting"),
        BFWD("Collateral", "Bond Forward"),
        CCIR("Collateral", "Cross Currency IRS"),
        CCPC("Collateral", "CCP Cleared Initial Margin"),
        CCPM("Collateral", "CCP Cleared Variation Margin"),
        CCSM("Collateral", "CCP Cleared Initial Margin Segregated Cash"),
        CRDS("Collateral", "Credit DefaultSwap"),
        CRPR("Collateral", "Cross Product"),
        CRSP("Collateral", "Credit Support"),
        CRTL("Collateral", "Credit Line"),
        EQPT("Collateral", "Equity Option"),
        EQUS("Collateral", "Equity Swap"),
        EXPT("Collateral", "Exotic Option"),
        EXTD("Collateral", "Exchange Traded Derivatives"),
        FIXI("Collateral", "Fixed Income"),
        FWBC("Collateral", "Forward Broker Owned Cash Collateral"),
        FWCC("Collateral", "Forward Client Owned Cash Collateral"),
        FWSB("Collateral", "Forward Broker Owned Cash Collateral Segregated"),
        FWSC("Collateral", "Forward Client Owned Segregated Cash Collateral"),
        MARG("Collateral", "Daily margin on listed derivatives "),
        MBSB("Collateral", "MBS Broker Owned Cash Collateral"),
        MBSC("Collateral", "MBS Client Owned Cash Collateral "),
        MGCC("Collateral", "Futures Initial Margin  "),
        MGSC("Collateral", "Futures Initial Margin Client Owned Segregated Cash Collateral "),
        OCCC("Collateral", "Client owned OCC pledged collateral"),
        OPBC("Collateral", "OTC Option Broker owned Cash collateral "),
        OPCC("Collateral", "OTC Option Client owned Cash collateral "),
        OPSB("Collateral", "OTC Option Broker Owned Segregated Cash Collateral "),
        OPSC("Collateral", "OTC Option Client Owned Cash Segregated Cash Collateral "),
        OPTN("Collateral", "FX Option"),
        OTCD("Collateral", "OTC Derivatives"),
        REPO("Collateral", "Repurchase Agreement"),
        RPBC("Collateral", "Bi-lateral repo broker owned collateral "),
        RPCC("Collateral", "Repo client owned collateral "),
        RPSB("Collateral", "Bi-lateral repo broker owned segregated cash collateral "),
        RPSC("Collateral", "Bi-lateral Repo client owned segregated cash collateral"),
        RVPO("Collateral", "Reverse Repurchase Agreement"),
        SBSC("Collateral", "Securities Buy Sell Sell Buy Back"),
        SCIE("Collateral", "Single Currency IRS Exotic"),
        SCIR("Collateral", "Single Currency IRS"),
        SCRP("Collateral", "Securities Cross Products"),
        SHBC("Collateral", "Broker owned collateral Short Sale"),
        SHCC("Collateral", "Client owned collateral Short Sale"),
        SHSL("Collateral", "Short Sell"),
        SLEB("Collateral", "Securities Lending And Borrowing"),
        SLOA("Collateral", "SecuredLoan"),
        SWBC("Collateral", "Swap Broker owned cash collateral "),
        SWCC("Collateral", "Swap Client owned cash collateral "),
        SWPT("Collateral", "Swaption"),
        SWSB("Collateral", "Swaps Broker Owned Segregated Cash Collateral "),
        SWSC("Collateral", "Swaps Client Owned Segregated Cash Collateral "),
        TBAS("Collateral", "To Be Announced"),
        TBBC("Collateral", "TBA Broker owned cash collateral"),
        TBCC("Collateral", "TBA Client owned cash collateral"),
        TRCP("Collateral", "Treasury Cross Product"),
        AGRT("Commercial", "AgriculturalTransfer"),
        AREN("Commercial", "Accounts Receivables Entry"),
        BEXP("Commercial", "BusinessExpenses"),
        BOCE("Commercial", "Back Office Conversion Entry"),
        COMC("Commercial", "CommercialPayment"),
        CPYR("Commercial", "Copyright"),
        GDDS("Commercial", "PurchaseSaleOfGoods"),
        GDSV("Commercial", "PurchaseSaleOfGoodsAndServices"),
        GSCB("Commercial", "PurchaseSaleOfGoodsAndServicesWithCashBack"),
        LICF("Commercial", "LicenseFee"),
        MP2B("Commercial", "Mobile P2B Payment"),
        POPE("Commercial", "Point of Purchase Entry"),
        ROYA("Commercial", "Royalties"),
        SCVE("Commercial", "PurchaseSaleOfServices"),
        SUBS("Commercial", "Subscription"),
        SUPP("Commercial", "SupplierPayment"),
        TRAD("Commercial", "TradeServices"),
        CHAR("Consumer", "CharityPayment"),
        COMT("Consumer", "ConsumerThirdPartyConsolidatedPayment"),
        MP2P("Consumer", "Mobile P2P Payment"),
        ECPG("E-Commerce", "GuaranteedEPayment"),
        ECPR("E-Commerce", "EPaymentReturn"),
        ECPU("E-Commerce", "NonGuaranteedEPayment"),
        EPAY("E-Commerce", "Epayment"),
        CLPR("Finance", "CarLoanPrincipalRepayment"),
        COMP("Finance", "Compensation Payment"),
        DBTC("Finance", "DebitCollectionPayment"),
        GOVI("Finance", "GovernmentInsurance"),
        HLRP("Finance", "HousingLoanRepayment"),
        INPC("Finance", "InsurancePremiumCar"),
        INSU("Finance", "InsurancePremium"),
        INTE("Finance", "Interest"),
        LBRI("Finance", "LaborInsurance"),
        LIFI("Finance", "LifeInsurance"),
        LOAN("Finance", "Loan"),
        LOAR("Finance", "LoanRepayment"),
        PENO("Finance", "PaymentBasedOnEnforcementOrder"),
        PPTI("Finance", "PropertyInsurance"),
        RELG("Finance", "RentalLeaseGeneral"),
        RINP("Finance", "RecurringInstallmentPayment"),
        TRFD("Finance", "TrustFund"),
        FORW("Foreign Exchange", "Forward Foreign Exchange"),
        FXNT("Foreign Exchange", "Foreign Exchange Related Netting"),
        ADMG("General", "AdministrativeManagement"),
        ADVA("General", "AdvancePayment"),
        BCDM("General", "BearerChequeDomestic"),
        BCFG("General", "BearerChequeForeign"),
        BLDM("General", "BuildingMaintenance"),
        BNET("General", "Bond Forward Netting"),
        CBFF("General", "CapitalBuilding"),
        CBFR("General", "CapitalBuildingRetirement "),
        CCRD("General", "CreditCardPayment "),
        CDBL("General", "CreditCardBill"),
        CFEE("General", "CancellationFee"),
        CGDD("General", "CardGeneratedDirectDebit"),
        CORT("General", "Trade Settlement Payment"),
        COST("General", "Costs"),
        CPKC("General", "Carpark Charges "),
        DCRD("General", "Debit Card Payment"),
        DSMT("General", "Printed Order Disbursement"),
        DVPM("General", "Deliver Against Payment"),
        EDUC("General", "Education "),
        FACT("General", "Factor Update related payment"),
        FAND("General", "FinancialAidInCaseOfNaturalDisaster"),
        FCPM("General", "Late Payment of Fees & Charges "),
        FEES("General", "Payment of Fees"),
        GOVT("General", "GovernmentPayment"),
        ICCP("General", "IrrevocableCreditCardPayment"),
        IDCP("General", "IrrevocableDebitCardPayment"),
        IHRP("General", "InstalmentHirePurchaseAgreement"),
        INSM("General", "Installment"),
        IVPT("General", "Invoice Payment "),
        MCDM("General", "MultiCurrenyChequeDomestic"),
        MCFG("General", "MultiCurrenyChequeForeign"),
        MSVC("General", "MultipleServiceTypes"),
        NOWS("General", "NotOtherwiseSpecified"),
        OCDM("General", "OrderChequeDomestic"),
        OCFG("General", "OrderChequeForeign"),
        OFEE("General", "OpeningFee"),
        OTHR("General", "Other"),
        PADD("General", "Preauthorized debit"),
        PTSP("General", "PaymentTerms"),
        RCKE("General", "Re-presented Check Entry"),
        RCPT("General", "ReceiptPayment"),
        REBT("General", "Rebate "),
        REFU("General", "Refund"),
        RENT("General", "Rent"),
        REOD("General", "AccountOverdraftRepayment"),
        RIMB("General", "Reimbursement of a previous erroneous transaction"),
        RPNT("General", "Bi-lateral repo internet netting"),
        RRBN("General", "Round Robin"),
        RVPM("General", "Receive Against Payment"),
        SLPI("General", "PaymentSlipInstruction"),
        SPLT("General", "Split payments"),
        STDY("General", "Study"),
        TBAN("General", "TBA pair-off netting"),
        TBIL("General", "Telecommunications Bill "),
        TCSC("General", "Town Council Service Charges "),
        TELI("General", "Telephone-Initiated Transaction"),
        TMPG("General", "TMPG claim payment"),
        TPRI("General", "Tri Party Repo Interest"),
        TPRP("General", "Tri-party Repo netting"),
        TRNC("General", "TruncatedPaymentSlip"),
        TRVC("General", "TravellerCheque"),
        WEBI("General", "Internet-Initiated Transaction"),
        ANNI("Investment", "Annuity"),
        CAFI("Investment", "Custodian Management fee In-house"),
        CFDI("Investment", "Capital falling due In-house"),
        CMDT("Investment", "CommodityTransfer"),
        DERI("Investment", "Derivatives"),
        DIVD("Investment", "Dividend"),
        FREX("Investment", "ForeignExchange"),
        HEDG("Investment", "Hedging"),
        INVS("Investment", "Investment & Securities "),
        PRME("Investment", "PreciousMetal"),
        SAVG("Investment", "Savings"),
        SECU("Investment", "Securities"),
        SEPI("Investment", "Securities Purchase In-house"),
        TREA("Investment", "TreasuryPayment"),
        UNIT("Investment", "UnitTrustPurchase"),
        FNET("Listed Derivatives", "Futures Netting Payment"),
        FUTR("Listed Derivatives", "Futures"),
        ANTS("Medical", "AnesthesiaServices"),
        CVCF("Medical", "ConvalescentCareFacility"),
        DMEQ("Medical", "DurableMedicaleEquipment"),
        DNTS("Medical", "DentalServices"),
        HLTC("Medical", "HomeHealthCare"),
        HLTI("Medical", "HealthInsurance"),
        HSPC("Medical", "HospitalCare"),
        ICRF("Medical", "IntermediateCareFacility"),
        LTCF("Medical", "LongTermCareFacility"),
        MAFC("Medical", "MedicalAidFundContribution"),
        MDCS("Medical", "MedicalServices"),
        VIEW("Medical", "VisionCare"),
        CDEP("OTC Derivatives", "Credit default event payment"),
        SWFP("OTC Derivatives", "Swap contract final payment"),
        SWPP("OTC Derivatives", "Swap contract partial payment"),
        SWRS("OTC Derivatives", "Swap contract reset payment"),
        SWUF("OTC Derivatives", "Swap contract upfront payment"),
        ADCS("Salary & Benefits", "AdvisoryDonationCopyrightServices"),
        AEMP("Salary & Benefits", "ActiveEmploymentPolicy"),
        ALLW("Salary & Benefits", "Allowance "),
        ALMY("Salary & Benefits", "AlimonyPayment"),
        BBSC("Salary & Benefits", "Baby Bonus Scheme "),
        BECH("Salary & Benefits", "ChildBenefit"),
        BENE("Salary & Benefits", "UnemploymentDisabilityBenefit"),
        BONU("Salary & Benefits", "BonusPayment."),
        CCHD("Salary & Benefits", "Cash compensation, Helplessness, Disability"),
        COMM("Salary & Benefits", "Commission"),
        CSLP("Salary & Benefits", "CompanySocialLoanPaymentToBank"),
        GFRP("Salary & Benefits", "GuaranteeFundRightsPayment"),
        GVEA("Salary & Benefits", "Austrian Government Employees Category A"),
        GVEB("Salary & Benefits", "Austrian Government Employees Category B"),
        GVEC("Salary & Benefits", "Austrian Government Employees Category C"),
        GVED("Salary & Benefits", "Austrian Government Employees Category D"),
        GWLT("Salary & Benefits", "GovermentWarLegislationTransfer"),
        HREC("Salary & Benefits", "Housing Related Contribution"),
        PAYR("Salary & Benefits", "Payroll"),
        PEFC("Salary & Benefits", "PensionFundContribution"),
        PENS("Salary & Benefits", "PensionPayment"),
        PRCP("Salary & Benefits", "PricePayment"),
        RHBS("Salary & Benefits", "RehabilitationSupport"),
        SALA("Salary & Benefits", "SalaryPayment"),
        SSBE("Salary & Benefits", "SocialSecurityBenefit"),
        LBIN("Securities Lending", "Lending Buy-In Netting"),
        LCOL("Securities Lending", "Lending Cash Collateral Free Movement"),
        LFEE("Securities Lending", "Lending Fees"),
        LMEQ("Securities Lending", "Lending Equity marked-to-market  cash collateral"),
        LMFI("Securities Lending", "Lending Fixed Income marked-to-market cash collateral"),
        LMRK("Securities Lending", "Lending unspecified type of marked-to-market cash collateral"),
        LREB("Securities Lending", "Lending rebate payments"),
        LREV("Securities Lending", "Lending Revenue Payments"),
        LSFL("Securities Lending", "Lending Claim Payment"),
        ESTX("Tax", "EstateTax"),
        FWLV("Tax", "Foreign Worker Levy "),
        GSTX("Tax", "Goods & Services Tax "),
        HSTX("Tax", "HousingTax"),
        INTX("Tax", "IncomeTax"),
        NITX("Tax", "NetIncomeTax"),
        PTXP("Tax", "Property Tax "),
        RDTX("Tax", "Road Tax "),
        TAXS("Tax", "TaxPayment"),
        VATX("Tax", "ValueAddedTaxPayment"),
        WHLD("Tax", "WithHolding"),
        TAXR("Tax ", "TaxRefund"),
        B112("Trailer Fee", "Trailer Fee Payment"),
        BR12("Trailer Fee", "Trailer Fee Rebate"),
        TLRF("Trailer Fee", "Non-US mutual fund trailer fee payment"),
        TLRR("Trailer Fee", "Non-US mutual fund trailer fee rebate payment"),
        AIRB("Transport", "Air"),
        BUSB("Transport", "Bus"),
        FERB("Transport", "Ferry"),
        RLWY("Transport", "Railway"),
        TRPT("Transport", "RoadPricing"),
        CBTV("Utilities", "CableTVBill"),
        ELEC("Utilities", "ElectricityBill"),
        ENRG("Utilities", "Energies"),
        GASB("Utilities", "GasBill"),
        NWCH("Utilities", "NetworkCharge"),
        NWCM("Utilities", "NetworkCommunication"),
        OTLC("Utilities", "OtherTelecomRelatedBill"),
        PHON("Utilities", "TelephoneBill"),
        UBIL("Utilities", "Utilities "),
        WTER("Utilities", "WaterBill");


        private final EnumInfo info;

        PurposeCode(String nickname, String description) {
            info = new EnumInfo(nickname, description);
        }

        public EnumInfo getInfo() {
            return info;
        }
    }

    enum CategoryPurposeCode {
        BONU("BonusPayment", "Transaction is the payment of a bonus."),
        CASH("CashManagementTransfer", "Transaction is a general cash management instruction."),
        CBLK("Card Bulk Clearing", " A Service that is settling money for a bulk of card transactions, while referring to a specific transaction file or other information like terminal ID, card acceptor ID or other transaction details."),
        CCRD("Credit Card Payment", "Transaction is related to a payment of credit card."),
        CORT("TradeSettlementPayment", "Transaction is related to settlement of a trade, eg a foreign exchange deal or a securities transaction."),
        DCRD("Debit Card Payment", "Transaction is related to a payment of debit card."),
        DIVI("Dividend", "Transaction is the payment of dividends."),
        DVPM("DeliverAgainstPayment", "Code used to pre-advise the account servicer of a forthcoming deliver against payment instruction."),
        EPAY("Epayment", "Transaction is related to ePayment."),
        FCOL("Fee Collection", "A Service that is settling card transaction related fees between two parties."),
        GOVT("GovernmentPayment", "Transaction is a payment to or from a government department."),
        HEDG("Hedging", "Transaction is related to the payment of a hedging operation."),
        ICCP("Irrevocable Credit Card Payment", "Transaction is reimbursement of credit card payment."),
        IDCP("Irrevocable Debit Card Payment", "Transaction is reimbursement of debit card payment."),
        INTC("IntraCompanyPayment", "Transaction is an intra-company payment, ie, a payment between two companies belonging to the same group."),
        INTE("Interest", "Transaction is the payment of interest."),
        LOAN("Loan", "Transaction is related to the transfer of a loan to a borrower."),
        MP2B("Commercial", "Mobile P2B Payment"),
        MP2P("Consumer", "Mobile P2P Payment"),
        OTHR("OtherPayment", "Other payment purpose."),
        PENS("PensionPayment", "Transaction is the payment of pension."),
        RVPM("ReceiveAgainstPayment", "Code used to pre-advise the account servicer of a forthcoming receive against payment instruction."),
        SALA("SalaryPayment", "Transaction is the payment of salaries."),
        SECU("Securities", "Transaction is the payment of securities."),
        SSBE("SocialSecurityBenefit", "Transaction is a social security benefit, ie payment made by a government to support individuals."),
        SUPP("SupplierPayment", "Transaction is related to a payment to a supplier."),
        TAXS("TaxPayment", "Transaction is the payment of taxes."),
        TRAD("Trade", "Transaction is related to the payment of a trade finance transaction."),
        TREA("TreasuryPayment", "Transaction is related to treasury operations.  E.g. financial contract settlement."),
        VATX("ValueAddedTaxPayment", "Transaction is the payment of value added tax."),
        WHLD("WithHolding", "Transaction is the payment of withholding tax."),
        PTM("PTM", "PTM");

        private final EnumInfo info;

        CategoryPurposeCode(String nickname, String description) {
            info = new EnumInfo(nickname, description);
        }

        public EnumInfo getInfo() {
            return info;
        }
    }

    enum ServiceLevel {

        BKTR("Book Transaction", "Payment through internal book transfer",""),
        NUGP("Non-urgent Priority Payment", "Payment must be executed as a non-urgent transaction with priority settlement.",""),
        NURG("Non-urgent Payment", "Payment must be executed as a non-urgent transaction, which is typically identified as an ACH or low value transaction.",""),
        PRPT("EBAPriorityService", "Transaction must be processed according to the EBA Priority Service.",""),
        SDVA("SameDayValue", "Payment must be executed with same day value to the creditor.",""),
        SEPA("SingleEuroPaymentsArea", "Payment must be executed following the Single Euro Payments Area scheme.",""),
        SVDE("Domestic Cheque Clearing and Settlement", "Payment execution following the cheque agreement and traveller cheque agreement of the German Banking Industry Committee (Die Deutsche Kreditwirtschaft - DK) and Deutsche Bundesbank – Scheck Verrechnung Deutschland",""),
        URGP("Urgent Payment", "Payment must be executed as an urgent transaction cleared through a real-time gross settlement system, which is typically identified as a wire or high value transaction.",""),
        URNS("Urgent Payment Net Settlement", "Payment must be executed as an urgent transaction cleared through a real-time net settlement system, which is typically identified as a wire or high value transaction.","");

        private final EnumInfo info;
        private final String s2bId;

        ServiceLevel(String nickname, String description, String s2bId) {
            info = new EnumInfo(nickname, description);
            this.s2bId = s2bId;
        }

        public EnumInfo getInfo() {
            return info;
        }

        public String getS2BId() {
            return s2bId;
        }
    }


    @SuppressWarnings("unused")
    enum LocalInstrumentType {

        DDMC("DirectDebitConfirmedElectronicMandate", "Transaction is related to a direct debit instruction authorized under a confirmed electronic mandate.", "APAC", "HK", "All", "eDDA", "DD", " Both "),
        DDMP("DirectDebitPaperMandateWithPaperAuthorization", "Transaction is related to a direct debit instruction authorized under a paper based mandate, supported by paper authorization signed by the debtor.", "APAC", "HK", "", "All", "eDDA", "DDBoth "),
        DDMU("DirectDebitUnconfirmedElectronicMandate", "Transaction is related to a direct debit instruction authorized under an unconfirmed electronic mandate requiring confirmation by the debtor.", "APAC", "HK", "All", "eDDA", "DD", "Both"),
        TRF("Credit Transfers", "Transaction is related to credit transfers", "EMEA", "AT", "All", "correspondent banking", "CT", ""),
        _82("Non-pre authorised Direct Debit", "Transaction is related to a direct debit that is not pre authorised (Einzugsermächtigung).", "EMEA", "AT", "EUR", " Domestic AT", "DD", "Both"),
        _83("Pre authorised Direct Debit", "Transaction is related to a direct debit that is pre authorised (Abbuchungsauftrag).", "EMEA", "AT", "EUR", " Domestic AT", "DD", "Both"),
        CPP("Cash Per Post", "Transaction is related to cash per post. Transaction to ultimate recipient having no bank account. Primary beneficiary is a postal service provider. Funds are paid out by cash. Additional necessary information for address and delivery options need to be attached.", "EMEA", "AT", "EUR", "correspondent banking", "CT", ""),
        RTR("Returned Credit", "TransfersTransaction is related to returned credit transfers.", "EMEA", "AT", "EUR", "correspondent banking", "CT", ""),
        GST("Truncated Credit Transfers", "Transaction is related to truncated credit transfers. Conversion of physical instrument to electonric form for transmission to the paying bank and where the original paper document does not continue in the clearing process..The original instrument rules are retained throughout the life of the instrument. Transaction triggered by specific marked and populated paper slip. Reconciliation reference is secured by check digits supporting secure optical recognition. All other remittance information is truncated prior transmission.", "EMEA", "AT", "EUR", "correspondent banking", "CT", ""),
        DDT("Direct Debits", "Transaction is related to direct debits.", "EMEA", "AT", "EUR", "correspondent banking", "DD", ""),
        RDD("Returned Direct Debits", "Transaction is related to returned direct debits.", "EMEA", "AT", "EUR", "correspondent banking", "DD", ""),
        CHN("Truncated Checks", "Transaction is related to truncated checks. Conversion of physical instrument to electonric form for transmission to the paying bank and where the original paper document does not continue in the clearing process..The original instrument rules are retained throughout the life of the instrument.", "EMEA", "AT", "EUR", "correspondent banking", "DD", ""),
        STR("Revoked Credit Transfers", "Transaction is related to revoked credit transfers", "EMEA", "AT", "EUR", "correspondent banking", "other", ""),
        SDD("Revoked Direct DE bits", "Transaction is related to revoked direct debits.", "EMEA", "AT", "EUR", "correspondent banking", "other", ""),
        SRT("Revoked ReturnedCredit Transfers", "Transaction is related to revoked returned credit transfers", "EMEA", "AT", "EUR", "correspondent banking", "other", ""),
        SRD("Revoked Returned Direct Debits", "Transaction is related to revoked returned direct debits.", "EMEA", "AT", "EUR", "correspondent banking", "other", ""),
        SCN("Revoked Truncated Checks", "Transaction is related to revoked truncated checks.", "EMEA", "AT", "EUR", "correspondent banking", "other", ""),
        SGT("Revoked Truncated Credit Transfers", "Transaction is related to revoked truncated credit transfers.", "EMEA", "AT", "EUR", "correspondent banking", "other", ""),
        CARD("Card Clearing", "Transaction is related to card clearing.", "EMEA", "DE", "EUR", "All", "Both", ""),
        _05("Non-pre authorised Direct Debit", "Transaction is related to a direct debit that is not pre authorised (Einzugsermächtigung).", "EMEA", "DE", "EUR", "Domestic DE", "DD", "Both"),
        _04("Pre authorised Direct Debit", "Transaction is related to a direct debit that is pre authorised (Abbuchungsauftrag).", "EMEA", "DE", "EUR", "Domestic DE", "DD", "Both"),
        ISE("Image-based Cheque Collection", "Transaction is related to the German Image-based Cheque Collection Procedure “Imagegestützter Scheckeinzug - ISE", "EMEA", "DE", "EUR", "Domestic DE", "Other", "Bk2Bk"),
        BSE("Paperless Cheque Collection", "Transaction is related to the German Paperless Cheque Collection procedure “Belegloser Scheckeinzug - BSE”", "EMEA", "DE", "EUR", " Domestic DE", "Other", "Bk2Bk"),
        IN("Cross Border Customer Credit Transfer", "Transaction is related to cross border customers credit transfers", "EMEA", "DK", "All", "All", "CT", ""),
        _58("Business-to-business Direct Debit", "Transaction is related to a business-to-business direct debit (CSB58).", "EMEA", "ES", "EUR", " Domestic ES", "DD", "Both"),
        _19("Business-to-customer Direct Debit", "Transaction is related to a business-to-customer direct debit (CSB19).", "EMEA", "ES", "EUR", " Domestic ES", "DD", "Both"),
        _85("Pre-authorised Direct Debit Accéléré (Accelerated clearing / 2 Day)Ordinaire (Normal clearing / 4 Day)", "Transaction is related to an urgent direct debit that is pre authorised (Avis de Prélèvement accéléré).", "EMEA", "FR", "EUR", " Domestic FR", "DD", "Both"),
        _08("Pre-authorised Direct Debit Ordinaire (Normal clearing / 4 Day)", "Transaction is related to a direct debit that is pre authorised (Avis de Prélèvement).", "EMEA", "FR", "EUR", " Domestic FR", "DD", "Both"),
        _89("Pre-authorised Direct Debit Vérifié  (Verified clearing)", "Transaction is related to an urgent direct debit that is pre authorised (Avis de Prélèvement vérifié).", "EMEA", "FR", "EUR", " Domestic FR", "DD", "Both"),
        _60("Recovered Bill of Exchange or Promissory Note", "LCR - Lettre de Change Relevé (Recovered Bill of Exchange) and BOR - Billet à Orde Relevé (Promissory Note)", "EMEA", "FR", "EUR", " Domestic FR", "Other", "Both"),
        RIBA("Non-pre authorised direct debit", "Transaction is related to a non-pre authorised collection (RIBA).", "EMEA", "IT", "EUR", " Domestic IT", "DD", "Both"),
        RIDO("Pre authorised revocable Direct Debit", "Transaction is related to a direct debit that is pre authorised and revocable (RID Ordinario).", "EMEA", "IT", "EUR", " Domestic IT", "DD", "Both"),
        RIDV("Pre authorised revocable urgent Direct Debit", "Transaction is related to an urgent direct debit that is pre authorised and revocable (RID Veloce).", "EMEA", "IT", "EUR", " Domestic IT", "DD", "Both"),
        IDEAL("Payments via Internet owned by Currence", "Transaction is related to payments via internet owned by Currence.", "EMEA", "NL", "EUR", "All", "Both", ""),
        INSTNT01("InstantCreditTransferNotTimeCritical", "The transaction is related to a regular  Credit Transfer and will be instantly processed under the Dutch AOS on top of the EPC SCT scheme.", "EMEA", "NL", "EUR", "All", "CT", "Both"),
        INSTTC01("InstantCreditTransferTimeCritical", "The transaction is related to an Instant Credit Transfer under the rules of the Dutch AOS on top of the EPC SCT Inst scheme.", "EMEA", "NL", "EUR", "All", "CT", "Both"),
        INSTIDEAL("Payments via Internet owned by Currence using Instant Credit Transfer", "Transaction is related to payments via internet owned by Currence which uses an Instant Credit Transfer.", "EMEA", "NL", "EUR", "All", "CT", "Both"),
        INSTNT01IDEAL("Payments via Internet owned by Currence using InstantCreditTransferNotTimeCritical", "Transaction is related to payments via internet owned by Currence which uses a regular  Credit Transfer and will be instantly processed under the Dutch AOS on top of the EPC SCT scheme.", "EMEA", "NL", "EUR", "All", "CT", "Both"),
        INSTTC01IDEAL("Payments via Internet owned by Currence using InstantCreditTransferTimeCritical", "Transaction is related to payments via internet owned by Currence which uses an Instant Credit Transfer under the rules of the Dutch AOS on top of the EPC SCT Inst scheme.", "EMEA", "NL", "EUR", "All", "CT", "Both"),
        NLDO("Dutch Domestic Bulk Payment", "Transaction is related to a Domestic payment initiated by PAIN.001", "EMEA", "NL", "EUR", "All", "CT", ""),
        NLUP("Dutch Urgent Payment", "Transaction is related to a Domestic payment initiated by PAIN.001", "EMEA", "NL", "EUR", "All", "CT", ""),
        ACCEPT("Payment via Acceptgiro owned by Currence", "Transaction is related to payments via Acceptgiro owned by Currence.", "EMEA", "NL", "EUR", "All", "CT", ""),
        SDN("Payments via Standaard Digitale Nota", "Transaction is related to payments via a ‘Standaard Digitale Nota’ InvoiceAcceptgiro payment.", "EMEA", "NL", "EUR", "All", "CT", ""),
        NLGOV("Direct debit initiated by the government with special conditions", "Transaction is related to direct debit scheme owned by the NVB", "EMEA", "NL", "EUR", "All", "DD", ""),
        _0090("Mass Payment Beneficiary", "Transaction is related to mass payment beneficiary.", "EMEA", "NL", "EUR", "CSS", "CT", ""),
        _0091("Mass Payment Ours", "Transaction is related to mass payment ours.", "EMEA", "NL", "EUR", "CSS", "CT", ""),
        _0092("Mass Payment Shared", "Transaction is related to mass payment shared.", "EMEA", "NL", "EUR", "CSS", "CT", ""),
        _0002("Standing Order", "Transaction is related to standing order.", "EMEA", "NL", "EUR", "CSS", "CT", ""),
        _0221("One-off Authorisation", "Transaction is related to one-off authorisation.", "EMEA", "NL", "EUR", "CSS", "DD", ""),
        _0224("One-off Authorisation Charities", "Transaction is related to one-off authorisation charities.", "EMEA", "NL", "EUR", "CSS", "DD", ""),
        _0226("One-off Authorisation Construction Industry", "Transaction is related to one-off authorisation construction industry.", "EMEA", "NL", "EUR", "CSS", "DD", ""),
        _0225("One-off Authorisation Tuition Fees", "Transaction is related to one-off authorisation tuition fees.", "EMEA", "NL", "EUR", "CSS", "DD", ""),
        _0222("Standing Authorisation Companies", "Transaction is related to standing authorisation companies.", "EMEA", "NL", "EUR", "CSS", "DD", ""),
        _0227("Standing Authorisation Companies Without Debtor Revocation Right", "Transaction is related to standing authorisation companies without debtor revocation right.", "EMEA", "NL", "EUR", "CSS", "DD", ""),
        _0220("Standing Authorisation General", "Transaction is related to standing authorisation general.", "EMEA", "NL", "EUR", "CSS", "DD", ""),
        _0223("Standing Authorisation Lotteries", "Transaction is related to standing authorisation lotteries.", "EMEA", "NL", "EUR", "CSS", "DD", ""),
        _0001("Converted (Bank) Payment", "Transaction is related to converted (bank) payment.Conversion of physical instrument to electonric form for transmission to the paying bank and where the original paper document does not continue in the clearing process.The instrument rules change upon conversion.", "EMEA", "NL", "EUR", "CSS", " CT", ""),
        _0000("Business Payment", "Transaction is related to business payment", "EMEA", "NL", "EUR", "CSS", " (Equens Clearing & Settlement System)CT", ""),
        _IN("Cross Border Customer Credit Transfer", "Transaction is related to cross border customer credit transfer.", "EMEA", "SE", "All", "All", "CT", ""),
        ONCL("Overnight", "Transaction is related to overnight clearing.", "EMEA", "SEPA", "EUR", "Corporate banking", "CT", "B2B"),
        SDCL("Same Day", "Transaction is related to same day clearing.", "EMEA", "SEPA", "EUR", "Corporate banking", "CT", "B2B"),
        DDNR("CoreNoRefund", "SEPA Core Direct Debit with ‘no refund’ option", "EMEA", "SEPA", "EUR", "PEACH", "DD", "B2B"),
        DDFA("DirectDebitFixedAmount", "SEPA Fixed Amount Direct Debit", "EMEA", "SEPA", "EUR", "PEACH", "DD", "B2B"),
        CORE("SEPA Direct Debit - Core", " Transaction is related to SEPA direct debit -core.", "EMEA", "SEPA", "EUR", "PEACH", "DD", "Both"),
        B2BAMIPM("SEPA B2B Direct Debit AMI", "SEPA B2B Direct Debit AMI based on a paper mandate", "EMEA", "SEPA", "EUR", "PEACH", "DD", ""),
        B2B("SEPA Business to Business Direct Debit", "Transaction is related to SEPA business to business direct debit.", "EMEA", "SEPA", "EUR", "PEACH", "DD", ""),
        CR1AMIPM("SEPA Core D-1 Direct Debit AMI", "Optional shorter time cycle (D-1) for SEPA Core Direct Debit AMI based on a paper mandate", "EMEA", "SEPA", "EUR", "PEACH", "DD", ""),
        CORAMIPM("SEPA Core Direct Debit AMI", "SEPA Core Direct Debit AMI based on a paper mandate", "EMEA", "SEPA", "EUR", "PEACH", "DD", ""),
        COR1("SEPA Direct Debit - 1 Day Settlement", "Optional shorter time cycle (D-1) for SEPA Core Direct Debit", "EMEA", "SEPA", "EUR", "PEACH", "DD", ""),
        FADAMIPM("SEPA FA Direct Debit AMI", "SEPA Fixed Amount Direct Debit AMI based on a paper mandate", "EMEA", "SEPA", "EUR", "PEACH", "DD", ""),
        INST("Instant Credit Transfer", "Transaction is related to an Instant Credit Transfer.Use case example: SEPA Instant Credit Transfer (SCT Inst)", "GLOBAL", "GLOBAL", "All", "All", "CT", ""),
        ADD("AuthenticatedDirectDebit", "Transaction is unauthenticated direct debit for domestic use.", "GLOBAL", "GLOBAL", "All", "All", "DD", "Both"),
        UDD("UnauthenticatedDirectDebit", "Transaction is authenticated direct debit for domestic use.", "GLOBAL", "GLOBAL", "All", "All", "DD", "Both"),
        CCI("Cash Concentration Intragroup", "Transaction is related to an intra-group bank initiated cash management payment", "GLOBAL", "GLOBAL", "All", "All", "DD, CT and CPAR", ""),
        BTR("Bank Transfer", "Transaction is related to a bank transfer.", "WHEM", "US", "USD", "Fedwire", "CT", "B2B"),
        CKS("Check Same Day Settlement Wire", "Transaction is related to check same day settlement wire.", "WHEM", "US", "USD", "Fedwire", "CT", "B2B"),
        CTR("Customer Transfer", "Transaction is related to customer transfer.", "WHEM", "US", "USD", "Fedwire", "CT", "B2B"),
        CTP("Customer Transfer Plus", "Transaction is related to a customer transfer, which may include information related to a cover payment or extended remittance information. ", "WHEM", "US", "USD", "Fedwire", "CT", "B2B"),
        DEP("Deposit to Sender’s Account", "Transaction is related to deposit to sender's account.", "WHEM", "US", "USD", "Fedwire", "CT", "B2B"),
        FFR("Fed Funds Returned", "Transaction is related to Fed funds returned.", "WHEM", "US", "USD", "Fedwire", "CT", "B2B"),
        FFS("Fed Funds Sold", "Transaction is related to Fed funds sold.", "WHEM", "US", "USD", "Fedwire", "CT", "B2B"),
        SVC("Non-Value Service Message", "Transaction is related to non-value service message.", "WHEM", "US", "USD", "Fedwire", "Non-value Service Message", "B2B"),
        DRW("Drawdown Response (Value) to Honor a Drawdown Request", "Transaction is related to drawdown response (value) to honor a drawdown request.", "WHEM", "US", "USD", "Fedwire", "other", "B2B"),
        DRB("Bank-to-Bank Drawdown Request or Response (Non-value)", "Transaction is related to bank-to-bank drawdown request or response (non-value)", "WHEM", "US", "USD", "Fedwire", "Request for credit", "B2B"),
        DRC("Customer or Corporate Drawdown Request or Response (Non-value)", "Transaction is related to customer or corporate drawdown request or response (non-value).", "WHEM", "US", "USD", "Fedwire", "Request for credit", "B2B"),
        IAT("International ACH", "Transaction is related to international ACH.", "WHEM", "US", "USD", "NACHA", "Both", "Both"),
        CCD("Cash Concentration or Disbursement Corporate counterparty.", "Transaction is related to cash concentration or disbursement corporate counterparty.", "WHEM", "US", "USD", "NACHA", "Both plus prenote.", "Both"),
        CTX("Corporate Trade Exchange", "Transaction is related to corporate trade exchange.", "WHEM", "US", "USD", "NACHA", "Both plus prenote.", "Both"),
        PPD("Prearranged Payment or Deposit.  Consumer counterparty.", "Transaction is related to prearranged payment or deposit consumer counterparty.", "WHEM", "US", "USD", "NACHA", "Both plus prenote.", "Both"),
        CIE("Customer Initiated Entry", "A credit entry initiated by or on behalf of the holder of a consumer account", "WHEM", "US", "USD", "NACHA", "CT", "Both"),
        RCK("Re-presented Check Entry", "Transaction is related to re-presented check entry.", "WHEM", "US", "USD", "NACHA", "CT", "Both"),
        ARC("Accounts Receivable Check", "Transaction is related to accounts receivable check.", "WHEM", "US", "USD", "NACHA", "DD", "Both"),
        WEB("Internet Initiated Entry", "Transaction is related to internet initiated entry.", "WHEM", "US", "USD", "NACHA", "DD", "Both"),
        POP("Point-Of-Purchase", "Transaction is related to point-of-purchase.", "WHEM", "US", "USD", "NACHA", "DD", "Both"),
        POS("Point-Of-Sale", "Transaction is related to point-of-sale.", "WHEM", "US", "USD", "NACHA", "DD", "Both"),
        TEL("Telephone Initiated Entry", "Transaction is related to telephone initiated entry.", "WHEM", "US", "USD", "NACHA", "DD", "Both"),
        IBFT("IBFT", "IBFT", "IBFT", "ALL", "ALL", "", "", "Both" ),
        TT("Telegraphic Transfer", "TT", "TT", "ALL", "ALL", "", "", "Both"),
        BT("Book Transfer","BT" ,"BT" , "ALL", "ALL","" ,"" ,"Both" );

        private final String name;
        private final String definition;
        private final String region;
        private final String isoCountry;
        private final String isoCurrency;
        private final String paymentSystem;
        private final String ddCtBoth;
        private final String copy2bk;

        LocalInstrumentType(String name, String definition, String region, String isoCountry, String isoCurrency, String paymentSystem, String ddCtBoth, String copy2bk) {
            this.name = name;
            this.definition = definition;
            this.region = region;
            this.isoCountry = isoCountry;
            this.isoCurrency = isoCurrency;
            this.paymentSystem = paymentSystem;
            this.ddCtBoth = ddCtBoth;
            this.copy2bk = copy2bk;
        }

        public String getName() {
            return name;
        }

        public String getDefinition() {
            return definition;
        }

        public String getRegion() {
            return region;
        }

        public String getIsoCountry() {
            return isoCountry;
        }

        public String getIsoCurrency() {
            return isoCurrency;
        }

        public String getPaymentSystem() {
            return paymentSystem;
        }

        public String getDdCtBoth() {
            return ddCtBoth;
        }

        public String getCopy2bk() {
            return copy2bk;
        }
    }

}
