package com.scb.api.client.model.custody;

import com.scb.api.client.model.CurrencyCode;
import org.joda.time.LocalDate;

import java.math.BigDecimal;
import java.util.Objects;

public class TransactionStatus {

    private String groupId;
    private String id;
    private String securitiesAccount;
    private TransactionReference transactionReference;
    private String securitiesMovementType = "";
    private String status = "";
    private ListedSecurity security;
    private BigDecimal settlementQuantity;
    private BigDecimal settlementAmount;
    private CurrencyCode currencyCode;
    private String depository;
    private String counterpartyName;
    private String correspondenceMode;
    private String businessPartnerId = "";

    private String messageCode = "";
    private String narrative = "";

    private LocalDate tradeDate;

    private LocalDate settlementDate = LocalDate.now();

    private TransactionStatus() {

    }

    public String getGroupId() {
        return groupId;
    }

    public String getId() {
        return id;
    }

    public String getSecuritiesAccount() {
        return securitiesAccount;
    }

    public LocalDate getTradeDate() {
        return tradeDate;
    }

    public TransactionReference getTransactionReference() {
        return transactionReference;
    }

    public String getSecuritiesMovementType() {
        return securitiesMovementType;
    }

    public String getStatus() {
        return status;
    }

    public LocalDate getSettlementDate() {
        return settlementDate;
    }

    public ListedSecurity getSecurity() {
        return security;
    }

    public BigDecimal getSettlementQuantity() {
        return settlementQuantity;
    }

    public BigDecimal getSettlementAmount() {
        return settlementAmount;
    }

    public CurrencyCode getCurrencyCode() {
        return currencyCode;
    }

    public String getDepository() {
        return depository;
    }

    public String getCounterpartyName() {
        return counterpartyName;
    }

    public String getCorrespondenceMode() {
        return correspondenceMode;
    }

    public String getBusinessPartnerId() {
        return businessPartnerId;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public String getNarrative() {
        return narrative;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransactionStatus that = (TransactionStatus) o;
        return Objects.equals(groupId, that.groupId) &&
                Objects.equals(id, that.id) &&
                Objects.equals(securitiesAccount, that.securitiesAccount) &&
                Objects.equals(transactionReference, that.transactionReference) &&
                Objects.equals(securitiesMovementType, that.securitiesMovementType) &&
                Objects.equals(status, that.status) &&
                Objects.equals(security, that.security) &&
                Objects.equals(settlementQuantity, that.settlementQuantity) &&
                Objects.equals(settlementAmount, that.settlementAmount) &&
                Objects.equals(currencyCode, that.currencyCode) &&
                Objects.equals(depository, that.depository) &&
                Objects.equals(counterpartyName, that.counterpartyName) &&
                Objects.equals(correspondenceMode, that.correspondenceMode) &&
                Objects.equals(businessPartnerId, that.businessPartnerId) &&
                Objects.equals(messageCode, that.messageCode) &&
                Objects.equals(narrative, that.narrative) &&
                Objects.equals(tradeDate, that.tradeDate) &&
                Objects.equals(settlementDate, that.settlementDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId, id, securitiesAccount, transactionReference, securitiesMovementType, status, security, settlementQuantity, settlementAmount, currencyCode, depository, counterpartyName, correspondenceMode, businessPartnerId, messageCode, narrative, tradeDate, settlementDate);
    }
}
