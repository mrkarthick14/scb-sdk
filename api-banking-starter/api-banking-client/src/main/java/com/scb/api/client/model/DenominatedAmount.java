package com.scb.api.client.model;

import java.math.BigDecimal;

public class DenominatedAmount {

    public static final DenominatedAmount ZERO = new DenominatedAmount("USD", BigDecimal.ZERO);
    private String currencyCode;
    private BigDecimal amount;

    public DenominatedAmount() {

    }

    public DenominatedAmount(String currencyCode, BigDecimal amount) {
        this.currencyCode = currencyCode;
        this.amount = amount != null ? amount.stripTrailingZeros() : null;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    
/*---------------------------------------------------------------------------*/
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DenominatedAmount that = (DenominatedAmount) o;

        if (currencyCode != null ? !currencyCode.equals(that.currencyCode) : that.currencyCode != null) return false;
        return amount != null ? amount.equals(that.amount) : that.amount == null;

    }

    @Override
    public int hashCode() {
        int result = currencyCode != null ? currencyCode.hashCode() : 0;
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        return result;
    }
/*-----------------------------------------------------------------------------------*/
    public static DenominatedAmount of(BigDecimal amount, String currency) {
        return new DenominatedAmount(currency, amount);
    }
}
