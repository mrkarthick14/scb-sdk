package com.scb.api.client.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.OpenAPI;
import com.scb.api.client.UPI;
import com.scb.api.client.model.ActivationRequest;
import com.scb.api.client.model.EventAck;
import com.scb.api.client.model.MessageBatch;
import com.scb.api.client.model.QueueStatistics;
import com.scb.api.client.util.EncryptDecrypt;
import com.scb.api.client.util.HttpUtility;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.security.KeyStoreException;

import static com.scb.api.client.util.Maps.map;

public class ApiBankingImpl implements ApiBanking {

    private static final Logger logger = LoggerFactory.getLogger(ApiBankingImpl.class);

    private final HttpUtility httpUtility;

    private KeyStore keyStore;
    private char[] keyPassword;
    private final String issuerId;
    private final boolean encryptedResponses;
    private ApiBankingEnvironment environment;
    private final String signatureKeyAlias;

    private ObjectMapper objectMapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false )
            .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,false);

    public ApiBankingImpl(HttpClientBuilder httpClientBuilder, KeyStore keyStore, char[] keyPassword, String signatureKeyAlias,
                          String issuerId,
                          boolean encryptedResponses,
                          ApiBankingEnvironment environment) {
        this.keyStore = keyStore;
        this.keyPassword = keyPassword;
        this.issuerId = issuerId;
        this.encryptedResponses = encryptedResponses;
        this.environment = environment;
        this.signatureKeyAlias = signatureKeyAlias;
        try {
            this.httpUtility = new HttpUtility(httpClientBuilder.build(), issuerId, keyStore, signatureKeyAlias, keyPassword,
                    objectMapper,
                    environment);
        } catch (KeyStoreException e) {
            logger.error("Error initialising API Banking client: " + e.getMessage());
            throw new RuntimeException(e.getMessage(),e);
        }
    }

    @Override
    public InputStream activate(InputStream activationKeyContent, String messageId, boolean enableWebHook, String webHookUrl) {

        logger.info("Invoking SCB API Banking Activation..");
        try {
            return httpUtility.performPost(URI_ACTIVATE,
                    messageId,
                    false, //responses to activate are not encrypted
                    constructActivationBody(activationKeyContent, webHookUrl, enableWebHook),null);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(),e);
        }

    }

    @Override
    public QueueStatistics statistics(String messageId) throws IOException {
        logger.info("Invoking SCB API Banking Queue Statistics..");
        InputStream response = httpUtility.performGet(URI_STATS, encryptedResponses, messageId,null);
        
        //noinspection unchecked
        return objectMapper.readValue(response, QueueStatistics.class);
    }

    @Override
    public MessageBatch peek(String messageId) throws IOException {
        logger.info("Invoking SCB API Banking PEEK..");
        InputStream response = httpUtility.performGet(URI_PEEK, encryptedResponses, messageId,null);
        //noinspection unchecked
        return objectMapper.readValue(response, MessageBatch.class);
    }

    @Override
    public MessageBatch consume(String messageId, int batchSize) throws IOException {
        logger.info("Invoking SCB API Banking CONSUME with batch size of " + batchSize + ".. ");
        InputStream response = httpUtility.performPost(URI_CONSUME, messageId,
                encryptedResponses, map("batchSize", batchSize, "messageId", messageId),null);
        
       
        //noinspection unchecked
        return objectMapper.readValue(response, MessageBatch.class);
    }

    @Override
    public MessageBatch consume(String messageId) throws IOException {
        logger.info("Invoking SCB API Banking CONSUME..");
        InputStream response = httpUtility.performGet(URI_CONSUME, encryptedResponses, messageId,null);
        InputStreamReader isReader = new InputStreamReader(response);
        BufferedReader reader = new BufferedReader(isReader);
        StringBuffer sb = new StringBuffer();
        String str;
        
        while((str = reader.readLine())!=null){
      		sb.append(str);
       	}
       	System.out.println("InputStream to string " + sb.toString());
        //noinspection unchecked
        return objectMapper.readValue(response, MessageBatch.class);
    }

    @Override
    public MessageBatch recover(String messageId) throws IOException {
        logger.info("Invoking SCB API Banking RECOVER..");
        InputStream response = httpUtility.performGet(URI_RECOVER, encryptedResponses, messageId,null);
        //noinspection unchecked
        return objectMapper.readValue(response, MessageBatch.class);
    }

    @Override
    public <T> EventAck failure(String messageId, String transactionId, T failureReport) throws IOException {
        logger.info("Invoking SCB API Banking Failure Report for message id " + messageId + " and transaction id " + transactionId);
        InputStream response = httpUtility.performPost(URI_FAILURE, messageId,
                encryptedResponses, failureReport, map("TransactionId", transactionId));
        //noinspection unchecked
        return objectMapper.readValue(response, EventAck.class);
    }

    @Override
    public <T> EventAck success(String messageId, String transactionId, T successReport) throws IOException {
        logger.info("Invoking SCB API Banking Success Report for message id " + messageId + " and transaction id " + transactionId);
        InputStream response = httpUtility.performPost(URI_FAILURE, messageId,
                encryptedResponses, successReport, map("TransactionId", transactionId));
        //noinspection unchecked
        return objectMapper.readValue(response, EventAck.class);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T schema(Class<T> type) {
        if (type == OpenAPI.class) {
            return (T)new OpenAPIImpl(httpUtility,encryptedResponses);
        }
        if (type == UPI.class) {
            return (T)new UPIImpl(httpUtility);
        }
        return null;
    }

    private ActivationRequest constructActivationBody(InputStream inputStream, String webHookUrl, boolean enableWebHook) throws IOException {

        ObjectMapper om = new ObjectMapper();
        EncryptDecrypt.EncryptedContent activationKeyMap = om.readValue(inputStream, EncryptDecrypt.EncryptedContent.class);

        return new ActivationRequest(webHookUrl, enableWebHook, activationKeyMap );
    }


}
