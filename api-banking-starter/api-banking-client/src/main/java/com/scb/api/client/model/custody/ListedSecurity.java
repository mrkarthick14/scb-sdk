package com.scb.api.client.model.custody;

import java.util.Objects;

public class ListedSecurity {

    private String name = "";

    private String isin = "";

    public String getName() {
        return name;
    }

    public String getIsin() {
        return isin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ListedSecurity that = (ListedSecurity) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(isin, that.isin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, isin);
    }
}
