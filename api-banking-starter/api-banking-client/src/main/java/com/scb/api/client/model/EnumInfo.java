package com.scb.api.client.model;

public class EnumInfo {

    private final String name;
    private final String definition;

    public EnumInfo(String name, String definition) {
        this.name = name;
        this.definition = definition;
    }

    public String name() {
        return name;
    }

    public String definition() {
        return definition;
    }

    @Override
    public String toString() {
        return name + " (" + definition + ")";
    }
}

