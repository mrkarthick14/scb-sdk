package com.scb.api.client.model;

public class OpenApiMeta {

    private OpenApiLicense license;

    public OpenApiMeta() { }

    public OpenApiMeta(OpenApiLicense openApiLicense) {
        this.license = openApiLicense;
    }

    public OpenApiLicense getLicense() {
        return license;
    }
}
