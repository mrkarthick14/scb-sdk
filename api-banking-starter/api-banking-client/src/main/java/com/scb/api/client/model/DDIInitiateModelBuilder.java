package com.scb.api.client.model;

public class DDIInitiateModelBuilder {
	private String referenceId;
	private instructedAmount instructedAmount;
	private String mandateId;
	private OpenApiBankParty debtor;
	
	
	
	
	public DDIInitiateModelBuilder() {
	
	}




	public DDIInitiateModelBuilder(String referenceId, com.scb.api.client.model.instructedAmount instructedAmount,
			String mandateId, OpenApiBankParty debtor) {
		
		this.referenceId = referenceId;
		this.instructedAmount = instructedAmount;
		this.mandateId = mandateId;
		this.debtor = debtor;
	}
	
	
	
}
