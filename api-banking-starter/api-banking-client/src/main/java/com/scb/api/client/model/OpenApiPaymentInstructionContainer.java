package com.scb.api.client.model;

public class OpenApiPaymentInstructionContainer {

    private OpenApiPaymentInstructionInstruction paymentInstruction;

    private String batchReference;

    private String paymentIdentifier;

    public OpenApiPaymentInstructionContainer(OpenApiPaymentInstructionInstruction paymentInstructionInstruction, String batchReference, String paymentIdentifier) {
        this.paymentInstruction = paymentInstructionInstruction;
        this.batchReference = batchReference;
        this.paymentIdentifier = paymentIdentifier;
    }

    public OpenApiPaymentInstructionInstruction getPaymentInstruction() {
        return paymentInstruction;
    }

    public String getBatchReference() {
        return batchReference;
    }

    public String getPaymentIdentifier() {
        return paymentIdentifier;
    }
}
