
package com.scb.api.client.model;

public class OpenApiRemittanceInfo {

    private String unstructured;
    private OpenApiStructureRemittanceInfo structured;

    /**
     * No args constructor for use in serialization
     * 
     */
    public OpenApiRemittanceInfo() {
    }

    /**
     * 
     * @param unstructured
     * @param structured
     */
    public OpenApiRemittanceInfo(String unstructured, OpenApiStructureRemittanceInfo structured) {
        super();
        this.unstructured = unstructured;
        this.structured = structured;
    }

    public String getUnstructured() {
        return unstructured;
    }

    public void setUnstructured(String unstructured) {
        this.unstructured = unstructured;
    }

    public OpenApiStructureRemittanceInfo getStructured() {
        return structured;
    }

    public void setStructured(OpenApiStructureRemittanceInfo structured) {
        this.structured = structured;
    }

}
