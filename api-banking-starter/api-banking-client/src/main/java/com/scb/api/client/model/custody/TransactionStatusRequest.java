package com.scb.api.client.model.custody;


import org.joda.time.LocalDate;

import java.util.List;

public class TransactionStatusRequest {
    private String status;
    private String businessPartnerId;
    private List<String> securityAccountIds;
    private List<LocalDate> tradeDates;
    private List<LocalDate> settlementDate;
    private String transactionReference;
    private String clientTransactionReference;

    public TransactionStatusRequest(String businessPartnerId,
                                    List<String> securityAccountIds,
                                    String status,
                                    List<LocalDate> tradeDates,
                                    List<LocalDate> settlementDate,
                                    String transactionReference,
                                    String clientTransactionReference) {
        this.status = status;
        this.businessPartnerId = businessPartnerId;
        this.securityAccountIds = securityAccountIds;
        this.tradeDates = tradeDates;
        this.settlementDate = settlementDate;
        this.transactionReference = transactionReference;
        this.clientTransactionReference = clientTransactionReference;
    }

    public static class Builder {

        private String bpId;
        private List<String> scaIds;
        private String status;
        private List<LocalDate> tradeDates;
        private List<LocalDate> settlementDates;
        private String txnRef;
        private String clientTrnRef;


        public Builder withBusinessPartnerId(String bpId) {
            this.bpId = bpId;
            return this;
        }

        public Builder withSecurityAccountIds(List<String> scaIds) {
            this.scaIds = scaIds;
            return this;
        }

        public Builder withTradeDates(List<LocalDate> tradeDates) {
            this.tradeDates = tradeDates;
            return this;
        }

        public Builder withSettlementDates(List<LocalDate> settlementDates) {
            this.settlementDates = settlementDates;
            return this;
        }

        public Builder withTransactionReference(String txnRef) {
            this.txnRef = txnRef;
            return this;
        }

        public Builder withClientTransactionReference(String clientTxnRef) {
            this.clientTrnRef = clientTxnRef;
            return this;
        }

        public Builder withStatus(String status) {
            this.status = status;
            return this;
        }

        public TransactionStatusRequest build() {
            return new TransactionStatusRequest(
                    bpId,
                    scaIds,
                    status,
                    tradeDates,
                    settlementDates,
                    txnRef,
                    clientTrnRef);
        }
    }

    public String getStatus() {
        return status;
    }

    public String getBusinessPartnerId() {
        return businessPartnerId;
    }

    public List<String> getSecurityAccountIds() {
        return securityAccountIds;
    }

    public List<LocalDate> getTradeDates() {
        return tradeDates;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public String getClientTransactionReference() {
        return clientTransactionReference;
    }

    public List<LocalDate> getSettlementDate() {
        return settlementDate;
    }
}
