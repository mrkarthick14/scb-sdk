package com.scb.api.client.model;

import java.util.List;

public class OpenApiDDAStatus {
	private String mandateId;
	private String status;
	private String statusCode;
	private String statusMessage;
	private List<String>comments;
	private String referenceId;
	private String timestamp;
	
	
	
	public OpenApiDDAStatus() {
		
	}



	public OpenApiDDAStatus(String mandateId, String status, String statusCode, String statusMessage,
			List<String> comments, String referenceId, String timestamp) {
		
		this.mandateId = mandateId;
		this.status = status;
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.comments = comments;
		this.referenceId = referenceId;
		this.timestamp = timestamp;
	}



	public String getMandateId() {
		return mandateId;
	}



	public String getStatus() {
		return status;
	}



	public String getStatusCode() {
		return statusCode;
	}



	public String getStatusMessage() {
		return statusMessage;
	}



	public List<String> getComments() {
		return comments;
	}



	public String getReferenceId() {
		return referenceId;
	}



	public String getTimestamp() {
		return timestamp;
	}
	
	
	
	
}
