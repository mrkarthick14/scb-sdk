
package com.scb.api.client.model;

public class OpenApiPaymentInstruction {

    private OpenApiPaymentInstructionHeader header;

    private OpenApiPaymentInstructionInstruction instruction;

    public OpenApiPaymentInstruction(OpenApiPaymentInstructionHeader header, OpenApiPaymentInstructionInstruction instruction) {
        this.header = header;
        this.instruction = instruction;
    }

    public OpenApiPaymentInstruction() {
    }

    public OpenApiPaymentInstructionHeader getHeader() {
        return header;
    }

    public OpenApiPaymentInstructionInstruction getInstruction() {
        return instruction;
    }
}
