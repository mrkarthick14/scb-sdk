package com.scb.api.client.model.MetaData;

public class MetaDataModel {
	
	private String name;
	private Values values;
	public MetaDataModel(String name) {
		this.name =name;
	}
	public MetaDataModel(String name, Values values) {
		
		this.name = name;
		this.values = values;
	}
	public String getName() {
		return name;
	}
	public Values getValues() {
		return values;
	}
	
	public static MetaDataModel withName(String name){
		return new MetaDataModel(name);
	}
	
	public MetaDataModel withValues(Values values){
		this.values = values;
		return this;
	}
	
	public MetaDataModelBuilder build(){
		return new MetaDataModelBuilder(name,values);
	}
	
	
	

}
