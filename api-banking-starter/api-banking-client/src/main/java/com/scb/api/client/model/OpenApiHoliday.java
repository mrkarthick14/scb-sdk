package com.scb.api.client.model;

import java.time.LocalDate;

/**
 *
 * [{
        “description” : “holiday name”,
        "value":”2016-12-25”,
        “currency” : “USD”,
        “country” : “US”,
        "meta":{
            "license":{
               "id":"copyrightrbs2015",
                "name":"Copyright 2015 Royal Bank of Scotland"
            }
        }
    }
 ]

 *
 *
 */

public class OpenApiHoliday {

    private String description;
    private LocalDate value;
    private String currency;
    private String country;
    private OpenApiMeta meta;

    public OpenApiHoliday() {

    }

    public OpenApiHoliday(String description, LocalDate value, String currency, String country, OpenApiMeta meta) {
        this.description = description;
        this.value = value;
        this.currency = currency;
        this.country = country;
        this.meta = meta;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getValue() {
        return value;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCountry() {
        return country;
    }

    public OpenApiMeta getMeta() {
        return meta;
    }

}
