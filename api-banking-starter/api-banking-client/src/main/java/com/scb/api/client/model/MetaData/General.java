package com.scb.api.client.model.MetaData;

public class General {

		private String version;
		private String apiBankingEnabled;
		private String webhookUrl;
		private String publicKey;
		public General(String version, String apiBankingEnabled, String webhookUrl, String publicKey) {
			
			this.version = version;
			this.apiBankingEnabled = apiBankingEnabled;
			this.webhookUrl = webhookUrl;
			this.publicKey = publicKey;
		}
		
		
}
