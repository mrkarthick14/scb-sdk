
package com.scb.api.client.model;

public class OpenApiBankParty {

    private String legalEntityId;
    private String name;
    private OpenApiContact contact;

    public OpenApiBankParty() {}

    public OpenApiBankParty(String legalEntityId, String name, OpenApiContact contact) {
        this.legalEntityId = legalEntityId;
        this.name = name;
        this.contact = contact;
    }

    public String getLegalEntityId() {
        return legalEntityId;
    }

    public String getName() {
        return name;
    }

}
