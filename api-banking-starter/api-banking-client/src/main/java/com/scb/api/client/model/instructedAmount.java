package com.scb.api.client.model;

import java.math.BigDecimal;

public class instructedAmount {
	private String currencyCode;
	private BigDecimal amount;
	
	
	
	public instructedAmount() {
		
	}



	public instructedAmount(String currencyCode, BigDecimal amount) {
		
		this.currencyCode = currencyCode;
		this.amount = amount;
	}
	
	
	
}
