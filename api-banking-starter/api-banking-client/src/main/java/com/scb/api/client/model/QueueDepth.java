package com.scb.api.client.model;

public class QueueDepth {

    private int messageCount;
    private long timestamp;

    public QueueDepth(int messageCount, long timestamp) {
        this.messageCount = messageCount;
        this.timestamp = timestamp;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public long getTimestamp() {
        return timestamp;
    }

}
