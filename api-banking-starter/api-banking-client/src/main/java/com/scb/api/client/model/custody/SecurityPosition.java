package com.scb.api.client.model.custody;

import java.math.BigDecimal;
import java.util.Objects;

public class SecurityPosition {

    private BigDecimal settled;
    private BigDecimal net;
    private BigDecimal pendingReceive;
    private BigDecimal pendingDeliver;
    private BigDecimal blocked;


    public BigDecimal getSettled() {
        return settled;
    }

    public BigDecimal getNet() {
        return net;
    }

    public BigDecimal getPendingReceive() {
        return pendingReceive;
    }

    public BigDecimal getPendingDeliver() {
        return pendingDeliver;
    }

    public BigDecimal getBlocked() {
        return blocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SecurityPosition that = (SecurityPosition) o;
        return Objects.equals(settled, that.settled) &&
                Objects.equals(net, that.net) &&
                Objects.equals(pendingReceive, that.pendingReceive) &&
                Objects.equals(pendingDeliver, that.pendingDeliver) &&
                Objects.equals(blocked, that.blocked);
    }

    @Override
    public int hashCode() {
        return Objects.hash(settled, net, pendingReceive, pendingDeliver, blocked);
    }
}
