package com.scb.api.client.model;

public class OpenApiAccountOwner {

    private String id;
    private String provider;
    private String display_name;

    public OpenApiAccountOwner(String id, String provider, String display_name) {
        this.id = id;
        this.provider = provider;
        this.display_name = display_name;
    }

    public String getId() {
        return id;
    }

    public String getProvider() {
        return provider;
    }

    public String getDisplay_name() {
        return display_name;
    }
}
