package com.scb.api.client.osgi;

import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.model.QueueStatistics;
import com.scb.api.client.util.JWT;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.UUID;

public class Activator implements BundleActivator {


    private static final Logger logger = LoggerFactory.getLogger(Activator.class);

    private static final String SCB_API_BANKING_ENVIRONMENT =               "scb-api-banking.environment";
    private static final String SCB_API_BANKING_KEYSTORE_FILE =             "scb-api-banking.keystore-file";
    private static final String SCB_API_BANKING_KEYSTORE_PASSWORD =         "scb-api-banking.keystore-password";
    private static final String SCB_API_BANKING_KEYSTORE_SIGNATURE_ALIAS =  "scb-api-banking.keystore-signature-alias";
    private static final String SCB_API_BANKING_ISSUER_ID =                 "scb-api-banking.issuer-id";
    private static final String SCB_API_BANKING_PROXY_HOST =                "scb-api-banking.proxy.host";
    private static final String SCB_API_BANKING_PROXY_PORT =                "scb-api-banking.proxy.port";
    private static final String SCB_API_BANKING_ENCRYPT_RESPONSES =         "scb-api-banking.encrypt-responses";
    private static final String SCB_API_BANKING_JWT_ISSUE_OFFSET =          "scb-api-banking.jwt-time-offset";

    private ServiceRegistration<?> registration;

    @Override
    public void start(BundleContext bundleContext) throws Exception {

        ApiBanking service =  createBuilderFromProperties(bundleContext).build();

        registration = bundleContext.registerService(
                ApiBanking.class,
               service,
                null
        );

        logger.info("Performing verification call (statistics).. ");

        QueueStatistics stats = service.statistics(UUID.randomUUID().toString());

        logger.info(" .. there are " + stats.getMessageCount() + " messages in the back log. ");
        logger.info(" .. the oldest message is " + (System.currentTimeMillis()-stats.getHeadOfBacklogTimestamp())/1000.0d + " seconds old. ");

        logger.info("API Banking service initialised successfully.");

    }

    @Override
    public void stop(BundleContext bundleContext) {
        registration.unregister();
    }

    private ApiBankingBuilder createBuilderFromProperties(BundleContext bundleContext) {

        String environment = bundleContext.getProperty(SCB_API_BANKING_ENVIRONMENT);
        String keyStoreFile = bundleContext.getProperty(SCB_API_BANKING_KEYSTORE_FILE);
        String keyStorePassword = bundleContext.getProperty(SCB_API_BANKING_KEYSTORE_PASSWORD);
        String signatureKeyAlias = bundleContext.getProperty(SCB_API_BANKING_KEYSTORE_SIGNATURE_ALIAS);

        String issuerId = bundleContext.getProperty(SCB_API_BANKING_ISSUER_ID);
        String proxyHost = bundleContext.getProperty(SCB_API_BANKING_PROXY_HOST);
        String proxyPort = bundleContext.getProperty(SCB_API_BANKING_PROXY_PORT);

        String jwtOffset = bundleContext.getProperty(SCB_API_BANKING_JWT_ISSUE_OFFSET);
        if (jwtOffset != null && jwtOffset.trim().length() > 0) {
            JWT.applyIssueTimeOffset(Long.parseLong(jwtOffset));
        }

        boolean encRes = "true".equals(bundleContext.getProperty(SCB_API_BANKING_ENCRYPT_RESPONSES));

        logger.info("Initialising API Banking service with the following properties: ");
        logger.info("     Environment: " + environment);
        logger.info("     Keystore File: " + keyStoreFile);
        logger.info("     Issuer Id: " + issuerId);
        logger.info("     Encrypted Responses: " + encRes);
        logger.info("     Proxy Host: " + proxyHost);
        logger.info("     Proxy Port: " + proxyPort);
        if (jwtOffset != null && jwtOffset.trim().length() > 0) {
            logger.info("   JWT Time Offset: " + jwtOffset + "ms. ");
        }


        logger.info("Initialising key store & http client .. ");
        ApiBankingBuilder builder = ApiBankingBuilder.$(ApiBankingEnvironment.valueOf(environment))
                .withSignatureAlias(signatureKeyAlias)
                .withIssuerId(issuerId)
                .withKeystore(openKeyStore(keyStoreFile, keyStorePassword), keyStorePassword);

        if (encRes) {
            builder = builder.encryptedResponses();
        }

        logger.info("Apply proxy configuration.. ");
        if (proxyHost != null && proxyHost.trim().length() > 0) {
            builder = builder.withProxy(new Proxy(Proxy.Type.HTTP,new InetSocketAddress(proxyHost,Integer.parseInt(proxyPort))));
        }
        return builder;
    }

    private KeyStore openKeyStore(String keyStoreFile, String keyStorePassword) {
        try {
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            try (InputStream input = new URL(keyStoreFile).openStream()) {
                ks.load(input, keyStorePassword.toCharArray());
            }
            return ks;
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

}
