package com.scb.api.client.model;

public class EventAck {

    private String id;
    private long timestamp;
    private String transactionId;
    private String status;

    public EventAck() {

    }

    public String getId() {
        return id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getStatus() {
        return status;
    }

    public String getTransactionId() { return transactionId; }
}
