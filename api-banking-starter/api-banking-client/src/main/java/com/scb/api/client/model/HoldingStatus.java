package com.scb.api.client.model;

public class HoldingStatus {

    private String status;
    private String scaId;
    private String date;

    //entity + ":" + businessPartnerId + ":" + scaId + ":" + date
    public HoldingStatus(String status, String scaId, String date) {
        this.status = status;
        this.scaId = scaId;
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public String getScaId() {
        return scaId;
    }

    public String getDate() {
        return date;
    }
}
