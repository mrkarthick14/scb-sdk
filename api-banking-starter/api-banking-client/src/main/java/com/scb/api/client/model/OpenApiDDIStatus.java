package com.scb.api.client.model;

public class OpenApiDDIStatus {
	private String statusString;
	private String statusCode;
	private String timestamp;
	private String reasonInformation;
	private String referenceId;
	
	
	
	public OpenApiDDIStatus() {
		
	}



	public OpenApiDDIStatus(String statusString, String statusCode, String timestamp, String reasonInformation,
			String referenceId) {
	
		this.statusString = statusString;
		this.statusCode = statusCode;
		this.timestamp = timestamp;
		this.reasonInformation = reasonInformation;
		this.referenceId = referenceId;
	}



	public String getStatusString() {
		return statusString;
	}



	public String getStatusCode() {
		return statusCode;
	}



	public String getTimestamp() {
		return timestamp;
	}



	public String getReasonInformation() {
		return reasonInformation;
	}



	public String getReferenceId() {
		return referenceId;
	}
	
	
	
	
	
}
