package com.scb.s2b;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.UUID;

import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.OpenAPI;
import com.scb.api.client.model.DDIInitiateModel;
import com.scb.api.client.model.DDIInitiateModelBuilder;
import com.scb.api.client.model.OpenApiBankParty;
import com.scb.api.client.model.OpenApiDDIId;
import com.scb.api.client.model.OpenApiDDIStatus;
import com.scb.api.client.model.instructedAmount;
import com.scb.api.client.util.HttpUtility;
import com.scb.api.client.util.JWT;

public class DDI {

	public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {

        JWT.applyIssueTimeOffset(1000);
        final HttpUtility httpUtility;
        ApiBanking apiBanking = ApiBankingBuilder.
                $(ApiBankingEnvironment.Test).
                withKeystore(PaymentTemplate.class.getClassLoader().getResourceAsStream("scb-api-banking.jks"), "123456").
                withSignatureAlias("apibanking-sig-key").
                encryptedResponses().
                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))).
                build();
        
        initiateDDI(apiBanking);

    }
	
	public static void initiateDDI(ApiBanking apiBanking){
		DDIInitiateModelBuilder template = DDIInitiateModel.WithReferenceId("1234689")
				.withInstructedAmount(new instructedAmount("INR",BigDecimal.valueOf(2))).withMandateId("1378967")
				.withDebtor(new OpenApiBankParty(null,"Name",null)).build();
		OpenApiDDIId receipt = apiBanking.schema(OpenAPI.class).initiateDDI(UUID.randomUUID().toString(), template);
		OpenApiDDIStatus status = apiBanking.schema(OpenAPI.class).DDIStatus(UUID.randomUUID().toString(), receipt.getReferenceId());
	}
}
