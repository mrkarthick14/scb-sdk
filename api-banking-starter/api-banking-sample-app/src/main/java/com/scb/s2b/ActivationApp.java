package com.scb.s2b;

import com.google.devtools.common.options.Option;
import com.google.devtools.common.options.OptionsBase;
import com.google.devtools.common.options.OptionsParser;
import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.util.JWT;
import org.apache.commons.io.IOUtils;

import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.UUID;

import static com.google.common.base.Strings.isNullOrEmpty;
import static com.scb.s2b.ActivationApp.ActivationOptions.mandatoryOptionsAreNotSet;
import static com.scb.s2b.ActivationApp.ActivationOptions.printUsage;

public class ActivationApp {

    public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {

        OptionsParser parser = OptionsParser.newOptionsParser(ActivationOptions.class);
        parser.parseAndExitUponError(args);
        ActivationOptions options = parser.getOptions(ActivationOptions.class);
        if (mandatoryOptionsAreNotSet(options)) {
            printUsage(parser);
            System.exit(-1);
        }

        //-a C:/Users/HP/Downloads/certs/activationKey.json -k C:/Users/HP/Downloads/certs/scb-api-banking.jks -s apibanking-sig-key -w DISABLED -p password
        String keystorePassword = isNullOrEmpty(options.keystorePassword) ? promptForPassword() : options.keystorePassword;

        ApiBankingEnvironment env = ApiBankingEnvironment.valueOf(options.environment);
        InputStream keyStore = openStreamForPath(options.keystorePath);
        InputStream activationKey = openStreamForPath(options.activationKeyPath);
        JWT.applyIssueTimeOffset(options.offset);

        ApiBanking apiBanking = ApiBankingBuilder.
                $(env).
                withKeystore(keyStore, keystorePassword).
                withSignatureAlias(options.signatureAlias).
                encryptedResponses().
//                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))). //set proxy details here only if required
                build();

        System.out.println("Activating account for web hook url: " + options.webhookUrl + "... ");

        String messageId = UUID.randomUUID().toString();

        InputStream result = apiBanking.activate(
                activationKey,
                messageId,
                true,
                options.webhookUrl
        );

        System.out.println(IOUtils.toString(result, StandardCharsets.UTF_8));

    }

    private static String promptForPassword() {
        Console console = System.console();
        if (console == null) {
            System.out.println("Couldn't get Console instance. Running in IDE?");
            System.exit(0);
        }
        return  new String(console.readPassword("Please enter keystore password: "));
    }

    private static InputStream openStreamForPath(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        return Files.newInputStream(path, StandardOpenOption.READ);
    }


    @SuppressWarnings("WeakerAccess")
    public static class ActivationOptions extends OptionsBase {


        @Option(
                name = "help",
                abbrev = 'h',
                help = "Prints usage info.",
                defaultValue = "false"
        )
        public boolean help;

        @Option(
                name = "webhookurl",
                abbrev = 'w',
                help = "The Webhook URL to receive events from API Bankiing",
                category = "mandatory",
                defaultValue = ""
        )
        public String webhookUrl;

        @Option(
                name = "keystorepath",
                abbrev = 'k',
                help = "Path to keystore.",
                category = "mandatory",
                defaultValue = ""
        )
        public String keystorePath;

        @Option(
                name = "sigalias",
                abbrev = 's',
                help = "Alias of keystore entry used for signing & encryption",
                category = "mandatory",
                defaultValue = ""
        )
        public String signatureAlias;

        @Option(
                name = "environment",
                abbrev = 'e',
                help = "Environment to connect to: Test or Production",
                category = "optional",
                defaultValue = "Test"
        )
        public String environment;


        @Option(
                name = "jwttimeoffset",
                abbrev = 'j',
                help = "Offset to apply to JWT issue time to account for clock skew between client and server",
                category = "optional",
                defaultValue = "1000"
        )
        public int offset;

        @Option(
                name = "activationkeypath",
                abbrev = 'a',
                help = "Path to activation key",
                category = "mandatory",
                defaultValue = "C:/Users/HP/Downloads/certs/scb-api-banking.jks"
        )
        public String activationKeyPath;


        @Option(
                name = "keystorepassword",
                abbrev = 'p',
                help = "Keystore password",
                category = "optional",
                defaultValue = ""
        )
        public String keystorePassword;


        static void printUsage(OptionsParser parser) {
            System.out.println("Usage: java -jar activate.jar OPTIONS");
            System.out.println(parser.describeOptions(Collections.<String, String>emptyMap(),
                    OptionsParser.HelpVerbosity.LONG));
        }

        static boolean mandatoryOptionsAreNotSet(ActivationOptions options) {
            return (options == null ||
                    options.help ||
                    isNullOrEmpty(options.keystorePath) ||
                    isNullOrEmpty(options.webhookUrl) ||
                    isNullOrEmpty(options.signatureAlias) ||
                    isNullOrEmpty(options.activationKeyPath));
        }

    }


}
