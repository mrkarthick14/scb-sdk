package com.scb.s2b;

import com.google.common.collect.ImmutableMap;
import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.model.EventAck;
import com.scb.api.client.model.MessageBatch;
import com.scb.api.client.model.QueueStatistics;
import com.scb.api.client.util.JWT;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.UUID;

public class SampleApp {

    public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {

        JWT.applyIssueTimeOffset(1000);

        ApiBanking apiBanking = ApiBankingBuilder.
                $(ApiBankingEnvironment.Test).
                withKeystore(SampleApp.class.getClassLoader().getResourceAsStream("scb-api-banking.jks"), "123456").
                withSignatureAlias("apibanking-sig-key").
                encryptedResponses().
                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))).
                build();


        QueueStatistics stats = apiBanking.statistics(UUID.randomUUID().toString());


        MessageBatch batch = apiBanking.consume(UUID.randomUUID().toString(), 10);
        
       MessageBatch recover = apiBanking.recover(UUID.randomUUID().toString());
        
       MessageBatch peek = apiBanking.peek(UUID.randomUUID().toString());

        System.out.println(batch);


    }

}
