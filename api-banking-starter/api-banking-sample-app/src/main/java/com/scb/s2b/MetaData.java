package com.scb.s2b;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.OpenAPI;

import com.scb.api.client.model.MessageBatch;
import com.scb.api.client.model.OpenApiAccount;
import com.scb.api.client.model.QueueStatistics;
import com.scb.api.client.model.MetaData.DataFilter;
import com.scb.api.client.model.MetaData.Delivery;
import com.scb.api.client.model.MetaData.Filters;
import com.scb.api.client.model.MetaData.General;
import com.scb.api.client.model.MetaData.MetaDataModel;
import com.scb.api.client.model.MetaData.MetaDataModelBuilder;
import com.scb.api.client.model.MetaData.OpenApiMetaDataId;
import com.scb.api.client.model.MetaData.RequestTransform;
import com.scb.api.client.model.MetaData.ResponseTransform;
import com.scb.api.client.model.MetaData.Steps;
import com.scb.api.client.model.MetaData.Values;
import com.scb.api.client.util.JWT;

public class MetaData {
	
	
	 
	
	 public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {
		 	JWT.applyIssueTimeOffset(1000);

	        ApiBanking apiBanking = ApiBankingBuilder.
	                $(ApiBankingEnvironment.Test).
	                withKeystore(SampleApp.class.getClassLoader().getResourceAsStream("scb-api-banking.jks"), "123456").
	                withSignatureAlias("apibanking-sig-key").
	                encryptedResponses().
	                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))).
	                build();
	        
	        metaSave(apiBanking);
	        
	    }
	 
//	 public static void metaSave(ApiBanking apiBanking) { 
//		 
//		 String metaData = "{  \"name\": \"scb.internal.supportingWebHookConfigDetails\",  \"values\": {    \"general\": {      \"version\": \"V_1_0\",      \"apiBankingEnabled\": \"Y\",      \"webhookUrl\": \"http://scb-test-api.picme.lk\",      \"publicKey\": \"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlnOCGYMjRR1toSvSgWq2DB1ytz5VF5I1M7wkOksxnJhbgiEC7MnlrctEgPqQXWzzULk/StQSbwip39ZbDEr+uLC/yks2z1+8DmgZe8WR5b/6SBXV3hrPc/rWS0v+fYHkdJ2buhqhqcOfwYntu1tEk3tGyjubQDa6XobDZRHhuRF9mzw7Eld2Atf//wxiGzHylNVa+/SysozVy68ZUcn3tveYClxOEuVYIv3EEnsWnkBPu5/PHBplLr7t6IdJ86viB9hkdocYWj5TQgMccwees6fZ7MKcLeh+WryyajqFJsppvT70t4RmiABjrp1BWB4VEoA9PYwiOKEPsDD8OZOCSQIDAQAB\"    },    \"steps\": {      \"dataFilter\": {        \"bean\": \"webHookPathDataBlockingFilter\",        \"filters\": [          {            \"path\": \"adviceType\",            \"values\": [\"Credit\", \"Debit\"]          }        ]      },      \"requestTransform\": {        \"bean\": \"webHookRequestTransform\"      },      \"delivery\": {        \"bean\": \"webHookDelivery\"      },      \"responseTransform\": {        \"bean\": \"defaultResponseTransformTransition\"      }    }  }}  ";
//		 
//		 OpenApiPaymentTemplateId receipt = apiBanking.schema(OpenAPI.class).initiateMetaSave(UUID.randomUUID().toString(), metaData);
//	 }
	 
	 public static void metaSave(ApiBanking apiBanking){
		 
		 List<Object> list = new ArrayList<Object>();
		 List<String> values = new ArrayList<String>();
		 values.add("Credit");
		 values.add("Debit");
		 list.add(new Filters("adviceType",values));
		 
		 MetaDataModelBuilder metaData = MetaDataModel.withName("scb.internal.supportingWebHookConfigDetails")
				 .withValues(new Values(new General("V_1_0","Y","http://scb-test-api.picme.lk","MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt+9VWdJtTkWqjhjITigN8tBpea23FioZu4quSV2VvIdU/s2rzKh/TlbLworws9yzi54A4cwHW0Mn2lY5Ef59dFmok+3E0tAJJqnH9svNsiV+YJCTaa/EI9xVfXoL5XklgXdBFhYDbbMyszoS3XgPD2WyA3q/KGsu3l/NM9PQ+TJCMvUEFhpNrY5dgHx0cX56AuU0sTds4bSrpe/iJ/ugZKHIJylIDpzCawyaN94FUIvedbAWNRulyPmGDUYiHwFRvUjgQcMKAReewkOA6hY51Etnt70saDRcLuDCbMYK3KEpHXD+pmlC1Xar4ybyUJanRHMuwwbfzDlaAdOhOQ8IpQIDAQAB"), new Steps(new DataFilter("webHookPathDataBlockingFilter",list),new RequestTransform("webHookRequestTransform"), new Delivery("webHookDelivery"), new ResponseTransform("defaultResponseTransformTransition")))).build();
		 OpenApiMetaDataId receipt = apiBanking.schema(OpenAPI.class).initiateMetaSave(UUID.randomUUID().toString(), metaData);		
		 
		 System.out.println(receipt.getId());
		 
	 }
//	 
	 
	 
}