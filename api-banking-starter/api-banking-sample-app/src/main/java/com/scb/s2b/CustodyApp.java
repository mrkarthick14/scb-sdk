package com.scb.s2b;

import com.google.common.collect.ImmutableList;
import com.scb.api.client.*;
import com.scb.api.client.model.custody.*;
import org.joda.time.LocalDate;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.UUID;

public class CustodyApp {

    /*
     * This is sample client application to consume custody holding and custody transaction APIs
     * */

    private ApiBanking apiBanking;

    public CustodyApp() throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {
        apiBanking = ApiBankingBuilder.
                $(ApiBankingEnvironment.Test).
                withKeystore(SampleApp.class.getClassLoader().getResourceAsStream("scb-api-banking.jks"), "123456").
                withSignatureAlias("apibanking-sig-key").
                encryptedResponses().
                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))).
                build();
        System.out.println("Environment Building Complete");
    }


    public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {

        CustodyApp custody = new CustodyApp();

        Holdings holdings = custody.getHoldings();

        HoldingStatus holdingStatus = custody.getHoldingsStatus();

        TransactionStatuses transactionStatus = custody.getTransactionStatus();
    }

    private Holdings getHoldings() {

        String messageId = UUID.randomUUID().toString();
        HoldingsReportRequest request = new HoldingsReportRequest.Builder()
                .withBusinessPartnerId("BP0436651")
                .withScaId("043665100002")
                .withDate(LocalDate.parse("2020-11-04"))
                .build();
        return apiBanking.schema(Custody.class).getHoldings(request, messageId);
    }

    private HoldingStatus getHoldingsStatus() {
        String messageId = UUID.randomUUID().toString();
        return apiBanking.schema(Custody.class).getHoldingStatus("d533caa5-374b-4a13-b19f-212c978b58f2", messageId);
    }

    private TransactionStatuses getTransactionStatus() {
        String messageId = UUID.randomUUID().toString();
        TransactionStatusRequest request = new TransactionStatusRequest.Builder()
                .withBusinessPartnerId("BP0436651")
                .withSecurityAccountIds(ImmutableList.<String>of("043665100005"))
                .withStatus("Cancelled")
                .build();
        return apiBanking.schema(Custody.class).getTransactionStatus(request, messageId);
    }
}