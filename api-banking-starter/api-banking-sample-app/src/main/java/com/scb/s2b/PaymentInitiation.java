package com.scb.s2b;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.UUID;

import org.joda.time.Instant;
import org.joda.time.LocalDate;

import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.OpenAPI;
import com.scb.api.client.model.AccountIdentifier;
import com.scb.api.client.model.DenominatedAmount;
import com.scb.api.client.model.ExternalCodes;
import com.scb.api.client.model.OpenApiAddress;
import com.scb.api.client.model.OpenApiAgent;
import com.scb.api.client.model.OpenApiBankParty;
import com.scb.api.client.model.OpenApiFinancialInstitution;
import com.scb.api.client.model.OpenApiParty;
import com.scb.api.client.model.OpenApiPaymentAccount;
import com.scb.api.client.model.OpenApiPaymentId;
import com.scb.api.client.model.OpenApiPaymentInstruction;
import com.scb.api.client.model.OpenApiPaymentInstructionHeader;
import com.scb.api.client.model.OpenApiPaymentInstructionInstruction;
import com.scb.api.client.model.OpenApiPaymentStatus;
import com.scb.api.client.model.OpenApiPaymentStatuses;
import com.scb.api.client.model.OpenApiPaymentInstructionInstruction.PaymentTypePreference;
import com.scb.api.client.util.JWT;

public class PaymentInitiation {

	  public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {

	        JWT.applyIssueTimeOffset(1000);

	        ApiBanking apiBanking = ApiBankingBuilder.
	                $(ApiBankingEnvironment.Test).
	                withKeystore(SampleApp.class.getClassLoader().getResourceAsStream("scb-api-banking.jks"), "123456").
	                withSignatureAlias("apibanking-sig-key").
	                encryptedResponses().
	                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))). //Proxy can be configured here if required
	                build();
	        initiatePayment(apiBanking);

}
	  private static void initiatePayment(ApiBanking apiBanking){
		  OpenApiPaymentInstructionHeader header = new OpenApiPaymentInstructionHeader().build();
		  OpenApiPaymentInstructionInstruction instruction = OpenApiPaymentInstructionInstruction.builder().withPaymentTimestamp(Instant.now())
				  .withPaymentTypePreference(PaymentTypePreference.Explicit).withRequiredExecutionDate(LocalDate.now())
				  .withAmount(DenominatedAmount.of(BigDecimal.valueOf(2), "INR")).withReferenceId("126567378")
				  .withPurpose("OTHR").withPaymentType("RTGS").withChargerBearer(ExternalCodes.ChargeBearerType.SHAR)
				  .withDebtor(new OpenApiBankParty(null,"IIFL",null))
				  .withDebtorAccount(new OpenApiPaymentAccount("22905004732",null,null))
				  .withDebtorAgent(new OpenApiAgent(new OpenApiFinancialInstitution("SCBLINBBXXX", "001", new OpenApiAddress(null,null,null,"","","600119","IN"), "STANDARD CHARTERED BK"), "MCP", "123", "001"))
				  .withCreditor(new OpenApiParty("Test",null,null,null))
				  .withCreditorAgent(new OpenApiAgent(new OpenApiFinancialInstitution("UTIB0000234", "", new OpenApiAddress(null,null,null,null,null,null,null), "Test Creditor"), "123", null, null))
				  .withCreditorAccount(new OpenApiPaymentAccount("S239374820",ExternalCodes.CashAccountType.CASH,AccountIdentifier.IdentifierType.BBAN))
				  .build();
		  
		  OpenApiPaymentInstruction paymentInstruction = new OpenApiPaymentInstruction(header, instruction);
		  OpenApiPaymentId receipt = apiBanking.schema(OpenAPI.class).initiatePayment(UUID.randomUUID().toString(), paymentInstruction, false);
		  OpenApiPaymentStatuses statuses = apiBanking.schema(OpenAPI.class).paymentStatus(UUID.randomUUID().toString(), Collections.singleton(receipt.getClientReferenceId()));

	       OpenApiPaymentStatus status = statuses.getStatuses().iterator().next();
	       System.out.println("Status of payment ref " + status.getClientIdentifier() + " is " + status.getStatusString());
		  
	  }

}
