package com.scb.s2b;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.UUID;

import com.scb.api.client.util.HttpUtility;
import com.scb.api.client.util.JWT;
import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.OpenAPI;
import com.scb.api.client.model.*;
import com.scb.api.client.model.PaymentTemplate.OpenApiPaymentTemplate;
import com.scb.api.client.model.PaymentTemplate.OpenApiPaymentTemplateBuilder;
import com.scb.api.client.model.PaymentTemplate.OpenApiPaymentTemplateId;

public class PaymentTemplate {

	public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {

        JWT.applyIssueTimeOffset(1000);
        final HttpUtility httpUtility;
        ApiBanking apiBanking = ApiBankingBuilder.
                $(ApiBankingEnvironment.Test).
                withKeystore(PaymentTemplate.class.getClassLoader().getResourceAsStream("scb-api-banking.jks"), "123456").
                withSignatureAlias("apibanking-sig-key").
                encryptedResponses().
                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))).
                build();
        
        submitPaymentTemplate(apiBanking);

    }
	private static void submitPaymentTemplate(ApiBanking apiBanking){
		
		
		OpenApiPaymentTemplateBuilder template = OpenApiPaymentTemplate.withScheme("OPENAPI").withCountryCode("IN")
				.withBicCode("BIC").withTransactionType("IBFT").withLocalInstrumentType("IBFT").withChargerBearerCode("SHAR")
				.withInitiatingPartyId("11153123").withPaymentMethod("TRF").withpaymentServiceLevel("SDVA").withPaymentPriority("NORM")
				.withCategoryPurpose("P2A").withDefaultCreditorAccountType("CASH").withVersion(0).withDefaultCity("BOM").withTsVersion(1).build();
	
		System.out.println(template.toString());
		
		OpenApiPaymentTemplateId receipt = apiBanking.schema(OpenAPI.class).initiatePaymentTemplate(UUID.randomUUID().toString(), template);
	    System.out.println(receipt);
	}

}
