package com.scb.s2b;

import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.OpenAPI;
import com.scb.api.client.model.*;
import com.scb.api.client.model.ExternalCodes.CashAccountType;
import com.scb.api.client.model.OpenApiPaymentInstructionInstruction.PaymentTypePreference;
import com.scb.api.client.util.JWT;
import org.joda.time.Instant;
import org.joda.time.LocalDate;

import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

public class PaymentApp {

    public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {

        JWT.applyIssueTimeOffset(1000);

        ApiBanking apiBanking = ApiBankingBuilder.
                $(ApiBankingEnvironment.Test).
                withKeystore(new FileInputStream("C:/Users/HP/Downloads/certs/scb-api-banking.jks"), "kV45BP2DNbxH4VjKhc").
                withSignatureAlias("apibanking-sig-key").
                encryptedResponses().
//                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))). //Proxy can be configured here if required
                build();
//        getBalance(apiBanking);
        submitIMPS(apiBanking);
    }

    private static void getBalance(ApiBanking apiBanking) {
        Object receipt = apiBanking.schema(OpenAPI.class).getBalance();

    }

    private static void submitIMPS(ApiBanking apiBanking) {
        OpenApiPaymentInstructionHeader header = new OpenApiPaymentInstructionHeader().build();
        
        
        OpenApiPaymentInstructionInstruction instruction = OpenApiPaymentInstructionInstruction.builder().
                withPaymentTimestamp(Instant.now()).
                withPaymentTypePreference(PaymentTypePreference.Explicit).
                withRequiredExecutionDate(LocalDate.now()).
                withAmount(DenominatedAmount.of(BigDecimal.valueOf(2), "INR")).
                withReferenceId("123456789").withPurpose("OTHER").
                withPaymentType("RTGS").
                withDebtorAccount(new OpenApiPaymentAccount("0101132824", CashAccountType.CACC, AccountIdentifier.IdentifierType.Other)).
                withDebtorAgent(new OpenApiAgent(new OpenApiFinancialInstitution("SCBLINBBXXX", "0021", null, null), null, null, null)).
                withCreditorAccount(new OpenApiPaymentAccount("11082030", CashAccountType.CACC, AccountIdentifier.IdentifierType.IBAN)).
                withCreditorAgent(new OpenApiAgent(new OpenApiFinancialInstitution("HDFC0001364", null, null, null), null, null, null)).
                build();

        OpenApiPaymentInstruction paymentInstruction = new OpenApiPaymentInstruction(header, instruction);


       OpenApiPaymentId receipt = apiBanking.schema(OpenAPI.class).initiatePayment(UUID.randomUUID().toString(), paymentInstruction, false);
      
        OpenApiPaymentStatuses statuses = apiBanking.schema(OpenAPI.class).paymentStatus(UUID.randomUUID().toString(), Collections.singleton(receipt.getClientReferenceId()));

       OpenApiPaymentStatus status = statuses.getStatuses().iterator().next();
       System.out.println("Status of payment ref " + status.getClientIdentifier() + " is " + status.getStatusString());
    }


    private static void submitPayNow(ApiBanking apiBanking) {
        OpenApiPaymentInstructionHeader header = new OpenApiPaymentInstructionHeader("client-sdk", UUID.randomUUID().toString(), "SG", Instant.now());

        OpenApiPaymentInstructionInstruction instruction = OpenApiPaymentInstructionInstruction.builder().
                withPaymentTimestamp(Instant.now()).
                withPaymentTypePreference(PaymentTypePreference.Fastest).
                withRequiredExecutionDate(LocalDate.now()).
                withAmount(DenominatedAmount.of(BigDecimal.valueOf(2), "SGD")).
                withReferenceId(String.valueOf(System.currentTimeMillis())).withPurpose("CASH").
                withPaymentType("IBFT").
                withDebtor(new OpenApiBankParty("", "TEST", null)).
                withDebtorAccount(new OpenApiPaymentAccount("0100000142", CashAccountType.CACC, AccountIdentifier.IdentifierType.Other)).
                withDebtorAgent(new OpenApiAgent(new OpenApiFinancialInstitution("SCBLSGS0XXX", null, new OpenApiAddress("SG"), "STANDARD CHARTERED BANK"), "", null, null)).
                withCreditor(new OpenApiParty(new OpenApiIdentity("+6583834952", "RetMSISDN"))).
                build();

        OpenApiPaymentInstruction paymentInstruction = new OpenApiPaymentInstruction(header, instruction);


        OpenApiPaymentId receipt = apiBanking.schema(OpenAPI.class).initiatePayment(UUID.randomUUID().toString(), paymentInstruction, true);

        System.out.println("Payment submitted:" + receipt);

        OpenApiPaymentStatuses statuses = apiBanking.schema(OpenAPI.class).paymentStatus(UUID.randomUUID().toString(), Collections.singleton(receipt.getClientReferenceId()));

        OpenApiPaymentStatus status = statuses.getStatuses().iterator().next();
        System.out.println("Status of payment ref " + status.getClientIdentifier() + " is " + status.getStatusString());
    }


}
