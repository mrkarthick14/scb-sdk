package com.scb.s2b;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.UUID;

import org.joda.time.LocalDate;

import com.scb.api.client.ApiBanking;
import com.scb.api.client.ApiBankingBuilder;
import com.scb.api.client.ApiBankingEnvironment;
import com.scb.api.client.OpenAPI;
import com.scb.api.client.model.AccountIdentifier;
import com.scb.api.client.model.DDAInitiateModel;
import com.scb.api.client.model.DDAInitiateModelBuilder;
import com.scb.api.client.model.OpenApiAgent;
import com.scb.api.client.model.OpenApiBankParty;
import com.scb.api.client.model.OpenApiContact;
import com.scb.api.client.model.OpenApiDDAID;
import com.scb.api.client.model.OpenApiDDAStatus;
import com.scb.api.client.model.OpenApiPaymentAccount;
import com.scb.api.client.model.OpenApiPaymentLimit;

import com.scb.api.client.util.HttpUtility;
import com.scb.api.client.util.JWT;

public class DDA {

	public static void main(String[] args) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {

        JWT.applyIssueTimeOffset(1000);
        final HttpUtility httpUtility;
        ApiBanking apiBanking = ApiBankingBuilder.
                $(ApiBankingEnvironment.Test).
                withKeystore(PaymentTemplate.class.getClassLoader().getResourceAsStream("scb-api-banking.jks"), "123456").
                withSignatureAlias("apibanking-sig-key").
                encryptedResponses().
                withProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.239.9.190", 443))).
                build();
        
        initiateDDA(apiBanking);

    }
	
	public static void initiateDDA(ApiBanking apibanking){
		DDAInitiateModelBuilder template = DDAInitiateModel.withReferenceId("1378967A").withDebtorAccount(new OpenApiPaymentAccount("22905004732",null,AccountIdentifier.IdentifierType.BBAN)).withCreditorAccount(new OpenApiPaymentAccount("S239374820",null,AccountIdentifier.IdentifierType.BBAN))
				.withCreditorAgent(new OpenApiAgent(null,"003",null,null)).withDebtorAgent(new OpenApiAgent(null,"003",null,null)).withCreditor(new OpenApiBankParty("123456","Company Name",null)).withDebtor(new OpenApiBankParty(null,"name",new OpenApiContact(null,"+852-89891010",null,null,null)))
				.withPaymentLimit(new OpenApiPaymentLimit("1000000","1000000","Daily")).withExpiryDate(LocalDate.now()).withEffectiveDate(LocalDate.now()).withDebtorReference("1378967A").withCurrencyCode("HKD").withMandateType("Variable").withType("DDMP").build();
	
		OpenApiDDAID receipt = apibanking.schema(OpenAPI.class).initiateDDA(UUID.randomUUID().toString(), template);
		
		OpenApiDDAStatus status = apibanking.schema(OpenAPI.class).DDAStatus(UUID.randomUUID().toString(), receipt.getReferenceId());
		System.out.println(status.getReferenceId());
	}

}
